-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.26 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para hidrover_oficial
CREATE DATABASE IF NOT EXISTS `hidrover_oficial` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hidrover_oficial`;

-- Copiando estrutura para tabela hidrover_oficial.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categoria_uni_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_categoria_uni_negocio10` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.categoria: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`id`, `unidade_negocio_id`, `descricao`, `ativo`) VALUES
	(1, 1, 'Construção Cívil', 1),
	(2, 1, ' Agrícolas ', 1),
	(3, 1, ' Automotivos ', 1),
	(4, 1, 'Petrolíferas', 1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.cidade
CREATE TABLE IF NOT EXISTS `cidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_federativa_id` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cidade_unidade_federativa1_idx` (`unidade_federativa_id`),
  CONSTRAINT `fk_cidade_unidade_federativa1` FOREIGN KEY (`unidade_federativa_id`) REFERENCES `unidade_federativa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.cidade: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` (`id`, `unidade_federativa_id`, `descricao`, `ativo`) VALUES
	(7, 21, 'Flores da Cunha', 1),
	(8, 21, 'Caxias do Sul', 1);
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `contato_telefone` varchar(45) DEFAULT NULL,
  `cidade_id` int(11) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cliente1_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_cliente1_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.cliente: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`id`, `unidade_negocio_id`, `nome_fantasia`, `razao_social`, `email`, `cpf`, `telefone`, `celular`, `endereco`, `bairro`, `cep`, `contato`, `contato_telefone`, `cidade_id`, `ativo`) VALUES
	(1, 1, 'José carlos', 'askda', 'guilhermeomott@gmail.com', '', '', '54999123648', '', '', '', '', '', 7, 1),
	(2, 1, 'Magnos', 'Magnos', 'guilhermemott@hotmail.com', '1231231231', '123123', '123123', '', '', '', '', '', 8, 1);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.contato
CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `data` datetime DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contato_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_contato_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.contato: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` (`id`, `unidade_negocio_id`, `data`, `nome`, `telefone`, `email`, `assunto`, `ordem`, `status`, `ativo`) VALUES
	(7, 1, NULL, 'Rodrigo', '54 3292-0000', 'hdsauihdsiuhs@gmail.com', 'dsfdsfdsfsdfds', NULL, 1, 1),
	(8, 1, '2019-07-31 00:00:00', 'John', '54 3292-0000', 'rodrigogranzotto73@gmail.com', 'dsasadsa', NULL, 2, 1),
	(9, 1, '2019-07-31 00:00:00', 'Jenny', '549999', 'gui@kabum', 'fdggfd', NULL, 2, 1);
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.equipe
CREATE TABLE IF NOT EXISTS `equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `especialidade` varchar(255) DEFAULT NULL,
  `descricao_curriculum` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_equipe_unidade_negocio` (`unidade_negocio_id`),
  CONSTRAINT `fk_equipe_unidade_negocio` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.equipe: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
INSERT INTO `equipe` (`id`, `unidade_negocio_id`, `nome`, `especialidade`, `descricao_curriculum`, `foto`, `ativo`) VALUES
	(7, 1, 'John', 'Criminalista', 'Descrição do currículo e especialidade do profissional nesse espaço......', 'uploads/equipe/7/team-11.jpg', 1);
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.galeria
CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `detalhe` varchar(2000) DEFAULT NULL,
  `banner` varchar(150) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galeria_unidade_negocio1_idx` (`unidade_negocio_id`),
  KEY `categoria_id` (`categoria_id`),
  CONSTRAINT `FK2_galeria_categoria_id` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  CONSTRAINT `fk_galeria_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.galeria: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `galeria` DISABLE KEYS */;
INSERT INTO `galeria` (`id`, `unidade_negocio_id`, `categoria_id`, `descricao`, `detalhe`, `banner`, `ativo`) VALUES
	(2, 1, 1, 'Projeto Coopertec', 'hsaudhdsauhdsua', 'uploads/galeria/2/banner_mid.jpg', 1);
/*!40000 ALTER TABLE `galeria` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.galeria_categoria
CREATE TABLE IF NOT EXISTS `galeria_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) NOT NULL DEFAULT '0',
  `categoria_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `galeria_categoria_fk1` (`galeria_id`),
  KEY `galeria_categoria_galeria_fk1` (`categoria_id`),
  CONSTRAINT `galeria_categoria_fk2` FOREIGN KEY (`galeria_id`) REFERENCES `galeria` (`id`),
  CONSTRAINT `galeria_categoria_galeria_fk1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.galeria_categoria: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `galeria_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `galeria_categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.galeria_foto
CREATE TABLE IF NOT EXISTS `galeria_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_id` int(11) NOT NULL,
  `nome_arquivo` varchar(255) DEFAULT NULL,
  `extensao` varchar(5) DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galeria_foto_area1_idx` (`galeria_id`),
  CONSTRAINT `fk_galeria_foto_area1` FOREIGN KEY (`galeria_id`) REFERENCES `galeria` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.galeria_foto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `galeria_foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `galeria_foto` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.grupo_empresa
CREATE TABLE IF NOT EXISTS `grupo_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_fantasia` varchar(255) DEFAULT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.grupo_empresa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `grupo_empresa` DISABLE KEYS */;
INSERT INTO `grupo_empresa` (`id`, `nome_fantasia`, `razao_social`, `telefone`, `celular`, `ativo`) VALUES
	(1, 'Fin Esquadrias', 'Fin Esqua lll', '9555555', '9645656565656', 1);
/*!40000 ALTER TABLE `grupo_empresa` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.historia
CREATE TABLE IF NOT EXISTS `historia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `texto` text,
  `ano` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_historia_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_historia_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.historia: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `historia` DISABLE KEYS */;
INSERT INTO `historia` (`id`, `unidade_negocio_id`, `descricao`, `texto`, `ano`, `ativo`) VALUES
	(14, 1, 'Hidrover', 'Quando encontramos um lugar muito limpo, seja onde for, não jogamos papel no chão, e cuidamos de não sujar o recinto. A limpeza por si só já exige respeito, nem precisa de vigia. Assim também deve ser a sua apresentação. Limpa, serena e a ao mesmo tempo forte e determinada. Não apareça com cara de fraqueza, de “coitadinho”. Quanto mais coitadinho, mais as pessoas pisam.', '2000', 1),
	(17, 1, 'Quarto ano', 'Quando encontramos um lugar muito limpo, seja onde for, não jogamos papel no chão, e cuidamos de não sujar o recinto. A limpeza por si só já exige respeito, nem precisa de vigia. Assim também deve ser a sua apresentação. Limpa, serena e a ao mesmo tempo forte e determinada. Não apareça com cara de fraqueza, de “coitadinho”. Quanto mais coitadinho, mais as pessoas pisam.', '2004', 1),
	(18, 1, 'Quinto ano', 'Quando encontramos um lugar muito limpo, seja onde for, não jogamos papel no chão, e cuidamos de não sujar o recinto. A limpeza por si só já exige respeito, nem precisa de vigia. Assim também deve ser a sua apresentação. Limpa, serena e a ao mesmo tempo forte e determinada. Não apareça com cara de fraqueza, de “coitadinho”. Quanto mais coitadinho, mais as pessoas pisam.', '2005', 1),
	(19, 1, 'Criação da Empresa', 'Quando encontramos um lugar mui', '1980', 1),
	(20, 1, 'Historia da Empresa', 'Quando encontramos um lugar muito limpo, seja onde for, não jogamos papel no chão, e cuidamos de não sujar o recinto. A limpeza por si só já exige respeito, nem precisa de vigia. Assim também deve ser a sua apresentação. Limpa, serena e a ao mesmo tempo forte e determinada. Não apareça com cara de fraqueza, de “coitadinho”. Quanto mais coitadinho, mais as pessoas pisam.', '1900', 1);
/*!40000 ALTER TABLE `historia` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(500) DEFAULT NULL,
  `menu` varchar(500) DEFAULT NULL,
  `tipo_menu_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `ordem` int(5) NOT NULL,
  `target` varchar(50) NOT NULL,
  `icone` varchar(200) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_menu_fk` (`tipo_menu_id`),
  KEY `modulo_menu_fk1` (`modulo_id`),
  CONSTRAINT `modulo_menu_fk1` FOREIGN KEY (`modulo_id`) REFERENCES `modulo` (`id`),
  CONSTRAINT `tipo_menu_fk` FOREIGN KEY (`tipo_menu_id`) REFERENCES `tipo_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.menu: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `descricao`, `menu`, `tipo_menu_id`, `modulo_id`, `ordem`, `target`, `icone`, `ativo`) VALUES
	(1, 'Tipo Menu', 'sistema/tipo_menu/index', 1, 1, 5, '_Self', 'fa fa-filter', 1),
	(2, 'Menu', 'sistema/menu/index', 1, 1, 10, '_Self', 'fa fa-list', 1),
	(3, 'Grupo Usuários', 'sistema/usuario_grupo/index', 2, 1, 15, '_Self', 'fa fa-users', 1),
	(4, 'Unidade Negócio', 'sistema/unidade_negocio/index', 2, 1, 5, '_blank', 'fa fa-building', 1),
	(5, 'Usuários', 'sistema/usuario/index', 2, 1, 20, '_Self', 'fa fa-user', 1),
	(14, 'Tipo Pessoa', 'sistema/tipo_pessoa/index', 1, 1, 7, '_self', 'fa fa-male', 1),
	(15, 'Unidade Federativa - UF', 'sistema/unidade_federativa/index', 1, 1, 18, '_self', 'fa fa-map', 1),
	(16, 'Cidades', 'sistema/cidade/index', 2, 1, 10, '_self', 'fa fa-map-marker', 1),
	(18, 'Pessoas', 'sistema/pessoa/index', 2, 1, 10, '_self', 'fa fa-male', 1),
	(20, 'Grupo Empresa', 'sistema/grupo_empresa/index', 2, 2, 1, '_self', 'fa fa-gears', 1),
	(25, 'Clientes', 'servico/cliente/index', 2, 2, 100, '_self', 'fa fa-user', 1),
	(28, 'Serviços', 'servico/servico/index', 2, 2, 155, '_self', 'fa fa-list', 1),
	(34, 'Status Atendimento', 'servico/atendimento_status/index', 1, 2, 123, '_self', 'fa fa-list', 1),
	(35, 'Atendimentos', 'servico/atendimento/index', 3, 2, 199, '_self', 'fa fa-calendar', 1),
	(37, 'Painel', 'sistema/painel/index', 2, 1, 10, '_self', 'fa fa-list', 1),
	(38, 'Serviços', 'sistema/servico/index', 2, 1, 15, '_self', 'fa fa-gears', 1),
	(40, 'Contato', 'sistema/contato/index', 2, 1, 20, '_self', 'fa fa-phone', 1),
	(50, 'História', 'sistema/historia/index', 2, 1, 10, '_self', 'fa fa-history', 1),
	(53, 'Noticia', 'sistema/noticia/index', 2, 1, 10, '_self', 'fa fa-picture-o', 1),
	(54, 'Equipe', 'sistema/equipe/index', 2, 1, 10, '_self', 'fa fa-user-circle-o', 1),
	(55, 'Tópicos', 'sistema/topico/index', 2, 1, 10, '_self', 'fa fa-list', 1),
	(56, 'Categoria', 'sistema/categoria/index', 2, 1, 10, '_self', 'fa fa-star', 1),
	(57, 'Galeria', 'sistema/galeria/index', 2, 1, 15, '_self', 'fa fa-picture-o', 1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.modulo
CREATE TABLE IF NOT EXISTS `modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `ordem` int(5) DEFAULT NULL,
  `icone` varchar(200) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.modulo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;
INSERT INTO `modulo` (`id`, `descricao`, `ordem`, `icone`, `ativo`) VALUES
	(1, 'Sistema', 1, 'fa fa-gears', 1),
	(2, 'Serviço', 10, 'fa fa-handshake-o', 1),
	(3, 'BI - Dashboard', 20, 'fa fa-handshake-o', 1);
/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.noticia
CREATE TABLE IF NOT EXISTS `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `texto` longtext,
  `titulo_fonte` varchar(255) DEFAULT NULL,
  `link_fonte` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_arte_unidade_negocio_idx` (`unidade_negocio_id`),
  CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela hidrover_oficial.noticia: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;
INSERT INTO `noticia` (`id`, `unidade_negocio_id`, `titulo`, `subtitulo`, `texto`, `titulo_fonte`, `link_fonte`, `banner`, `ativo`) VALUES
	(26, 1, 'Noticia 1', 'SubNoticia 1', 'Podemos acreditar que tudo que a vida nos oferecerá no futuro é repetir o que fizemos ontem e hoje. Mas, se prestarmos atenção, vamos nos dar conta de que nenhum dia é igual a outro. Cada manhã traz uma bênção escondida; uma bênção que só serve para esse dia e que não se pode guardar nem desaproveitar. Se não usamos este milagre hoje, ele vai se perder. \r\n', 'Record TV', 'https://recordtv.r7.com/', 'uploads/noticia/26/background3.jpg', 1),
	(27, 1, 'Noticia 2', 'SubNoticia 2', 'Podemos acreditar que tudo que a vida nos oferecerá no futuro é repetir o que fizemos ontem e hoje. Mas, se prestarmos atenção, vamos nos dar conta de que nenhum dia é igual a outro. Cada manhã traz uma bênção escondida; uma bênção que só serve para esse dia e que não se pode guardar nem desaproveitar. Se não usamos este milagre hoje, ele vai se perder. \r\n', 'Record TV', 'https://recordtv.r7.com/', 'uploads/noticia/27/teste.jpg', 1),
	(28, 1, 'Noticia 3', 'SubNoticia 3', 'Podemos acreditar que tudo que a vida nos oferecerá no futuro é repetir o que fizemos ontem e hoje. Mas, se prestarmos atenção, vamos nos dar conta de que nenhum dia é igual a outro. Cada manhã traz uma bênção escondida; uma bênção que só serve para esse dia e que não se pode guardar nem desaproveitar. Se não usamos este milagre hoje, ele vai se perder. \r\n', 'Record TV', 'https://recordtv.r7.com/', 'uploads/noticia/28/call-to-action-bg.jpg', 1);
/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.noticia_foto
CREATE TABLE IF NOT EXISTS `noticia_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_id` int(11) NOT NULL,
  `nome_arquivo` varchar(255) DEFAULT NULL,
  `extensao` varchar(5) DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_noticia_foto_area1_idx` (`noticia_id`),
  CONSTRAINT `FK1_noticia_foto_noticia_id` FOREIGN KEY (`noticia_id`) REFERENCES `noticia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.noticia_foto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `noticia_foto` DISABLE KEYS */;
INSERT INTO `noticia_foto` (`id`, `noticia_id`, `nome_arquivo`, `extensao`, `principal`) VALUES
	(19, 26, 'background3', '.jpg', 0);
/*!40000 ALTER TABLE `noticia_foto` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.painel
CREATE TABLE IF NOT EXISTS `painel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `titulo` varchar(500) DEFAULT NULL,
  `texto` varchar(2000) DEFAULT NULL,
  `banner` varchar(1000) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_painel_site_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_painel_site_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.painel: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `painel` DISABLE KEYS */;
INSERT INTO `painel` (`id`, `unidade_negocio_id`, `descricao`, `titulo`, `texto`, `banner`, `ativo`) VALUES
	(7, 1, 'Foto 1', 'Soluções em sistemas hidráulicos', 'Construção, agrícola, automotivo e petrolífero', 'uploads/painel/7/banner11.jpg', 1),
	(8, 1, 'Foto 2', 'Renomada no Brasil', 'Certificações ISO 9001:2000 e ISO 14000', 'uploads/painel/8/banner2.jpg', 1),
	(9, 1, 'Foto 3', '45 anos de experiência', 'Know-how e especialização', 'uploads/painel/9/banner3.jpg', 1),
	(10, 1, 'Foto 4', 'Inovação e dinamismo', 'Forte liderança e solidez', 'uploads/painel/10/banner4.jpg', 1);
/*!40000 ALTER TABLE `painel` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.pessoa
CREATE TABLE IF NOT EXISTS `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `nome_fantasia` varchar(500) DEFAULT NULL,
  `razao_social` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `contato` varchar(255) DEFAULT NULL,
  `contato_telefone` varchar(45) DEFAULT NULL,
  `cidade_id` int(11) NOT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cliente_unidade_negocio1_idx` (`unidade_negocio_id`),
  KEY `fk_pessoa_cidade1_idx` (`cidade_id`),
  CONSTRAINT `fk_cliente_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pessoa_cidade1` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.pessoa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` (`id`, `unidade_negocio_id`, `nome_fantasia`, `razao_social`, `email`, `cpf`, `telefone`, `celular`, `endereco`, `bairro`, `cep`, `contato`, `contato_telefone`, `cidade_id`, `ativo`) VALUES
	(1, 1, 'Farrapo Travel', 'Farrapo Travel', 'guilhermeomott@gmail.com', '123.123.123-12', '432123423423', '99123648', 'Frare4444', 'asdfasd', '2323432432', 'asdfafafd', '3432423423', 8, 1);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.pessoa_tipo
CREATE TABLE IF NOT EXISTS `pessoa_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pessoa_id` int(11) NOT NULL,
  `tipo_pessoa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pessoa_tipo_pessoa_idx` (`pessoa_id`),
  KEY `fk_pessoa_tipo_tipo_pessoa1_idx` (`tipo_pessoa_id`),
  CONSTRAINT `fk_pessoa_tipo_pessoa` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pessoa_tipo_tipo_pessoa1` FOREIGN KEY (`tipo_pessoa_id`) REFERENCES `tipo_pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.pessoa_tipo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoa_tipo` DISABLE KEYS */;
INSERT INTO `pessoa_tipo` (`id`, `pessoa_id`, `tipo_pessoa_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 5);
/*!40000 ALTER TABLE `pessoa_tipo` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.servico
CREATE TABLE IF NOT EXISTS `servico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_produto_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_produto_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.servico: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `servico` DISABLE KEYS */;
INSERT INTO `servico` (`id`, `unidade_negocio_id`, `descricao`, `titulo`, `texto`, `banner`, `ativo`) VALUES
	(5, 1, 'Consumidor', 'Consumidor', 'Podemos acreditar que tudo que a vida nos oferecerá no futuro é repetir o que fizemos ontem e hoje.', 'uploads/servico/5/182e49_b0bdfb6f672547ba97554c46956549fc_mv2.jpg', 1),
	(6, 1, 'Trabalhista', 'Trabalhista', 'Cada manhã traz uma bênção escondida; uma bênção que só serve para esse dia e que não se pode guardar nem desaproveitar.', 'uploads/servico/6/advocacia-trabalhista.jpg', 1),
	(7, 1, 'Criminal', 'Criminal', 'Mas, se prestarmos atenção, vamos nos dar conta de que nenhum dia é igual a outro. ', 'uploads/servico/7/2951862.jpg', 1);
/*!40000 ALTER TABLE `servico` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.servico_foto
CREATE TABLE IF NOT EXISTS `servico_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servico_id` int(11) NOT NULL,
  `nome_arquivo` varchar(255) DEFAULT NULL,
  `extensao` varchar(5) DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_servico_foto_area1_idx` (`servico_id`),
  CONSTRAINT `fk_servico_foto_area1` FOREIGN KEY (`servico_id`) REFERENCES `servico` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.servico_foto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `servico_foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `servico_foto` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.tipo_menu
CREATE TABLE IF NOT EXISTS `tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `ordem` int(5) DEFAULT NULL,
  `icone` varchar(200) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.tipo_menu: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_menu` DISABLE KEYS */;
INSERT INTO `tipo_menu` (`id`, `descricao`, `ordem`, `icone`, `ativo`) VALUES
	(1, 'Configurações', 5, 'fa fa-gear', 1),
	(2, 'Cadastros', 10, 'fa fa-list', 1),
	(3, 'Movimentos', 15, 'fas fa-chalkboard-teacher', 1),
	(4, 'Relatórios', 20, 'far fa-chart-bar', 1);
/*!40000 ALTER TABLE `tipo_menu` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.tipo_pessoa
CREATE TABLE IF NOT EXISTS `tipo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.tipo_pessoa: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_pessoa` DISABLE KEYS */;
INSERT INTO `tipo_pessoa` (`id`, `descricao`, `ativo`) VALUES
	(1, 'Cliente', 1),
	(2, 'Fornecedor', 1),
	(3, 'Motorista', 1),
	(4, 'Posto Combustível', 1),
	(5, 'Mecânico', 1);
/*!40000 ALTER TABLE `tipo_pessoa` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.topico
CREATE TABLE IF NOT EXISTS `topico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidade_negocio_id` int(11) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `detalhes` varchar(255) DEFAULT NULL,
  `icone` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_topicos_unidade_negocio1_idx` (`unidade_negocio_id`),
  CONSTRAINT `fk_topicos_unidade_negocio1` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.topico: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `topico` DISABLE KEYS */;
INSERT INTO `topico` (`id`, `unidade_negocio_id`, `descricao`, `detalhes`, `icone`, `ativo`) VALUES
	(9, 1, 'Segurança', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', 'fa fa-shield', 1),
	(10, 1, 'Especializado', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', 'fa fa-pencil', 1),
	(11, 1, 'Certificado', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', 'fa fa-star', 1),
	(12, 1, 'Qualidade', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', 'fa fa-thumbs-up', 1);
/*!40000 ALTER TABLE `topico` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.unidade_federativa
CREATE TABLE IF NOT EXISTS `unidade_federativa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `sigla` varchar(2) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.unidade_federativa: ~27 rows (aproximadamente)
/*!40000 ALTER TABLE `unidade_federativa` DISABLE KEYS */;
INSERT INTO `unidade_federativa` (`id`, `descricao`, `sigla`, `ativo`) VALUES
	(1, 'Acre', 'AC', 1),
	(2, 'Alagoas', 'AL', 1),
	(3, 'Amapá', 'AP', 1),
	(4, 'Amazonas', 'AM', 1),
	(5, 'Bahia', 'BA', 1),
	(6, 'Ceará', 'CE', 1),
	(7, 'Distrito Federal', 'DF', 1),
	(8, 'Espírito Santo', 'ES', 1),
	(9, 'Goiás', 'GO', 1),
	(10, 'Maranhão', 'MA', 1),
	(11, 'Mato Grosso', 'MT', 1),
	(12, 'Mato Grosso do Sul', 'MS', 1),
	(13, 'Minas Gerais', 'MG', 1),
	(14, 'Pará', 'PA', 1),
	(15, 'Paraíba', 'PB', 1),
	(16, 'Paraná', 'PR', 1),
	(17, 'Pernambuco', 'PE', 1),
	(18, 'Piauí', 'PI', 1),
	(19, 'Rio de Janeiro', 'RJ', 1),
	(20, 'Rio Grande do Norte', 'RN', 1),
	(21, 'Rio Grande do Sul', 'RS', 1),
	(22, 'Rondônia', 'RO', 1),
	(23, 'Roraima', 'RR', 1),
	(24, 'Santa Catarina', 'SC', 1),
	(25, 'São Paulo', 'SP', 1),
	(26, 'Sergipe', 'SE', 1),
	(27, 'Tocantins', 'TO', 1);
/*!40000 ALTER TABLE `unidade_federativa` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.unidade_negocio
CREATE TABLE IF NOT EXISTS `unidade_negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_fantasia` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `endereco` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `cnpj` varchar(50) NOT NULL,
  `titulo_empresa` varchar(200) NOT NULL,
  `descricao_empresa` varchar(200) NOT NULL,
  `detalhes_empresa` varchar(20000) NOT NULL,
  `titulo_produto` varchar(200) NOT NULL,
  `descricao_produto` varchar(200) NOT NULL,
  `servico_titulo` varchar(200) NOT NULL,
  `servico_detalhes` varchar(200) NOT NULL,
  `skype` varchar(200) NOT NULL,
  `face` varchar(200) NOT NULL,
  `insta` varchar(200) NOT NULL,
  `titulo_localizacao` varchar(200) NOT NULL,
  `texto_localizacao` varchar(200) NOT NULL,
  `mapa` varchar(2000) NOT NULL,
  `contato_titulo` varchar(200) NOT NULL,
  `contato_texto` varchar(200) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `usuario_grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.unidade_negocio: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `unidade_negocio` DISABLE KEYS */;
INSERT INTO `unidade_negocio` (`id`, `nome_fantasia`, `email`, `celular`, `telefone`, `endereco`, `cidade`, `cnpj`, `titulo_empresa`, `descricao_empresa`, `detalhes_empresa`, `titulo_produto`, `descricao_produto`, `servico_titulo`, `servico_detalhes`, `skype`, `face`, `insta`, `titulo_localizacao`, `texto_localizacao`, `mapa`, `contato_titulo`, `contato_texto`, `logo`, `ativo`, `usuario_grupo_id`) VALUES
	(1, 'Hidrover', 'guilhermeomott@gmail.com', '99123648', '9955555555', 'Rua Tirando os Dentes', 'Flores Da Cunha - RS', '56545465464654', 'Hidrover', 'Conheça um pouco da nossa história.', 'Quando encontramos um lugar muito limpo, seja onde for,\r\nnão jogamos papel no chão, e cuidamos de não sujar o recinto.\r\nA limpeza por si só já exige respeito, nem precisa de vigia.\r\nAssim também deve ser a sua apresentação.\r\nLimpa, serena e a ao mesmo tempo forte e determinada.\r\nNão apareça com cara de fraqueza, de “coitadinho”.\r\nQuanto mais coitadinho, mais as pessoas pisam.', 'Produtos', 'sabiugdsaiudsaiudsadsahiuhdiusa', 'Serviços', '', 'df', 'dsadsaads', 'dsasadsa', 'Localização', 'https://goo.gl/maps/5RxCva3g7V34iCMQ8', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d27905.442372402962!2d-51.201571!3d-29.041248!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc2559f24b9acd824!2sHidrover%E2%80%93+Equipamentos+Hidr%C3%A1ulicos+Ltda.!5e0!3m2!1spt-BR!2sbr!4v1564599886275!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'Contato', 'dsfdsfd', 'uploads/unidade_negocio//Logo1.png', 1, NULL);
/*!40000 ALTER TABLE `unidade_negocio` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `celular` varchar(200) DEFAULT NULL,
  `tipo` char(1) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `usuario_grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela hidrover_oficial.usuario: ~89 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nome`, `usuario`, `senha`, `email`, `celular`, `tipo`, `ativo`, `usuario_grupo_id`) VALUES
	(3, 'Administrador satrt', 'admin', '$2y$10$S/t5EhqTxIWwkZpJ2gDjW.BS.x9BwwgD75SyUfmZD.cFZT8E0sGi.', 'guilhermemott@hotmail.com', NULL, 'A', 1, 1),
	(13, 'Oscar William Baldin', 'obaldin', '$2y$10$tU2Gns2JTkFmjMaDN347vucYHkmvS3qSYsScS/fm.E7oQduCHr.vm', 'obaldin@guentner.com.br', NULL, 'A', 1, 1),
	(14, 'Fabrício Dedavid', 'fdedavid', '$2y$10$cCS3XPnQDhxMtmoPaQBO5uEpHqEe4bFFVeGJ2yU4sugwsC2n.Jr86', 'fdedavid@guentner.com.br', NULL, 'T', 1, 3),
	(15, 'Herlan Piccoli', 'herlanpiccoli', '$2y$10$.7Ed8OzvSZObqvz9cnvJO.39QcN1/OiYtUEikNWnFjEB9qpT23Hf2', 'herlan.piccoli@guentner.com.br', NULL, 'T', 1, 2),
	(16, 'Tarcisia Rodeghiero Saalfeld', 'tsaalfeld', '$2y$10$aAh5L9hcxuvuT/TH8/SKo.Z1oWm/a1BhGz3.2NGaQRzrw4JBshcV2', 'tsaalfeld@guentner.com.br', NULL, 'T', 1, 2),
	(17, 'Frank Alves da Rosa', 'frankalves', '$2y$10$Z3hPfUgVIi.7Vp.aDHW7aupz4WbJsApj36jDwoqUkorLonS2oeUc2', 'frank.alves@guentner.com.br', NULL, 'T', 1, 2),
	(18, 'Alisson Souza', 'asouza', '$2y$10$unUtQ9vsW4jFd.dDwD0JPeMxxHMZVcadV6lgrlNfE0l7hg387UPSO', 'asouza@guentner.com.br', NULL, 'T', 1, 2),
	(19, 'André Pinheiro', 'apinheiro', '$2y$10$z0dpg6hJvCOvfyK0lEWAT.so9Mmhmq68okJ4CdgNBhmH8wWUziBwi', 'apinheiro@frostfrio.com.br', NULL, 'T', 1, 2),
	(20, 'Aznery Aranda Duarte', 'aaranda', '$2y$10$zGukrDlsEm0eI/fvEtZ9Her6Hr.GPzmdJtkQtlaUv4cjfbNMmlleq', 'aaranda@guentner.com.br', NULL, 'T', 1, 2),
	(21, 'Thainã Leite Garcia', 'thgarcia', '$2y$10$QEXIOAspDa8txPuFlflMNuedenjaQ7ah6QQyhpOq5TJGEf1OvSUFy', 'thgarcia@guentner.com.br', NULL, 'T', 1, 2),
	(22, 'Fabrício Vitorio dos Santos', 'fsantos', '$2y$10$M.qJ.5DIxFplU5TE57u0DO3ecqBL65H4tm7BIIBZlLgb3o8qYLZYS', 'fsantos@guentner.com.br', NULL, 'T', 1, 2),
	(23, 'Rubilar Borges', 'rborges', '$2y$10$FJt5j7fjth2bm.Ql4TFx1OzF3T7EN7d4UNF8dNWEdeRd/oZm/v0k6', 'rborges@guentner.com.br', NULL, 'T', 1, 2),
	(24, 'Débora de Lima Faili', 'deborafaili', '$2y$10$yj.NPd4TUfyK8K9JrXU53uVI1c7..4aKW4KPsLUClr4Ls0NmWeV1S', 'debora.faili@guentner.com.br', NULL, 'T', 1, 2),
	(25, 'Janse Uilton Piaia', 'jpiaia', '$2y$10$0E9y4mf09ku5hpaDfjH1ce1HtBVF4I9ZWLCno49B7ajOCAf5hitmS', 'jpiaia@guentner.com.br', NULL, 'T', 1, 2),
	(26, 'Walter Murback ', 'waltermurback', '$2y$10$zWLw8ulDooGo2.XUksOCk.5eSNEAGz04GDPxntc/Ow9.TkReBeAdm', 'walter.murback@jbs.com.br', NULL, 'T', 1, 2),
	(27, 'Adriano Gonçalves da Silva', 'asilva', '$2y$10$9EuboNOqrTJHmqhlk2jVO.xcFLi78.Nc2aXc1qWn4fQ.UqZ5EEwBS', 'asilva@guentner.com.br', NULL, 'T', 1, 2),
	(28, 'Roberto Carreon', 'robertocarreon', '$2y$10$vzxK/1VDkUowo66YC0GphuApc3QHPnCwj9ZvwLx4ukqlZd5e2fDLS', 'roberto.carreon@guentner.com', NULL, 'T', 1, 2),
	(29, 'Tim Leach', 'timleach', '$2y$10$nltcdtrVUbeJwwWHx97bHejZVob3QCWrJFmEwfRf9RIFopdRWRvU6', 'tim.leach@guentner.com', NULL, 'T', 1, 2),
	(30, 'Osni Roberto Caron Filho', 'ocaron', '$2y$10$Vyc8bQQE7tcrYvxh.Nv7zeTT9Y.5NyOmGSyxTlJmq41WFxnolgAfG', 'ocaron@guentner.com.br', NULL, 'T', 1, 2),
	(31, 'Juan Arias', 'juanarias', '$2y$10$YVSNW72rHlBwJDhiUTnRAOrQNC638u.bWgs2mlrdGvX5loUK3ScC.', 'juan.arias@guentner.com', NULL, 'T', 1, 2),
	(32, 'Yudha Octavian Himawan', 'yudhahimawan', '$2y$10$7lM66fG9WD4O//ZNxvbCBeUMWQdVznIKIjJFz3c2DZ4tpxlj3ajsO', 'yudha.himawan@guentner.com', NULL, 'T', 1, 2),
	(34, 'Marcelo Tissot', 'mtissot', '$2y$10$PSRBUKZaGbNR6IJRkjtEgOx1Bb.ZrvyM4wCMdhaAfscL8I8z6m.vS', 'mtissot@guentner.com.br', NULL, 'T', 1, 2),
	(35, '', '', '', NULL, NULL, '', 1, NULL),
	(36, 'Martin Beermann', 'martinbeermann', '$2y$10$SWQEEuLjGxS29ll3e6jK9O7UPIZoAlK1ksE0CUNXxbSvUKd3io/D6', 'martin.beermann@guentner.com', NULL, 'T', 0, 2),
	(37, 'Thomas Odrich', 'thomasodrich', '$2y$10$R9mfYptefPxWrbJntft9aOUiFRGPBNNeBQ.gKqWqajBPfJ99eSuOW', 'Thomas.Odrich@jaeggi-hybrid.ch', NULL, 'T', 0, 2),
	(38, 'Everardo Gómez Garza', 'everardogomez', '$2y$10$bEujUaM.gMs0dzOrKqhnFuQbgJSGGS6SDyKgjR5SIvelCd5TnYNWe', 'everardo.gomez@guentner.com', NULL, 'T', 0, 2),
	(39, 'Erika Carvajal', 'erikacarvajal', '$2y$10$QZDWhIJ8b5N/7mvacXXn7OV/otLjh5K9Qt2pDd8xBX1FxXC5n.hoC', 'erika.carvajal@guentner.com', NULL, 'T', 0, 2),
	(40, 'Thomas Bercek', 'thomasbercek', '$2y$10$4hUgtKkKeIAecSOjea907.N0YhTPujOd1NLKWZ1MzXNed8.SAE6Qi', 'thomas.bercek@guentner.com', NULL, 'T', 1, 2),
	(41, 'Michael Freiherr', 'michaelfreiherr', '$2y$10$HsZ8a9AngrkecTPQjqcnbudVSoHW5WJ.KhKwBaP.WgPLp7uFcUkn.', 'michael.freiherr@guentner.com', NULL, 'T', 0, 2),
	(42, 'Matthew Rutherford', 'matthewrutherford', '$2y$10$gxhYCnAQD02UgaUHbh5B4.IwGZzRjQni6CXDZUWiO/QnlJqV2GZy.', 'matthew.rutherford@jaeggi-hybrid.ch', NULL, 'T', 0, 2),
	(43, 'Jens Schirmer', 'jensschirmer', '$2y$10$UpVrMrOItwReN6mitRpXMuHOnpMUvMPUP3jEc5BIM8hfw7DbJINtm', 'Jens.Schirmer@jaeggi-hybrid.ch', NULL, 'T', 0, 2),
	(45, 'Alan Pecchio', 'alanpecchio', '$2y$10$IDqldUOz40nWBp5ITD/64O7rc/EsCZqQODiXkk.aPgcrNgfL4qhaC', 'alan.pecchio@gea.com', NULL, 'T', 1, 2),
	(46, 'Darko Capl', 'darkocapl', '$2y$10$wOxGOqpICz2itDjnOoDKnu/l7vJJRwW2OUGwa884IJaT3B8ZsqhGO', 'darko.capl@jaeggi-hybrid.ch', NULL, 'T', 0, 2),
	(47, 'Pedro Bauer', 'pedrobauer', '$2y$10$foRNQoQ8owm6O78jwkMGFeccSE1i2h.J/s3jM8Ht1Q0y7kRv2qc7K', 'pedro.bauer@guentner.com', NULL, 'T', 1, 2),
	(48, 'Ian Runsey', 'ianrunsey', '$2y$10$HwdIGEkMh7qD6oj6SrtcU.cG/scTS2FlOUDm3gZfZk5dWhGEfxroG', 'ian.runsey@guentner.com', NULL, 'T', 1, NULL),
	(50, 'Quang Nguyen', 'quangnguyen', '$2y$10$H.gJCmQSRBgeGxWrJszYCeNgeJz3uUjIMXZVNzgENvJ3pJJ1Eyi5u', 'quang.nguyen@guentner.com', NULL, 'T', 1, 2),
	(51, 'Glen Wiles', 'glenwiles', '$2y$10$rMM02WI0ynwFuzbz6JNuJucFw3Mgs3OVnvJOfb6Xbdxuh9P12nIQS', 'glen.wiles@guentner.com', NULL, 'T', 1, 2),
	(52, 'Aleksey Odintsov', 'alekseyodintsov', '$2y$10$JkGgVo2p8Rm17uP4ObrCQeiMNz5J8wf1jd7q4GQgfiHI5m6MAgPeO', 'aleksey.odintsov@guentner.com', NULL, 'T', 1, 2),
	(53, 'Nikita Shishkin', 'nikitashishkin', '$2y$10$0psMEgjPzHZ9APEHOE/dhOmvq42Fosxq.HsRCusCKVTGp01JS6OS2', 'Nikita.Shishkin@guentner.com', NULL, 'T', 1, 2),
	(54, 'Dmitry Grigoriev', 'dmitrygrigoriev', '$2y$10$91C8pzrKemFXeofix1OGTewSZR8aFxWOnMhfLp9duPeMqz03.D7yK', 'Dmitry.Grigoriev@guentner.com', NULL, 'T', 1, 2),
	(55, 'Alexey Tsepkov', 'alexeytsepkov', '$2y$10$G6CsbpYDXqsQSaFSaxmr5OOTScD5jCYIopC7IOQ8ISFSsLwVHp8oa', 'Alexey.Tsepkov@guentner.com', NULL, 'T', 1, 2),
	(56, 'Alexey Egorov', 'alexeyegorov', '$2y$10$2o7qHIP.kz6txsLSEJUK1O0CeznXWbZroetO3aiDIn6O2eXKZMYnC', 'Alexey.Egorov@guentner.com', NULL, 'T', 1, 2),
	(58, 'Regik Dwi Stiawan', 'regikstiawan', '$2y$10$/ry7ssTISmwtPkA4t3xzbe4Xwrc.Y/nORSCnT9micl39zIc3.kwmi', 'regik.stiawan@guentner.com', NULL, 'T', 1, 2),
	(59, 'Han Piao Ngo', 'hanpiaongo', '$2y$10$NWwpnL8szS3uiLUU/K4X/udbksZ2.idW2DMKX6WtmAaRf/ydWeklW', 'hanpiao.ngo@guentner.com', NULL, 'T', 1, 2),
	(60, 'Andri Ari Sanjaya', 'andrisanjaya', '$2y$10$or10tnGIH88BzbvAgLvuneJiUGcTCKNmEVvBECI/ch8ugaNYGn.HK', 'andriari.sanjaya@guentner.com', NULL, 'T', 1, 2),
	(61, 'GPS Brazil', 'qwer', '$2y$10$iVZgsLBoLCAxceyg4Age2O1Jml37VijBgAZY8IEK0D.w0Qlj2fu4K', 'qwer@guentner.com', NULL, 'T', 1, 2),
	(62, 'Rodolfo de Campos Lima', 'rodolfolima', '$2y$10$F5R7juuV0xu2nvlelv6fJONlOBIFddjnDpOqCfMaYhqGwHeLOgEuO', 'rodolfo.lima@guentner.com', NULL, 'T', 1, 2),
	(63, 'Omar Juárez', 'omarjuarez', '$2y$10$.HHQVDND5Icv4qTBs.4yhOVvc/VvGb6J4UAYYTKSBc7RmxZ0Su/re', 'omar.juarez@guentner.com', NULL, 'T', 1, 2),
	(64, 'Aleksandr Alekseev', 'aleksandralekseev', '$2y$10$RreOOOjGsLDe8cftXB0FGO1zQZJVy0QboGyXUOK/ObT0A7nsX17UK', 'aleksandr.alekseev@guentner.com', NULL, 'T', 1, 2),
	(65, 'Thunkarn Vilaiphorn', 'thunkarnvilaiphorn', '$2y$10$/rWDfErcPOgyy7dr5J1O8uf.9fK/i8Vu1jT2zYNuY2HFeWePr5HCa', 'Thunkarn.Vilaiphorn@guentner.com', NULL, 'T', 1, 2),
	(66, 'Anuj Bedi', 'anujbedi', '$2y$10$0aRfF69eTDJ6RTMJ7FlPuu90D6mRqCbMaBLSvJagZ3Xb/d2T.efZu', 'Anuj.Bedi@guentner.com', NULL, 'T', 1, 2),
	(67, 'Ichwan Satrio Dwi Bisono', 'ichwanbisono', '$2y$10$9WHMghC8USVCmHTLfr5mVuRwFdg5nxRImaU65HmbEnwWpt//in8gC', 'Ichwan.Bisono@guentner.com', NULL, 'T', 1, 2),
	(68, 'Wind Chen', 'windchen', '$2y$10$yP5AQifUXHROsmP/2r3pAe.RcUiES42Rgg/88IB.ldUoH321dHHpi', 'Wind.Chen@guentner.com', NULL, 'T', 1, 2),
	(69, 'Charles Cheng', 'charlescheng', '$2y$10$RDzzxYyAPP.3O89v.ESkBeXUBb2WuOUCqGPPJaHLwYMjpY7MjOIbe', 'Charles.Cheng@guentner.com', NULL, 'T', 1, 2),
	(70, 'Jason Dai', 'jasondai', '$2y$10$jUfFxzFJ6zxGNWOJBTlzC.3v3DLfU5AyCFME2nf6V0b/T0IUCXYGG', 'Jason.Dai@guentner.com', NULL, 'T', 1, 2),
	(71, 'Insan Fadlillah', 'insanfadlillah', '$2y$10$D5ZdzvBs.HEyHsAv9idMkehm8ej85YVMJ4WytqAGRbYBxjUDnquWu', 'Insan.Fadlillah@guentner.com', NULL, 'T', 1, 2),
	(72, 'Wei Guo', 'weiguo', '$2y$10$NunU98CbtfP8KPCLhJ3H8.HM7jzu6wHUtyiDFnMeB3VVOHyoHMtqu', 'Wei.Guo@guentner.com', NULL, 'T', 1, 2),
	(73, 'Dina Gustiani', 'dinagustiani', '$2y$10$cPeZduTg.YSiaQg2fGLdWuOpLv8SQAHCkZjzFoaglwVnvT6cFDOMS', 'Dina.Gustiani@guentner.com', NULL, 'T', 1, 2),
	(74, 'Chuanbin Lian', 'chuanbinlian', '$2y$10$Bd5EJzZJukTvFbjuWzPwGeIJxI6k3RE9ZX/bR/QFnMIB0hCqLv9DO', 'Chuanbin.Lian@guentner.com', NULL, 'T', 1, 2),
	(75, 'Henry Napitupulu', 'henrynapitupulu', '$2y$10$eWGc4OZWL7ZKvpK5TnTZtO9j/DD41rOses6XzKwdlVhlHHOota5p6', 'Henry.Napitupulu@guentner.com', NULL, 'T', 1, 2),
	(76, 'Loc Nguyen', 'locnguyen', '$2y$10$S0JqKTHSgvYvijjgRKBmeOV7V3gO19JVt/I/jM98GQHLnCtSRXL3S', 'Loc.Nguyen@guentner.com', NULL, 'T', 1, 2),
	(77, 'Huss Osman', 'hussosman', '$2y$10$sAJu4w0lIocS3kOD4vDGMuy..ptNRQqRrraR2/Dt2Srgp4WPdk9Wy', 'Huss.Osman@guentner.com', NULL, 'T', 1, 2),
	(78, 'Widjanarko Poerwosunu', 'poerwosunu', '$2y$10$6rFpkZ3.PXMnslUJXHiQmuJAG/uT7548xSJOpe.vUgGKYO06H5/dG', 'Widjanarko.Poerwosunu@guentner.com', NULL, 'T', 1, 2),
	(79, 'Serena Xia', 'serenaxia', '$2y$10$TWS0X16w8uCs58kw3snqz.FgHmkPq4oS3735n9xD6LATYczFUxgme', 'Serena.Xia@guentner.com', NULL, 'T', 1, 2),
	(80, 'Herry Wiyono', 'herrywiyono', '$2y$10$CjT8yVEKZPnXu7FU.aS0fuxtTQ0gkHqp7AImfyCyVOR0MrQLxOiY6', 'Herry.Wiyono@guentner.com', NULL, 'T', 1, 2),
	(81, 'Jocelyn Wang', 'jocelynwang', '$2y$10$xprETLBAeAlxswS2yGRP1.i7cQ1TYECwGTSqB39avMybp2ys20Yfe', 'Jocelyn.Wang@guentner.com', NULL, 'T', 1, 2),
	(82, 'Wei Wang', 'weiwang', '$2y$10$ppeNLpLsueEcpx4GZgt.iubwdW.5MlKvJWAYutKg2g1mWUNOKrt6m', 'Wei.Wang@guentner.com', NULL, 'T', 1, 2),
	(83, 'Patrik Raich', 'patrikraich', '$2y$10$wiy0d.B7GJsPfR.74Hj4VeZ6CB53uee2am/Jc1/CyXkknoL4l4szu', 'Patrik.Raich@guentner.com', NULL, 'T', 1, 2),
	(84, 'Sorrawee Ruengyodngarmlerd', 'sorrawee', '$2y$10$2DDUehThN8v/Evv5eKch0.0gcTvgzpjCAnimIFK9jM2A.T7uGBv5a', 'Sorrawee.Ruengyodngarmlerd@guentner.com', NULL, 'T', 1, 2),
	(85, 'Tomohiro Satake', 'tomohirosatake', '$2y$10$tomaeAyyIqPMBFmqtokhOeabh/Xp1URUdoWVYxPM.ROztCW/AyCcy', 'Tomohiro.Satake@guentner.com', NULL, 'T', 1, 2),
	(86, 'Chatwaroon Somboon', 'chatwaroonsomboon', '$2y$10$xxib8Q78NwCDwal5MV22ou6zB1eeYIFuLk.u0Kh9ZoOv/TRC7aTAC', 'Chatwaroon.Somboon@guentner.com', NULL, 'T', 1, 2),
	(87, 'Triboon Suwathikul', 'triboonsuwathikul', '$2y$10$8ucWzXalYQgWamV9NgpSAupFpApiNn30ySZFQQ.HNRZjRK3HcH1BG', 'Triboon.Suwathikul@guentner.com', NULL, 'T', 1, 2),
	(88, 'Chifrul Syaichudin', 'chifrulsyaichudin', '$2y$10$AL5Lq3nD0LtiE7sIv2q7j.QhUKZFTXKgEr4lRitdMkqY/wrrniL3O', 'Chifrul.Syaichudin@guentner.com', NULL, 'T', 1, 2),
	(89, 'Nho Thuong', 'nhothuong', '$2y$10$iZYA7P.gxyxL1Rystx8sg.N.Ga5oxBRkba0UOenixqiKt57hxjDhW', 'Nho.Thuong@guentner.com', NULL, 'T', 1, 2),
	(90, 'Huy Tran', 'huytran', '$2y$10$S.M7LlKqrqcUNNWRgBfODOk57IU5SPG6ERdH4fHz03RG2ah.k.MUq', 'Huy.Tran@guentner.com', NULL, 'T', 1, 2),
	(91, 'Tri Tran', 'tritran', '$2y$10$LdmNMhAE3WFJti.uEceYa.qb8E4eG1pOOkgtoFN08Cr4YOeXKzLdO', 'Tri.Tran@guentner.com', NULL, 'T', 1, 2),
	(92, 'Tu Tran', 'tutran', '$2y$10$mxdRaGuOvpKaxZC0TIP3RerBL6cn4Bu2YZv3RBU0sN5p3ryYYzSaq', 'Tu.Tran@guentner.com', NULL, 'T', 1, 2),
	(93, 'Rolando Manriquez', 'rolandomanriquez', '$2y$10$c.eobZb/Au.riaGYHUqF0.h.VZiA/C.3zeT.FuV6lv4zs7F9fGrnC', 'rolando.manriquez@guentner.com', NULL, 'T', 1, 2),
	(94, 'David Widjanarko', 'davidwidjanarko', '$2y$10$I68lKfvMw0kNFjIQaOJuhe/zqge.gWXJLgDSHuLCjok3ovHg6rKUW', 'widjanarko.poerwosunu@guentner.com', NULL, 'T', 1, 2),
	(95, 'Gabriel Franzosi', 'gabrielfranzosi', '$2y$10$ksOrJoOBS7MzKg00tzPTOevVeeApXNZ6CV2es6J1jZUyLI1Zp0Yc6', 'gabriel.franzosi@guentner.com', NULL, 'T', 1, 6),
	(96, 'Sawika Jaewsrithon', 'jaewsrithong', '$2y$10$j9iNqAywhcTwapOrDx4YXupMXjXKNHNRQgMoW8Ml47gfSQZl7rR2G', 'sawika.jaewsrithong@guentner.com', NULL, 'T', 1, 2),
	(97, 'Aguk Haryono', 'aguk', '$2y$10$40F/CSn3p6rZlnQdkN3.QuiFdTqUuqyA73D9CW0SjZxR7BaXvrzg6', 'Aguk.Haryono@guentner.com', NULL, 'T', 1, 2),
	(98, 'William Crave', 'williamcrave', '$2y$10$tJkJ0QgEF/x2BTfjiA3a/.DNkY6oE7kvD0ME5tUbYOR5pdsODk5jK', 'William.Crave@guentner.com', NULL, 'T', 1, 2),
	(99, 'Luqman Syed', 'luqmansyed', '$2y$10$yuVUrGpnasZMxhY2Wmj/tuS9dT/8rHR2BuDkodRX4RzcYuwWHdHmu', 'Luqman.Syed@guentner.com', NULL, 'T', 1, 2),
	(100, 'Shin Youngsu', 'shinyoungsu', '$2y$10$fl9vWtwuWic3R75kwq1WNulyl8mMrJZ8GjkN7DqHmMR72NWRADQ0a', 'Shin.Youngsu@guentner.com', NULL, 'T', 1, 2),
	(101, 'Leonardo Netto', 'leonardonetto', '$2y$10$IC1fQJXFSzAmwUCcc7TOQe2PuIB/oU3HkPxogV07rCM2lZGTCysnC', 'Leonardo.Netto@guentner.com', NULL, 'T', 1, 2),
	(102, 'Frank Drogi', 'frankdrogi', '$2y$10$O9D76rSL3C9A6qGQ1nndCunMcU9YWh50PHnKm7ARWGGDjsLzi9r3a', 'frank.drogi@guentner.com', NULL, 'T', 1, 2),
	(104, 'teste', 'teste', '$2y$10$FPGVXQXM880hUbE1HVRvFuRbNhu8HIY1/ixvkwfFhRDbgrp5d/U16', 'guilhermeomott@gmail.com', '5499999999', 'A', 1, 1),
	(105, 'Guilherme Mott', 'sadasdsadasd11122', '', 'mott2@coopertecsolucoes.com.br', '474772772', 'A', 1, 1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.usuario_grupo
CREATE TABLE IF NOT EXISTS `usuario_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.usuario_grupo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_grupo` DISABLE KEYS */;
INSERT INTO `usuario_grupo` (`id`, `descricao`, `ativo`) VALUES
	(1, 'Master Administrador', 1);
/*!40000 ALTER TABLE `usuario_grupo` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.usuario_grupo_menu
CREATE TABLE IF NOT EXISTS `usuario_grupo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_grupo_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_grupo_usuario_fk1` (`usuario_grupo_id`),
  KEY `menu_menu_fk1` (`menu_id`),
  CONSTRAINT `menu_grupo_usuario_fk1` FOREIGN KEY (`usuario_grupo_id`) REFERENCES `usuario_grupo` (`id`),
  CONSTRAINT `menu_menu_fk1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1416 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.usuario_grupo_menu: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_grupo_menu` DISABLE KEYS */;
INSERT INTO `usuario_grupo_menu` (`id`, `usuario_grupo_id`, `menu_id`) VALUES
	(1398, 1, 56),
	(1399, 1, 16),
	(1400, 1, 25),
	(1401, 1, 40),
	(1402, 1, 57),
	(1403, 1, 20),
	(1404, 1, 3),
	(1405, 1, 50),
	(1406, 1, 2),
	(1407, 1, 37),
	(1408, 1, 28),
	(1409, 1, 38),
	(1410, 1, 1),
	(1411, 1, 14),
	(1412, 1, 55),
	(1413, 1, 15),
	(1414, 1, 4),
	(1415, 1, 5);
/*!40000 ALTER TABLE `usuario_grupo_menu` ENABLE KEYS */;

-- Copiando estrutura para tabela hidrover_oficial.usuario_unidade
CREATE TABLE IF NOT EXISTS `usuario_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL DEFAULT '0',
  `unidade_negocio_id` int(11) NOT NULL DEFAULT '0',
  `padrao` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Usuario_Usuario_unidade_FK1` (`usuario_id`),
  KEY `Unidade_Usuario_unidade_FK2` (`unidade_negocio_id`),
  CONSTRAINT `Unidade_Usuario_unidade_FK2` FOREIGN KEY (`unidade_negocio_id`) REFERENCES `unidade_negocio` (`id`),
  CONSTRAINT `Usuario_Usuario_unidade_FK1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela hidrover_oficial.usuario_unidade: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_unidade` DISABLE KEYS */;
INSERT INTO `usuario_unidade` (`id`, `usuario_id`, `unidade_negocio_id`, `padrao`) VALUES
	(3, 3, 1, 1);
/*!40000 ALTER TABLE `usuario_unidade` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
