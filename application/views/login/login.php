<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Correa Couto Painel </title>
  <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.png")?>" type="image/x-icon" />
  <!-- ================== APP CSS START ================== -->
  <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css")?>" />
  <!-- ================== APP CSS END ================== -->
       <script type="text/javascript" src="<?php echo base_url("assets/vendor/validator/validator.js"); ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/vendor/validation/css/validationEngine.jquery.css"); ?>">

  <script src="<?php echo base_url("assets/vendor/validation/js/jquery.validationEngine.js"); ?>"></script>
<script src="<?php echo base_url("assets/vendor/validation/js/languages/jquery.validationEngine-pt_BR.js"); ?>"></script> 


</head>

<body>
  <div class="auth-container d-flex align-items-center">
    <div class="col-lg-4 mx-auto">
      <div class="auth-form">
             <img src="<?php echo base_url('assets/images/logo.png')?>" alt="Vision Painel"> 
        <form id="formulario" data-parsley-validate data-toggle="validator" id="validation-form"  method="POST" action="<?php echo base_url("login/login"); ?>">
          <div class="form-group">
            <label class="label">Usuário/Email</label>
            <div class="input-group">
                <input type="text" class="form-control" id="usuario" name="usuario" required placeholder="Usuário">
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                    </span>
                </div>
                <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
                <label class="label">Senha</label>
                <div class="input-group">
                    <input type="password" id="senha" name="senha" class="form-control" required placeholder="*********">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                      </span>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Login</button>
          </div>
          <div class="form-group d-flex justify-content-between">
                <a href="#" class="text-small forgot-password text-black">Forgot Password</a>
          </div>
         
        </form>
      </div>
    </div>
  </div>
</body>

</html>