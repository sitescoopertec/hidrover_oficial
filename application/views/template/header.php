<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ($_SESSION['usuario_id'] == null) {
    redirect('/login/login');
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, maximum-scale=1">
    <title> Vision Painel </title>
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.png")?>" type="image/x-icon" />
    <!-- ================== BEGIN PAGE LEVEL CSS START ================== -->
    <link href="<?php echo base_url("assets/vendor/data-table/css/jquery.dataTables.min.css")?>" />
    <link href="<?php echo base_url("assets/vendor/data-table/css/buttons.dataTables.min.css")?>" />
    <link href="<?php echo base_url("assets/vendor/data-table/css/dataTables.bootstrap.min.css")?>" />

    <link href="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/css/responsive.bootstrap4.min.css"); ?>" rel="stylesheet" type="text/css" />

      <link rel="stylesheet" href="<?php echo base_url("assets/vendor/bootstrap-select/css/bootstrap-select.min.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/bootstrap-multiselect/css/bootstrap-multiselect.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/select2/css/select2.min.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/bootstrap-taginput/css/bootstrap-tagsinput.css")?>" />
    <!-- ================== BEGIN PAGE LEVEL END ================== -->

  
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/bootstrap-daterangepicker/css/daterangepicker.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/minicolors/css/jquery.minicolors.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/pignose-calender/css/pignose.calendar.min.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/vendor/fullcalender/css/fullcalendar.min.css")?>" />
    
     <!-- ================== BEGIN PAGE LEVEL CSS START ================== -->
    <link rel="stylesheet" href="<?php echo base_url("assets/assets/vendor/owl-carousel/css/owl.carousel.min.css")?>" />
    <link rel="stylesheet" href="<?php echo base_url("assets/assets/vendor/owl-carousel/css/owl.theme.default.min.css")?>" />
  
  
    <!-- ================== APP CSS START ================== -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css")?>" />
    <!-- ================== APP CSS END ================== -->


    <script src="<?php echo base_url("assets/vendor/jQuery/js/jquery-3.3.1.js")?>"></script>
    
    <link rel="stylesheet" href="<?php echo base_url("assets/css/jquery.edittreetable.css")?>" />
        
</head>

<body>
    

    
    <div id="wrapper">

    <!-- Header Start -->
    <header class="top-bar flex-row align-items-center">

      <!-- Logo  -->
      <div class="nav-brand">
        <div class="hamburg-icon">
          <a href="javascript:void(0)" class="nav-link">
            <i class="mdi mdi-menu md-24"></i>
          </a>
        </div>
          </div>
        <div class="navbar-header">
          <h2 class="mb-0">
            <a class="navbar-brand" href="<?php echo base_url('login/home')?>">
              <img src="<?php echo base_url('assets/images/logo.png')?>" alt="Vision Painel">
            </a>
          </h2>
          
        </div>
   
      
      <!-- Logo End  -->

      <!-- Right Nav -->
      <div class="navbar-container">
        <div class="right-bar align-self-center">
            <a class="btn btn-small btn-primary" href="<?php echo base_url('servico/ordem_servico/index')?>">Ordens Serviços</a>
            <!--<a class="btn btn-small btn-primary">Atendimentos</a>-->
        </div>
        <button class="d-md-none three-dots">
          <i class="mdi mdi-dots-vertical md-24"></i>
        </button>
        <div class="notifications">
          <ul>
     
            <li>
              <a href="javascript:void(0)" data-toggle="dropdown" class="nav-link" id="userProfile" aria-haspopup="true"
                aria-expanded="false">
                <img src="../../assets/images/users-thumbs/user-1.jpg" width="24" alt="user" class="rounded-circle align-middle" />
              </a>
              <div class="dropdown-menu" aria-labelledby="userProfile">
                <ul>
                  <li>
                    <a href="javascript:void(0)">
                      Meus dados
                    </a>
                  </li>
             
                  <li>
                    <a href="<?php echo base_url("sistema/usuario/senha")?>">
                      Trocar Senha
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("login/logout")?>">
                      Sair
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </div><!-- Right Nav End -->

    </header><!-- Header End -->

    <!-- Content Wrapper Start -->
    <div class="main-wrapper" id="main-wrapper">

      <!-- Left Navigation Start -->
        <aside class="main-header">
            <!--<img class="bg-barra" src="<?php echo base_url("assets/images/bg-barra.jpg")?>">-->
            <nav class="navbar">
                <ul class="navbar-nav">
                    <?php echo $_SESSION['menu'] ?>
                </ul>
            </nav>
        </aside> <!-- Left Navigation End -->

      <!-- Main Start -->
      <main>
    <!-- Content Start -->
    <div class="content-wrapper">
        <?php
        if ($this->session->flashdata('alerta_sucesso')) {
            ?>    
            <div id="notificacao_sucesso" class="alert bg-success text-white alert-dismissible fade show" >
                
               <?php print $this->session->flashdata('alerta_sucesso')?> 
            </div>
        
        <script>
            setTimeout(function () {
			$("#notificacao_sucesso").remove();
		}, 3000);
        </script>
        <?php }?>  
        
        <?php
        if ($this->session->flashdata('alerta_erro')) {
            ?>    
            <div id="notificacao_erro" class="alert bg-danger text-white alert-dismissible fade show" >
                
               <?php print $this->session->flashdata('alerta_erro')?> 
            </div>
        
        <script>
            setTimeout(function () {
			$("#notificacao_erro").remove();
		}, 3000);
        </script>
        <?php }?> 
        