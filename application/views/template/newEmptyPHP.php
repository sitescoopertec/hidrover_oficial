<li class="nav-item ">
    <a class="nav-link has-submenu waves-effect menu_modulo" href="#">
        <i class="fa fa-gears md-18 mr-2"></i><span class="menu-title ">Geral</span>
    </a>
    <div class="sub-menu level1">
        <ul> 
            <li class="nav-item">
                <a class="nav-link has-submenu" href="#">
                    <i class=" md-18 mr-2"></i>Control Panel
                </a>
                    <div class="sub-menu level2">
                        <ul>
                            <li>
                                <a class="nav-link" href="tipo_menu/index"><i class="md-16 mr-2"></i>Menu Type</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a class="nav-link" href="menu/index"><i class="md-16 mr-2"></i>Menu</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a class="nav-link" href="usuario_grupo/index"><i class="md-16 mr-2"></i>User Group</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a class="nav-link" href="usuarios/index"><i class="md-16 mr-2"></i>Users</a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a class="nav-link" href="unidade_negocio/index"><i class="md-16 mr-2"></i>Business Subsidiary</a>
                            </li>
                        </ul>  
                </div> </li> </ul>
                        
                    </div> 
            </li>