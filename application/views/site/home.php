<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <?php
        if (isset($painel)) {
            $primeiro = true;
            foreach ($painel as $row) {
                if ($primeiro) {
                    $primeiro = false;
                    ?> 
        <div class="item active" style="background: url('<?php echo base_url($row->banner)?>') no-repeat;">
            <div class="container">
                <div class="carousel-caption">
                    <h3><?php echo $row->titulo?></h3>
                    <p><?php echo $row->texto?></p>
                </div>
            </div>
        </div>
        <?php } else {
        ?>   
        <div class="item" style="background: url('<?php echo base_url($row->banner)?>') no-repeat;">
            <div class="container">
                <div class="carousel-caption">
                    <h3><?php echo $row->titulo?></h3>
                    <p><?php echo $row->texto?></p>
                </div>
            </div>
        </div>
        <?php }}}?>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!-- The Modal -->
</div>
<!--//banner -->
<!--/ab-->
<div class="banner_bottom" id="sobre">
    <div class="container">
        <h3 class="tittle-w3ls">Quem Somos</h3>
        <div class="inner_sec_info_wthree_agile">
            <div class="help_full">

                <div class="col-md-6 banner_bottom_grid help">
                    <img src="<?php echo base_url("assets/modelo_site/images/fachada.jpg")?>" alt=" " class="img-responsive">
                </div>
                <div class="col-md-6 banner_bottom_left">
                    <h4><?php echo $empresa->titulo_empresa?></h4>
                    <p><?php echo $empresa->detalhes_empresa?></p>
                    
                    <div class="ab_button">
                        <a class="btn btn-primary btn-lg hvr-underline-from-left" href="<?php echo base_url('site/historia')?>" role="button">Saiba Mais </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
<!--//ab-->
<!--/what-->
<div class="works">
    <div class="container">

        <div class="inner_sec_info_wthree_agile">
            <div class="ser-first">
                <?php
                    if (isset($topicos)) {
                        foreach ($topicos as $row) {
                    ?>
                <div class="col-md-3 ser-first-grid text-center">
                    <span class="<?php echo $row->icone?>" aria-hidden="true"></span>
                    <h3><?php echo $row->descricao?></h3>
                    <p><?php echo $row->detalhes?></p>
                </div>
                    <?php }} ?>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
<!--//what-->
<!--/banner_bottom-->

<!--//banner_bottom-->
<!--/projects-->
<div class="banner_bottom proj" id="produto">
    <div class="wrap_view">
        <h3 class="tittle-w3ls">Produtos</h3>
        <div class="inner_sec">
            <ul class="portfolio-categ filter">
                <li class="port-filter all active">
                    <a href="#">Todas</a>
                </li>
                <?php
                if (isset($categorias)) {
                    foreach ($categorias as $row) {
                ?>
                <li class="<?php echo "cat-item-" . $row->id?>">
                    <a href="#" title="<?php echo $row->descricao?>"><?php echo $row->descricao?></a>
                </li>
                <?php }}?>
            </ul>


            <ul class="portfolio-area">
                <?php
                if (isset($galerias)) {
                    foreach ($galerias as $row) { 
                ?>
                <li class="portfolio-item2"  data-id="<?php echo "id-" . $row->id?>" data-type="<?php echo "cat-item-" . $row->categoria_id?>">
                    <div>
                        <span class="image-block img-hover">
                            <a class="image-zoom" href="<?php echo base_url($row->banner)?>" rel="prettyPhoto[gallery]">

                                <img src="<?php echo base_url($row->banner)?>" class="img-responsive" alt="Conceit">
                                <div class="port-info">
                                    <h5><?php echo $row->descricao?></h5>
                                    <p><?php echo $row->detalhe?></p>
                                </div>
                            </a>
                        </span>
                    </div>
                </li>
                <?php }}?>
                <div class="clearfix"></div>
            </ul>
            <!--end portfolio-area -->

        </div>

    </div>
</div>

<!--//projects-->

<!-- /newsletter-->
<!-- <div class="newsletter_w3ls_agileits">
        <div class="col-sm-6 newsleft">
    <h3>Entre em contato conosco </h3>
    <input type="submit" value="Submit">
        </div>
        <div class="col-sm-6 newsright">
                <form action="#" method="post">
                        <h3>Entre em contato conosco </h3>
                        <input type="submit" value="Submit">
                </form>
        </div>

        <div class="clearfix"></div>
</div> -->
<!-- //newsletter-->

<div class="banner_bottom" id="servico">
    <div class="container">
        <h3 class="tittle-w3ls"><?php echo $empresa->servico_titulo?></h3>
        <div class="inner_sec_info_wthree_agile">
            <?php
            if (isset($servicos)) {
                foreach ($servicos as $row) {
            ?>
            <div class="col-md-3 team_grid_info">
                <img src="<?php echo base_url($row->banner)?>" alt=" " class="img-responsive" />
                <h3><?php echo $row->titulo?></h3>
                <p><?php echo $row->texto?></p>
            </div>
            <?php }}?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="banner_bottom proj" id="contato">
    <div class="container">

        <div class="mail_form">
            <h3 class="tittle-w3ls"><?php echo $empresa->contato_titulo?></h3>
            <div class="inner_sec_info_wthree_agile">
                <form action="#" method="post">
                    <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Nome" type="text" id="input-13" placeholder=" " required="" />
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Name">Nome</span>
                        </label>
                    </span>
                    <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Email" type="email" id="input-14" placeholder=" " required="" />
                        <label class="input__label input__label--chisato" for="input-14">
                            <span class="input__label-content input__label-content--chisato" data-content="Email">E-mail</span>
                        </label>
                    </span>
                    <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Assunto" type="text" id="input-15" placeholder=" " required="" />
                        <label class="input__label input__label--chisato" for="input-15">
                            <span class="input__label-content input__label-content--chisato" data-content="Subject">Assunto</span>
                        </label>
                    </span>
                    <textarea name="Message" placeholder="Seu comentário..." required=""></textarea>
                    <input type="submit" value="Enviar">
                </form>

            </div>
        </div>
        <div class="inner_sec_info_wthree_agile">
            <div class="col-md-8 map">
                <?php echo $empresa->mapa?>
            </div>
            <div class="col-md-4 contact_grids_info">
                <?php
                if (isset($empresas)) {
                    foreach ($empresas as $row) {
                ?>
                <div class="contact_grid">
                    <div class="contact_grid_right">
                        <h4> <?php echo $row->nome_fantasia?></h4>
                        <p><?php echo $row->endereco?>,</p>
                        <p><?php echo $row->cidade?></p>
                    </div>
                </div>
                <?php }}?>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
