<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!--/ab-->
<div class="banner_bottom" id="sobre" style="margin-top: 10%;">
    <div class="container">
        <h3 class="tittle-w3ls">Quem Somos</h3>
        <div class="inner_sec_info_wthree_agile">
            <div class="help_full">

                <div class="col-md-6 banner_bottom_grid help">
                    <img src="<?php echo base_url("assets/modelo_site/images/fachada.jpg") ?>" alt=" " class="img-responsive">
                </div>
                <div class="col-md-6 banner_bottom_left">
                    <h4><?php echo $empresa->titulo_empresa ?></h4>
                    <p><?php echo $empresa->detalhes_empresa ?></p>

                    <!--                    <div class="ab_button">
                                            <a class="btn btn-primary btn-lg hvr-underline-from-left" href="#" role="button">Saiba Mais </a>
                                        </div>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="main-timeline4">
                <?php
                    if (isset($historias)) {
                        foreach ($historias as $row) {
                    ?>
<!--                <div class="timeline">
                    <a href="#" class="timeline-content">
                        <span class="year"><?php echo $row->ano?></span>
                        <div class="inner-content">
                            <h3 class="title"><?php echo $row->descricao?></h3>
                            <p class="description">
                                <?php echo $row->texto?>
                            </p>
                        </div>
                    </a>
                </div>-->
  <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year"><?php echo $row->ano?></span>
                                <div class="inner-content">
                                    <h3 class="title"><?php echo $row->descricao?></h3>
                                    <p class="description"><?php echo nl2br($row->texto) ?> </p>
                                </div>
                            </a>
                        </div>
                <?php }}?>
         
            </div>
        </div>
    </div>
</div>



 <div class="container">
            <h4>Timeline Style : Demo-4</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-timeline4">
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2018</span>
                                <div class="inner-content">
                                    <h3 class="title">Web Designer</h3>
                                    <p class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ex odio, rhoncus sit amet tincidunt eu, suscipit a orci. In suscipit quam eget dui auctor.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2017</span>
                                <div class="inner-content">
                                    <h3 class="title">Web Developer</h3>
                                    <p class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ex odio, rhoncus sit amet tincidunt eu, suscipit a orci. In suscipit quam eget dui auctor.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2016</span>
                                <div class="inner-content">
                                    <h3 class="title">Web Designer</h3>
                                    <p class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ex odio, rhoncus sit amet tincidunt eu, suscipit a orci. In suscipit quam eget dui auctor.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2015</span>
                                <div class="inner-content">
                                    <h3 class="title">Web Developer</h3>
                                    <p class="description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ex odio, rhoncus sit amet tincidunt eu, suscipit a orci. In suscipit quam eget dui auctor.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>