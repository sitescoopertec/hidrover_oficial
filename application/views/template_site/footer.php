<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- footer -->
<div class="footer">
    <div class="footer_inner_info_w3ls_agileits">
        <div class="col-md-3 footer-left">
            <h2><a href="index.html"><i class="fa fa-gears" aria-hidden="true"></i> <?php echo $empresa->nome_fantasia?> </a></h2>
            <p><?php echo $empresa->detalhes_empresa?></p>
            <ul class="social-nav model-3d-0 footer-social social two">
                <li>
                    <?php 
                    if ($empresa->face <> "") { 
                        ?>
                    <a target="_blank" href="<?php echo $empresa->face?>" class="facebook">
                        <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                        <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                    </a>
                    <?php } ?>
                </li>
                <li>
                    <?php 
                    if ($empresa->insta <> "") { 
                        ?>
                    <a target="_blank" href="<?php echo $empresa->insta?>" class="instagram">
                        <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                        <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                    </a>
                    <?php } ?>
                </li>				
            </ul>
        </div>
        <div class="col-md-9 footer-right">
            <div class="sign-grds">
                <div class="col-md-4 sign-gd">
                    <h4>Menus</h4>
                    <ul>
                        <li><a href="#sobre">Quem Somos?</a></li>
                        <li><a href="#produto">Produtos</a></li>
                        <li><a href="#servico">Serviços</a></li>
                        <li><a href="#contato">Contato</a></li>
                    </ul>
                </div>

                <div class="col-md-5 sign-gd-two">
                    <h4><?php echo $empresa->contato_titulo?></h4>
                    <div class="address">
                        <div class="address-grid">
                            <div class="address-left">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <div class="address-right">
                                <h6>Telefone</h6>
                                <p>+55 <?php echo $empresa->telefone?></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="address-grid">
                            <div class="address-left">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            <div class="address-right">
                                <h6>Email</h6>
                                <p><a href="mailto:<?php echo $empresa->email?>"> <?php echo $empresa->email?></a></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="address-grid">
                            <div class="address-left">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                            <div class="address-right">
                                <h6>Endereço</h6>
                                <p><?php echo $empresa->endereco?>, <br>
                                    <?php echo $empresa->cidade?>

                                </p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <p class="copy-right">&copy 2019 <?php echo $empresa->nome_fantasia?>. Todos direitos reservados <br> Desenvolvido por <a target="_blank" href="https://www.coopertecsolucoes.com.br">Coopertec Soluções</a></p>
        
    </div>
</div>
</div>
<!-- //footer -->
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery-2.2.3.min.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/bootstrap.js")?>"></script>
<script>
    $('ul.dropdown-menu li').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
</script>

<!-- js -->
<!-- Smooth-Scrolling-JavaScript -->
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/easing.js")?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/move-top.js")?>"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll, .navbar li a, .footer li a").click(function (event) {
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- //Smooth-Scrolling-JavaScript -->
<script type="text/javascript">
    $(document).ready(function () {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear' 
         };
         */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
<script type="text/javascript" src="<?php echo base_url("assets/modelo_site/js/jquery-1.7.2.js")?>"></script>
<script src="<?php echo base_url("assets/modelo_site/js/jquery.quicksand.js")?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/modelo_site/js/script.js")?>" type="text/javascript"></script>
<script src="<?php echo base_url("assets/modelo_site/js/jquery.prettyPhoto.js")?>" type="text/javascript"></script>
<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->