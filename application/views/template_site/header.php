<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html class="no-js " lang="en">
    <head>
        <title>Hidrover</title>

        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
            window.scrollTo(0, 1);
            }
        </script>
        <!--//tags -->
        <link href="<?php echo base_url("assets/modelo_site/css/bootstrap.css")?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url("assets/modelo_site/css/style.css")?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url("assets/modelo_site/css/prettyPhoto.css")?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url("assets/modelo_site/css/team.css")?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url("assets/modelo_site/css/contact.css")?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url("assets/modelo_site/css/font-awesome.css")?>" rel="stylesheet">
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    </head>

    <body>
        <div class="top_header" id="home">
            <!-- Fixed navbar -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="nav_top_fx_w3ls_agileinfo">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                                aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo-w3layouts-agileits">
                            <h1> <a class="navbar-brand" href="<?php echo base_url('site/index')?>"><img src="<?php echo base_url($empresa->logo)?>" alt=" " class="img-responsive"></a></h1>
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div class="nav_right_top">
                            <ul class="nav navbar-nav navbar-right">
                                <?php 
                                if ($empresa->face <> "") { 
                                    ?>
                                <li><a target="_blank" href="<?php echo $empresa->face?>"><i class="fa fa-facebook"></i></a></li>
                                <?php } ?>
                                
                                <?php 
                                if ($empresa->insta <> "") { 
                                    ?>
                                <li><a target="_blank" href="<?php echo $empresa->insta?>"><i class="fa fa-instagram"></i></a></li>
                                <?php } ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PT <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">PT</a></li>
                                        <li><a href="#">EN</a></li>
                                        <li><a href="#">ES</a></li>

                                    </ul>
                                </li>

                            </ul>
                            <ul class="nav navbar-nav">
                                <!-- <li class="active"><a href="index.html">Home</a></li> -->
                                <li><a href="#sobre">Quem Somos?</a></li>
                                <li><a href="#produto">Produtos</a></li>
                                <li><a href="#servico">Serviços</a></li>
                                <li><a href="#contato">Contato</a></li>
                                <!-- <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                                <li><a href="#">Projects</a></li>
                                                <li><a href="404.html">Services</a></li>


                                        </ul>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </nav>
        </div>