<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historia extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Historia_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de História';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','historia');
            $data->result = $this->Historia_model->retorna_historias($_SESSION["uni_negocio_id"]);
            $this->load->template('historia/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'História - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','historia');
            $this->load->template('historia/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'História';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','historia');
            $data->result = $this->Historia_model->retorna_historia($id);
            $this->load->template('historia/edit',$data);
	}
        
        public function imagens($id) {
            $data = new stdClass();
            $this->load->library('form_validation');
            $data->result = $this->Historia_model->retorna_historia($id);
            $data->page_title = 'Historia - ' . $data->result->nome . ' - Fotos';
            $this->load->template('historia/imagens', $data);
        }

        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->texto = $this->input->post('texto');
            $class->ano = $this->input->post('ano');
            $class->ativo = 1; //Por padrão no create é ativo =1
            

            if ($this->Historia_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/historia/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/historia/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->texto = $this->input->post('texto');
            $class->ano = $this->input->post('ano');
//            //UPLOAD DO BANNER
//            if (!is_dir('./uploads/historia/' . $id)) {
//                mkdir('./uploads/historia/' . $id, 0777);
//            }
//
//            // upload
//            if (!empty($_FILES['userfile']['name'])) {
//                $config['upload_path'] = './uploads/historia/' . $id;
//                $config['allowed_types'] = 'jpg|png';
//                $config['max_size'] = '10000';
//                $config['overwrite'] = FALSE;
//                $config['remove_spaces'] = TRUE;
//                $new_name = normalizeChars($_FILES["userfile"]['name']);
//                $config['file_name'] = $new_name;
//
//                $this->load->library('upload', $config);
//
//                if (!$this->upload->do_upload()) {
//                    $data->error = $this->upload->display_errors();
//                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
//                    redirect('historia/edit/' . $id);
//                } else {
//                    $class->banner = 'uploads/historia/'. $id . '/' . $this->upload->file_name;
//                    
//                }
//            }
            if ($this->Historia_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/historia/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/historia/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Historia_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Historia_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/historia/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/historia/index');
            }
        }
        
        public function update_imagem() {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');

            $id = $this->input->post('id');
            $principal = $this->input->post('principal');
            // set validation rules
            $this->form_validation->set_rules('id', 'ID Historia', 'required');
            $this->form_validation->set_rules('principal', 'Principal', 'required');

            if ($this->form_validation->run() === false) {

                // validation not ok, send validation errors to the view
                $data->result = $this->Historia_model->retorna_historia($id);
                $data->page_title = 'Historia - Mensagens';
                redirect('historia/imagens/' . $id);
            } else {

                // set variables from the form
                $class = new stdClass();

                if (!is_dir('./uploads/historia/' . $id)) {
                    mkdir('./uploads/historia/' . $id, 0777);
                }

                // upload
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/historia/' . $id;
                    $config['allowed_types'] = 'jpg|png';
                    $config['max_size'] = '10000';
                    $config['overwrite'] = FALSE;
                    $config['remove_spaces'] = TRUE;
                    $new_name = normalizeChars($_FILES["userfile"]['name']);
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {
                        $data->error = $this->upload->display_errors();
                        $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                        redirect('historia/imagens/' . $id);
                    } else {
                        $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                        $class->extensao = $this->upload->file_ext;
                    }
                }
                // fim upload
                // insere foto no BD
                $class->historia_id = $id;
                $class->principal = $this->input->post('principal');

                if (!$this->Historia_foto_model->create_historia_foto($class)) {
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

                }
                 redirect('historia/imagens/' . $id);

            }
        }
    
    public function delete_imagem($id) {
        $foto = $this->Historia_foto_model->retorna_historia_foto($id);
        if ($this->Historia_foto_model->delete_historia_foto($foto->id)) {
            unlink("./uploads/historia/" . $foto->historia_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('historia/imagens/' . $foto->historia_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('historia/imagens/' . $foto->historia_id);
        }
    }
        
        
        
        
}
