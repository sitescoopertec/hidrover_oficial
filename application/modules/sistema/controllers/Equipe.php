<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipe extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Equipe_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Equipes';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','equipe');
            $data->result = $this->Equipe_model->retorna_equipes($_SESSION["uni_negocio_id"]);
            $this->load->template('sistema/equipe/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Equipes - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','equipe');
            $this->load->template('sistema/equipe/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Equipes';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','equipe');
            $data->result = $this->Equipe_model->retorna_equipe($id);
            $this->load->template('sistema/equipe/edit',$data);
	}
              
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');
            $class->especialidade = $this->input->post('especialidade');
            $class->descricao_curriculum = $this->input->post('descricao_curriculum');

            $class->ativo = 1; //Por padrão no create é ativo =1
            
            

             if ($this->Equipe_model->create($class)) {
                // OK
                $id = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/equipe/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/equipe/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');
            $class->especialidade = $this->input->post('especialidade');
            $class->descricao_curriculum = $this->input->post('descricao_curriculum');
            
             //UPLOAD DO BANNER
            if (!is_dir('./uploads/equipe/' . $id)) {
                mkdir('./uploads/equipe/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/equipe/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('sistema/equipe/edit/' . $id);
                } else {
                    $class->foto = 'uploads/equipe/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            
            if ($this->Equipe_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/equipe/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/equipe/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Equipe_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Equipe_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/equipe/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/equipe/index');
            }
        }

}
