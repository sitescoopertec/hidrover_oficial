<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cidade extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Unidade_federativa_model');
            $this->load->model('Cidade_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Cidades';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','cidade');
            $data->result = $this->Cidade_model->retorna_cidades();
            $this->load->template('cidade/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Cidades - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','cidade');
            //Busca os dados da View
            $data->ufs = $this->Unidade_federativa_model->retorna_unidade_federativa_ativos();
            
            $this->load->template('cidade/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Cidades';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','cidade');
            //Busca os dados da View
            $data->ufs = $this->Unidade_federativa_model->retorna_unidade_federativa_ativos();
            $data->result = $this->Cidade_model->retorna_cidade($id);
            $this->load->template('cidade/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');               
            $class->unidade_federativa_id = $this->input->post('unidade_federativa_id'); 
            $class->ativo = 1; //Por padrão no create é ativo =1
            if ($this->Cidade_model->create_cidade($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/cidade/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/cidade/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');               
            $class->unidade_federativa_id = $this->input->post('unidade_federativa_id'); 
            
            if ($this->Cidade_model->update_cidade($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/cidade/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/cidade/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Cidade_model->update_cidade($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {
            if ($this->Cidade_model->delete_cidade($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/cidade/index');
            } else {         
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/cidade/index');
            }
        }
        
        public function carrega_cidades_estado($uf_id){   
        
            $cidades = $this->Cidade_model->retorna_cidades_estado($uf_id);

            if( empty ( $cidades ) ) 
                return '{ "descricao": "Nenhuma Cidade encontrada" }';         
            echo json_encode($cidades); 
            return;  
        }
}
