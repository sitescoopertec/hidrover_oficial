<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Servico_model');
            $this->load->model('Servico_foto_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Serviço';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','servico');
            $data->result = $this->Servico_model->retorna_servicos($_SESSION["uni_negocio_id"]);
            $this->load->template('servico/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Serviço - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','servico');
            $this->load->template('servico/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Serviço';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','servico');
            $data->result = $this->Servico_model->retorna_servico($id);
            $this->load->template('servico/edit',$data);
	}
        
        public function imagens($id) {
            $data = new stdClass();
            $this->load->library('form_validation');
            $data->result = $this->Servico_model->retorna_servico($id);
            $data->foto_principal = $this->Servico_foto_model->retorna_servico_foto_principal($id);
            $data->page_title = 'Servico - ' . $data->result->nome . ' - Fotos';
            $this->load->template('servico/imagens', $data);
        }

        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
            $class->ativo = 1; //Por padrão no create é ativo =1
            

            if ($this->Servico_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/servico/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/servico/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
            //UPLOAD DO BANNER
            if (!is_dir('./uploads/servico/' . $id)) {
                mkdir('./uploads/servico/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/servico/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('servico/edit/' . $id);
                } else {
                    $class->banner = 'uploads/servico/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->Servico_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/servico/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/servico/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Servico_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Servico_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/servico/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/servico/index');
            }
        }
        
        public function update_imagem() {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');

            $id = $this->input->post('id');
            $principal = $this->input->post('principal');
            // set validation rules
            $this->form_validation->set_rules('id', 'ID Servico', 'required');
            $this->form_validation->set_rules('principal', 'Principal', 'required');

            if ($this->form_validation->run() === false) {

                // validation not ok, send validation errors to the view
                $data->result = $this->Servico_model->retorna_servico($id);
                $data->page_title = 'Servico - Mensagens';
                redirect('servico/imagens/' . $id);
            } else {

                // set variables from the form
                $class = new stdClass();

                if (!is_dir('./uploads/servico/' . $id)) {
                    mkdir('./uploads/servico/' . $id, 0777);
                }

                // upload
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/servico/' . $id;
                    $config['allowed_types'] = 'jpg|png';
                    $config['max_size'] = '10000';
                    $config['overwrite'] = FALSE;
                    $config['remove_spaces'] = TRUE;
                    $new_name = normalizeChars($_FILES["userfile"]['name']);
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {
                        $data->error = $this->upload->display_errors();
                        $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                        redirect('servico/imagens/' . $id);
                    } else {
                        $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                        $class->extensao = $this->upload->file_ext;
                    }
                }
                // fim upload
                // insere foto no BD
                $class->servico_id = $id;
                $class->principal = $this->input->post('principal');

                if (!$this->Servico_foto_model->create_servico_foto($class)) {
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

                }
                 redirect('servico/imagens/' . $id);

            }
        }
    
    public function delete_imagem($id) {
        $foto = $this->Servico_foto_model->retorna_servico_foto($id);
        if ($this->Servico_foto_model->delete_servico_foto($foto->id)) {
            unlink("./uploads/servico/" . $foto->servico_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('servico/imagens/' . $foto->servico_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('servico/imagens/' . $foto->servico_id);
        }
    }
        
        
        
        
}
