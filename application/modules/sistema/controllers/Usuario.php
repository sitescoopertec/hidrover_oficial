<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Unidade_negocio_model');
            $this->load->model('Usuario_grupo_model');
            $this->load->model('Unidade_negocio_model');
            $this->load->model('Usuario_unidade_model');
            $this->load->model('Usuario_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Usuários';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario');
            $data->result = $this->Usuario_model->retorna_usuarios_completos();
            $this->load->template('usuario/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Usuário - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario');
            //Busca os dados da View
            $data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
            $this->load->template('usuario/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Usuário';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario');
            //Busca os dados da View
            $data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
            $data->result = $this->Usuario_model->retorna_usuario($id);
            $this->load->template('usuario/edit',$data);
	}
        
        public function unidade_negocio($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Usuário - Unidades de Negócio';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario');
            //Busca os dados da View
            $data->Usuario_unidade= $this->Usuario_unidade_model->retorna_usuario_unidades($id);
            $data->Unidade_negocio = $this->Unidade_negocio_model->retorna_Unidades_usuario($id);        
        
            $data->result = $this->Usuario_model->retorna_usuario($id);
            $this->load->template('usuario/unidade_negocio',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->usuario = $this->input->post('usuario');
            $class->senha = $this->input->post('senha');
            $class->ativo = 1;
            if ($_SESSION['is_admin']) {
               $class->usuario_grupo_id = $this->input->post('usuario_grupo_id'); 
               $class->tipo = $this->input->post('tipo');
            }
            else{
                //Busca o Grupo de Usuário Padrão por Unidade de Negocios                
                $uni_neg = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
                $class->tipo = 'T'; //Básico 
                $class->usuario_grupo_id = $uni_neg->usuario_grupo_id;
            }
            $class->nome = $this->input->post('nome');
            $class->email = $this->input->post('email');
            $class->celular = $this->input->post('celular');
            
            if ($this->Usuario_model->create_usuario($class)) {
                $retID = $this->db->insert_id();
                //Grava a Unidade Atual como Padrão
                $this->grava_unidade_padrao($retID, $_SESSION["uni_negocio_id"]);
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/usuario/edit/' . $retID);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', 'Error! Please, try again.');
                redirect('sistema/usuario/create');
            } 
           
        }
        
        function grava_unidade_padrao($usuario_id, $unidade_negocio_id){
            $class = new stdClass();
            $class->usuario_id = $usuario_id;
            $class->unidade_negocio_id = $unidade_negocio_id;
            $class->padrao = '1';
            $this->load->model('Usuario_unidade_model');
            $this->Usuario_unidade_model->create_usuario_unidade($class);        
        }
        
        public function update_registro(){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->usuario = $this->input->post('usuario');
            $class->senha = $this->input->post('senha');
            if ($_SESSION['is_admin']) {
               $class->usuario_grupo_id = $this->input->post('usuario_grupo_id'); 
               $class->tipo = $this->input->post('tipo');
            }
            else{
                //Busca o Grupo de Usuário Padrão por Unidade de Negocios                
                $uni_neg = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
                $class->tipo = 'T'; //Básico 
                $class->usuario_grupo_id = $uni_neg->usuario_grupo_id;
            }
            $class->nome = $this->input->post('nome');
            $class->email = $this->input->post('email');
            $class->celular = $this->input->post('celular');
            
            if ($this->Usuario_model->update_usuario($class)) {
                
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/usuario/edit/' . $class->id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('sistema/usuario/create');
            } 
        }
        
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Usuario_model->update_usuario($class)) {            
                $this->session->set_flashdata('alerta_sucesso','Salvo com sucesso!');            
            }
        }
        
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Usuario_model->delete_usuario($id)) {            
                $this->session->set_flashdata('alerta_sucesso', 'Item excluído com sucesso!');
                redirect('sistema/usuario/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', 'OPS! Alguma coisa saiu errada, tente novamente!');
                redirect('sistema/usuario/index');

            }
        }
        
        public function create_unidade_negocio() {
            $class = new stdClass();
            $class->usuario_id = $this->input->post('id');
            $class->unidade_negocio_id = $this->input->post('unidade_negocio_id');
            $class->padrao = $this->input->post('padrao');
            
            if ($this->Usuario_unidade_model->create_usuario_unidade($class)) {
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/usuario/unidade_negocio/' . $class->usuario_id);
            } else {
                // creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('sistema/usuario/unidade_negocio/' . $class->usuario_id);
            }
            
        }

        public function delete_unidade_negocio($usuario_unidade_id) {
            $this->load->model('Usuario_unidade_model');
            // create the data object
            $data = new stdClass();
            $Unidade= $this->Usuario_unidade_model->retorna_usuario_unidade($usuario_unidade_id);
            if ($this->Usuario_unidade_model->delete_usuario_unidade($usuario_unidade_id)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',  $_SESSION['msg_sucesso']);
                redirect('sistema/usuario/unidade_negocio/' . $Unidade->usuario_id);
            } else {
                $this->session->set_flashdata('alerta_erro',  $_SESSION['msg_erro']);
                redirect('sistema/usuario/unidade_negocio/' . $Unidade->usuario_id);
            }
        }
        
        public function senha() {
            $data = new stdClass();
            $this->load->library('form_validation');
            $usuario_id= $_SESSION["usuario_id"];
            $data->result = $this->Usuario_model->retorna_usuario($usuario_id);
            $this->load->template('usuarios/senha', $data);
        }
        
        public function update_troca_senha() {
            $data = new stdClass();
            $this->load->helper('form');
            $this->load->library('form_validation');

            $id = $this->input->post('id');
            // set validation rules

            // set variables from the form
            $class = new stdClass();
            $class->id = $id;
            $class->senha = $this->input->post('senha');

            if ($this->Usuario_model->update_usuario($class)) {
                $colec = new stdClass();
                $colec->usuario_id = $id;                
                redirect('login/login');
            } else {
                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', 'Error! Please, try again.');

            }

        }
        
}
