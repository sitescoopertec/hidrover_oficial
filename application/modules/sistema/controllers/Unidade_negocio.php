<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidade_negocio extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Unidade_negocio_model');
            $this->load->model('Usuario_grupo_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Unidades de Negócios';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_negocio');
            $data->result = $this->Unidade_negocio_model->retorna_unidade_negocios();
            $this->load->template('unidade_negocio/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Unidades de Negócios - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_negocio');
            //Busca os dados da View
            $data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
            $this->load->template('unidade_negocio/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Unidades de Negócios';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_negocio');
            //Busca os dados da View
            $data->grupo_usuarios = $this->Usuario_grupo_model->retorna_usuario_grupos();
            $data->result = $this->Unidade_negocio_model->retorna_unidade_negocio($id);
            $this->load->template('unidade_negocio/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->ativo = 1;
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->celular = $this->input->post('celular');
            $class->endereco = $this->input->post('endereco');
            $class->cidade = $this->input->post('cidade');
            $class->cnpj = $this->input->post('cnpj');
            $class->titulo_empresa = $this->input->post('titulo_empresa');
            $class->detalhes_empresa = $this->input->post('detalhes_empresa');
            $class->titulo_produto = $this->input->post('titulo_produto');
            $class->descricao_produto = $this->input->post('descricao_produto');
            $class->servico_titulo = $this->input->post('servico_titulo');
            $class->servico_detalhes = $this->input->post('servico_detalhes');
            $class->usuario_grupo_id = $this->input->post('usuario_grupo_id');
            $class->site = $this->input->post('site');
            
            if ($this->Unidade_negocio_model->create_unidade_negocio($class)) {
                $retID = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/unidade_negocio/edit/' . $retID);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('sistema/unidade_negocio/create');
            } 
           
        }
        
        
        public function update_registro($id){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->celular = $this->input->post('celular');
            $class->endereco = $this->input->post('endereco');
            $class->cidade = $this->input->post('cidade');
            $class->cnpj = $this->input->post('cnpj');
            $class->titulo_empresa = $this->input->post('titulo_empresa');
            $class->descricao_empresa = $this->input->post('descricao_empresa');
            $class->detalhes_empresa = $this->input->post('detalhes_empresa');
            $class->titulo_produto = $this->input->post('titulo_produto');
            $class->descricao_produto = $this->input->post('descricao_produto');
            $class->servico_titulo = $this->input->post('servico_titulo');
            $class->servico_detalhes = $this->input->post('servico_detalhes');
            $class->skype = $this->input->post('skype');
            $class->face = $this->input->post('face');
            $class->insta = $this->input->post('insta');
            $class->titulo_localizacao = $this->input->post('titulo_localizacao');
            $class->texto_localizacao = $this->input->post('texto_localizacao');
            $class->mapa = $this->input->post('mapa');
            $class->contato_titulo = $this->input->post('contato_titulo');
            $class->contato_texto = $this->input->post('contato_texto');
            $class->usuario_grupo_id = $this->input->post('usuario_grupo_id');
            //UPLOAD DO LOGO
            if (!is_dir('./uploads/unidade_negocio/' . $id)) {
                mkdir('./uploads/unidade_negocio/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/unidade_negocio/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('unidade_negocio/edit/' . $id);
                } else {
                    $class->logo = 'uploads/unidade_negocio/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            
            
            if ($this->Unidade_negocio_model->update_unidade_negocio($class)) {
                
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/unidade_negocio/edit/' . $class->id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('sistema/unidade_negocio/create');
            } 
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Unidade_negocio_model->update_unidade_negocio($class)) {            
                $this->session->set_flashdata('alerta_sucesso','Salvo com sucesso!');            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Unidade_negocio_model->delete_unidade_negocio($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('sistema/unidade_negocio/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('sistema/unidade_negocio/index');

            }
        }
}
