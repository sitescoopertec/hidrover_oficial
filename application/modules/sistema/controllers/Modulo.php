<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Modulo_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Módulos';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','modulo');
            $data->result = $this->Modulo_model->retorna_modulos();
            $this->load->template('modulo/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Módulos - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','modulo');
            $this->load->template('modulo/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Módulos';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','modulo');
            $data->result = $this->Modulo_model->retorna_modulo($id);
            $this->load->template('modulo/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');    
            $class->icone= $this->input->post('icone');
            $class->ordem= $this->input->post('ordem');            
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Modulo_model->create_modulo($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/modulo/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/modulo/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');    
            $class->icone= $this->input->post('icone');
            $class->ordem= $this->input->post('ordem');            
//            $class->ativo = ($this->input->post('ativo')==='on') ? '1' : '0';

            if ($this->Modulo_model->update_modulo($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/modulo/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/modulo/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Modulo_model->update_modulo($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Modulo_model->delete_modulo($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/modulo/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/modulo/index');

            }
        }
}
