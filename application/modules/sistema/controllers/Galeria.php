<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Galeria_model');
            $this->load->model('Categoria_model');
            $this->load->model('Galeria_foto_model');

            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Galerias';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','galeria');
            $data->result = $this->Galeria_model->retorna_galerias($_SESSION["uni_negocio_id"]);
            $this->load->template('sistema/galeria/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Galerias - Novo';
            $data->categorias = $this->Categoria_model->retorna_categoria_ativos($_SESSION["uni_negocio_id"]);
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','galeria');
            $this->load->template('sistema/galeria/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Galerias';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','galeria');
            $data->result = $this->Galeria_model->retorna_galeria($id);
            $data->categorias = $this->Categoria_model->retorna_categoria_ativos($_SESSION["uni_negocio_id"]);
            $this->load->template('sistema/galeria/edit',$data);
	}
        
        public function imagens($id) {
            $data = new stdClass();
            $this->load->library('form_validation');
            $data->result = $this->Galeria_model->retorna_galeria($id);
            $this->load->model('Galeria_foto_model');
            $data->fotos = $this->Galeria_foto_model->retorna_galeria_fotos($id);
            $this->load->template('sistema/galeria/imagens', $data);
        }
        
              
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->categoria_id = $this->input->post('categoria_id'); 
            $class->descricao = $this->input->post('descricao');    
            $class->detalhe = $this->input->post('detalhe');
            $class->ativo = 1; //Por padrão no create é ativo =1
            
            

             if ($this->Galeria_model->create($class)) {
                // OK
                $id = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/galeria/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/galeria/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->categoria_id = $this->input->post('categoria_id'); 
            $class->descricao = $this->input->post('descricao');    
            $class->detalhe = $this->input->post('detalhe');
            
             //UPLOAD DO BANNER
            if (!is_dir('./uploads/galeria/' . $id)) {
                mkdir('./uploads/galeria/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/galeria/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('sistema/galeria/edit/' . $id);
                } else {
                    $class->banner = 'uploads/galeria/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            
            if ($this->Galeria_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/galeria/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/galeria/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Galeria_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Galeria_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/galeria/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/galeria/index');
            }
        }

        
        public function update_imagem() {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');
            $this->load->model('Galeria_foto_model');
            $id = $this->input->post('id');

            $this->form_validation->set_rules('id', 'Galeria Foto', 'required');

            if ($this->form_validation->run() === false) {
                $data->result = $this->Galeria_model->retorna_galeria($id);
                redirect('sistema/galeria/imagens/' . $id);
            } else {
               $class = new stdClass();
                if (!is_dir('./uploads/galeria_img/' . $id)) {
                    mkdir('./uploads/galeria_img/' . $id, 0777);
                }

                // upload
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/galeria_img/' . $id;
                    $config['allowed_types'] = 'jpg|png';
                    $config['max_size'] = '100000';
                    $config['overwrite'] = FALSE;
                    $config['remove_spaces'] = TRUE;
                    $new_name = normalizeChars($_FILES["userfile"]['name']);
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {
                        $data->error = $this->upload->display_errors();
                        $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                        redirect('sistema/galeria/imagens/' . $id);
                    } else {
                        $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                        $class->extensao = $this->upload->file_ext;
                        $this->load->library('image_lib', $config2);
                    }
                }
                $class->galeria_id = $id;
                $class->principal = 0;
                if (!$this->Galeria_foto_model->create_galeria_foto($class)) {
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

                }
                redirect('sistema/galeria/imagens/' . $id);
            }
        }
        
        public function ativa_principal($galeria_foto_id) {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');

            $this->load->model('Galeria_foto_model');
            $foto = $this->Galeria_foto_model->retorna_galeria_foto($galeria_foto_id);

            $this->Galeria_foto_model->ativa_foto_principal($foto->galeria_id,$galeria_foto_id);
            redirect('sistema/galeria/imagens/' . $foto->galeria_id);
        }
    
    public function delete_imagem($id) {
        $this->load->model('Galeria_foto_model');
        $foto = $this->Galeria_foto_model->retorna_galeria_foto($id);
        if ($this->Galeria_foto_model->delete_galeria_foto($foto->id)) {
            unlink("./uploads/galeria_img/" . $foto->galeria_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('sistema/galeria/imagens/' . $foto->galeria_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('sistema/galeria/imagens/' . $foto->galeria_id);
        }
    }
  
        
}
