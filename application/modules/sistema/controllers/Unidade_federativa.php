<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidade_federativa extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Unidade_federativa_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Unidades Federativas';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_federativa');
            $data->result = $this->Unidade_federativa_model->retorna_unidade_federativas();
            $this->load->template('unidade_federativa/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Unidades Federativas - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_federativa');
            $this->load->template('unidade_federativa/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Unidades Federativas';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','unidade_federativa');
            $data->result = $this->Unidade_federativa_model->retorna_unidade_federativa($id);
            $this->load->template('unidade_federativa/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');    
            $class->sigla= $this->input->post('sigla');            
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Unidade_federativa_model->create_unidade_federativa($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/unidade_federativa/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/unidade_federativa/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');    
            $class->sigla= $this->input->post('sigla');            
            if ($this->Unidade_federativa_model->update_unidade_federativa($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/unidade_federativa/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/unidade_federativa/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Unidade_federativa_model->update_unidade_federativa($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Unidade_federativa_model->delete_unidade_federativa($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/unidade_federativa/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/unidade_federativa/index');

            }
        }
}
