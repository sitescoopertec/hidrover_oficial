<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Categoria_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Categorias';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','categoria');
            $data->result = $this->Categoria_model->retorna_categorias($_SESSION["uni_negocio_id"]);
            $this->load->template('categoria/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Categoria - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','categoria');
            $this->load->template('categoria/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Categoria';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','categoria');
            $data->result = $this->Categoria_model->retorna_categoria($id);
            $this->load->template('categoria/edit',$data);
	}

        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1
            

            if ($this->Categoria_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/categoria/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/categoria/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
           
            if ($this->Categoria_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/categoria/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/categoria/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Categoria_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Categoria_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/categoria/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/categoria/index');
            }
        }
        
        
        
        
}
