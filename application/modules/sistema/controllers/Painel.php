<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Painel_model');
            $this->load->model('Painel_site_foto_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Painéis';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','painel');
            $data->result = $this->Painel_model->retorna_paineis($_SESSION["uni_negocio_id"]);
            $this->load->template('painel/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Painel - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','painel');
            $this->load->template('painel/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Painel';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','painel');
            $data->result = $this->Painel_model->retorna_painel($id);
            $this->load->template('painel/edit',$data);
	}
        
        public function imagens($id) {
            $data = new stdClass();
            $this->load->library('form_validation');
            $data->result = $this->Painel_model->retorna_painel($id);
            $data->foto_principal = $this->Painel_site_foto_model->retorna_painel_site_foto_principal($id);
            $data->page_title = 'Painel - ' . $data->result->nome . ' - Fotos';
            $this->load->template('painel/imagens', $data);
        }

        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
            $class->ativo = 1; //Por padrão no create é ativo =1
            

            if ($this->Painel_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/painel/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/painel/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->titulo = $this->input->post('titulo');
            $class->texto = $this->input->post('texto');
            //UPLOAD DO CAROUSEL
            if (!is_dir('./uploads/painel/' . $id)) {
                mkdir('./uploads/painel/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/painel/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('painel/edit/' . $id);
                } else {
                    $class->banner = 'uploads/painel/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            if ($this->Painel_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/painel/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/painel/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Painel_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Painel_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/painel/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/painel/index');
            }
        }
        
        public function update_imagem() {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');

            $id = $this->input->post('id');
            $principal = $this->input->post('principal');
            // set validation rules
            $this->form_validation->set_rules('id', 'ID Painel', 'required');
            $this->form_validation->set_rules('principal', 'Principal', 'required');

            if ($this->form_validation->run() === false) {

                // validation not ok, send validation errors to the view
                $data->result = $this->Painel_model->retorna_painel($id);
                $data->page_title = 'Painel - Mensagens';
                redirect('painel/imagens/' . $id);
            } else {

                // set variables from the form
                $class = new stdClass();

                if (!is_dir('./uploads/painel/' . $id)) {
                    mkdir('./uploads/painel/' . $id, 0777);
                }

                // upload
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/painel/' . $id;
                    $config['allowed_types'] = 'jpg|png';
                    $config['max_size'] = '10000';
                    $config['overwrite'] = FALSE;
                    $config['remove_spaces'] = TRUE;
                    $new_name = normalizeChars($_FILES["userfile"]['name']);
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {
                        $data->error = $this->upload->display_errors();
                        $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                        redirect('painel/imagens/' . $id);
                    } else {
                        $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                        $class->extensao = $this->upload->file_ext;
                    }
                }
                // fim upload
                // insere foto no BD
                $class->painel_id = $id;
                $class->principal = $this->input->post('principal');

                if (!$this->Painel_site_foto_model->create_painel_site_foto($class)) {
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

                }
                 redirect('painel/imagens/' . $id);

            }
        }
    
    public function delete_imagem($id) {
        $foto = $this->Painel_site_foto_model->retorna_painel_site_foto($id);
        if ($this->Painel_site_foto_model->delete_painel_site_foto($foto->id)) {
            unlink("./uploads/painel/" . $foto->painel_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('painel/imagens/' . $foto->painel_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('painel/imagens/' . $foto->painel_id);
        }
    }
        
        
        
        
}
