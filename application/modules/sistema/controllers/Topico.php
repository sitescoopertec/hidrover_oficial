<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topico extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Topico_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Topicos';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','topico');
            $data->result = $this->Topico_model->retorna_topicos($_SESSION["uni_negocio_id"]);
            $this->load->template('sistema/topico/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Topicos - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','topico');
            $this->load->template('sistema/topico/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Topicos';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','topico');
            $data->result = $this->Topico_model->retorna_topico($id);
            $this->load->template('sistema/topico/edit',$data);
	}
              
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->detalhes = $this->input->post('detalhes');
            $class->icone = $this->input->post('icone');

            $class->ativo = 1; //Por padrão no create é ativo =1
            
            

             if ($this->Topico_model->create($class)) {
                // OK
                $id = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/topico/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/topico/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->detalhes = $this->input->post('detalhes');
            $class->icone = $this->input->post('icone');
            
            if ($this->Topico_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/topico/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/topico/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Topico_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Topico_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/topico/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/topico/index');
            }
        }

}
