<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_grupo extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Usuario_grupo_model');
            $this->load->model('Usuario_grupo_menu_model');
            $this->load->model('Menu_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Grupo de Usuário';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario_grupo');
            $data->result = $this->Usuario_grupo_model->retorna_usuario_grupos();
            $this->load->template('usuario_grupo/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Grupo de Usuários - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario_grupo');
            $this->load->template('usuario_grupo/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Grupo de Usuários';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','usuario_grupo');
            //Busca os dados da View
            $data->menus = $this->Menu_model->retorna_menus_ativos();
            $data->menus_salvos = $this->Usuario_grupo_menu_model->retorna_usuario_grupo_menus($id);
            $data->result = $this->Usuario_grupo_model->retorna_usuario_grupo($id);
            $this->load->template('usuario_grupo/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');               
            $class->ativo = 1; //Por padrão no create é ativo =1
            if ($this->Usuario_grupo_model->create_usuario_grupo($class)) {
                $id= $this->db->insert_id();                        
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/usuario_grupo/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/usuario_grupo/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');               
            
            if ($this->Usuario_grupo_model->update_usuario_grupo($class)) {
                
                
                $menus_grp = new stdClass();
                $menus_grp->usuario_grupo_id = $id;
                $menus_grp->menus = $this->input->post('menus');
                //Salva Menus
                $this->Usuario_grupo_menu_model->update_usuario_grupo_menu($menus_grp);
                
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
//                redirect('sistema/usuario_grupo/edit/' . $id);
                 redirect('login/home');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/usuario_grupo/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Usuario_grupo_model->update_usuario_grupo($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Usuario_grupo_model->delete_usuario_grupo($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/usuario_grupo/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/usuario_grupo/index');

            }
        }
}
