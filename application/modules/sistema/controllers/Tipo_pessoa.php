<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_pessoa extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Tipo_pessoa_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Tipo Pessoa';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_pessoa');
            $data->result = $this->Tipo_pessoa_model->retorna_tipo_pessoas();
            $this->load->template('tipo_pessoa/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo Pessoa - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_pessoa');
            $this->load->template('tipo_pessoa/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo Pessoa';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_pessoa');
            $data->result = $this->Tipo_pessoa_model->retorna_tipo_pessoa($id);
            $this->load->template('tipo_pessoa/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Tipo_pessoa_model->create_tipo_pessoa($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_pessoa/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/tipo_pessoa/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');    
            if ($this->Tipo_pessoa_model->update_tipo_pessoa($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_pessoa/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/tipo_pessoa/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Tipo_pessoa_model->update_tipo_pessoa($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Tipo_pessoa_model->delete_tipo_pessoa($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_pessoa/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/tipo_pessoa/index');

            }
        }
}
