<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Contato_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Contatos';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','contato');
            $data->result = $this->Contato_model->retorna_contatos($_SESSION["uni_negocio_id"]);
            $this->load->template('contato/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Contato - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','contato');
            $this->load->template('contato/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Contato';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','contato');
            $data->result = $this->Contato_model->retorna_contato($id);
            $this->load->template('contato/edit',$data);
	}

        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');    
            $class->telefone = $this->input->post('telefone');
            $class->email = $this->input->post('email');
            $class->assunto = $this->input->post('assunto');
            $dt_data =DateTime::createFromFormat('d/m/Y',  $this->input->post('data'));
            if ($dt_data == false) {
                $date = null;
            } else {
                $date = $dt_data->format('Y-m-d');
            }            
            $class->data = empty($date) ? NULL : $date;  
            $class->status = 1;
            $class->ativo = 1; //Por padrão no create é ativo =1
            

            if ($this->Contato_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/contato/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/contato/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');    
            $class->telefone = $this->input->post('telefone');
            $class->email = $this->input->post('email');
            $class->assunto = $this->input->post('assunto');
            
            $class->status = 2;
//             $class->sombra = $this->input->post('sombra');
            
            if ($this->Contato_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/contato/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/contato/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Contato_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Contato_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/contato/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/contato/index');
            }
        }
        
        
        public function create_contato_site(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome = $this->input->post('nome');    
            $class->telefone = $this->input->post('telefone');
            $class->email = $this->input->post('email');
            $class->assunto = $this->input->post('assunto');
            $class->data = date("Y-m-d"); 
            $class->status = 1;
            $class->ativo = 1; //Por padrão no create é ativo =1
            
            $this->Contato_model->create($class);
        }
        
        
}
