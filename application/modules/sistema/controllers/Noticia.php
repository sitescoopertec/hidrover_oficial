<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Noticia_model');
            $this->load->model('Noticia_foto_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Noticias';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','noticia');
            $data->result = $this->Noticia_model->retorna_noticias($_SESSION["uni_negocio_id"]);
            $this->load->template('sistema/noticia/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Noticias - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','noticia');
            $this->load->template('sistema/noticia/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Noticias';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','noticia');
            $data->result = $this->Noticia_model->retorna_noticia($id);
            $this->load->template('sistema/noticia/edit',$data);
	}
        
        public function imagens($id) {
            $data = new stdClass();
            $this->load->library('form_validation');
            $data->result = $this->Noticia_model->retorna_noticia($id);
            $this->load->model('Noticia_foto_model');
            $data->fotos = $this->Noticia_foto_model->retorna_noticia_fotos($id);
            $this->load->template('sistema/noticia/imagens', $data);
        }
        
              
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->titulo = $this->input->post('titulo');
            $class->subtitulo = $this->input->post('subtitulo');
            $class->texto = $this->input->post('texto');
            $class->titulo_fonte = $this->input->post('titulo_fonte');
            $class->link_fonte = $this->input->post('link_fonte');

            $class->ativo = 1; //Por padrão no create é ativo =1
            
            

             if ($this->Noticia_model->create($class)) {
                // OK
                $id = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/noticia/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/noticia/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->titulo = $this->input->post('titulo');
            $class->subtitulo = $this->input->post('subtitulo');
            $class->texto = $this->input->post('texto');
            $class->titulo_fonte = $this->input->post('titulo_fonte');
            $class->link_fonte = $this->input->post('link_fonte');
            
             //UPLOAD DO BANNER
            if (!is_dir('./uploads/noticia/' . $id)) {
                mkdir('./uploads/noticia/' . $id, 0777);
            }

            // upload
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path'] = './uploads/noticia/' . $id;
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $config['remove_spaces'] = TRUE;
                $new_name = normalizeChars($_FILES["userfile"]['name']);
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data->error = $this->upload->display_errors();
                    $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                    redirect('sistema/noticia/edit/' . $id);
                } else {
                    $class->banner = 'uploads/noticia/'. $id . '/' . $this->upload->file_name;
                    
                }
            }
            
            if ($this->Noticia_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/noticia/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/noticia/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Noticia_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Noticia_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/noticia/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/noticia/index');
            }
        }
        
        public function update_imagem() {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');
            $this->load->model('Noticia_foto_model');
            $id = $this->input->post('id');

            $this->form_validation->set_rules('id', 'Noticia Foto', 'required');

            if ($this->form_validation->run() === false) {
                $data->result = $this->Noticia_model->retorna_noticia($id);
                redirect('sistema/noticia/imagens/' . $id);
            } else {
               $class = new stdClass();
                if (!is_dir('./uploads/noticia_img/' . $id)) {
                    mkdir('./uploads/noticia_img/' . $id, 0777);
                }

                // upload
                if (isset($_FILES['userfile'])) {
                    $config['upload_path'] = './uploads/noticia_img/' . $id;
                    $config['allowed_types'] = 'jpg|png';
                    $config['max_size'] = '100000';
                    $config['overwrite'] = FALSE;
                    $config['remove_spaces'] = TRUE;
                    $new_name = normalizeChars($_FILES["userfile"]['name']);
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload()) {
                        $data->error = $this->upload->display_errors();
                        $this->session->set_flashdata('alerta_erro', $this->upload->display_errors());
                        redirect('sistema/noticia/imagens/' . $id);
                    } else {
                        $class->nome_arquivo = normalizeChars(basename($this->upload->file_name, $this->upload->file_ext));
                        $class->extensao = $this->upload->file_ext;
                        $this->load->library('image_lib', $config2);
                    }
                }
                $class->noticia_id = $id;
                $class->principal = 0;
                if (!$this->Noticia_foto_model->create_noticia_foto($class)) {
                    $data->error = 'Erro! Por favor, tente novamente.';
                    $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');

                }
                redirect('sistema/noticia/imagens/' . $id);
            }
        }
        
        public function ativa_principal($noticia_foto_id) {

            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->helper('geral');
            $this->load->library('form_validation');

            $this->load->model('Noticia_foto_model');
            $foto = $this->Noticia_foto_model->retorna_noticia_foto($noticia_foto_id);

            $this->Noticia_foto_model->ativa_foto_principal($foto->noticia_id,$noticia_foto_id);
            redirect('sistema/noticia/imagens/' . $foto->noticia_id);
        }
    
    public function delete_imagem($id) {
        $this->load->model('Noticia_foto_model');
        $foto = $this->Noticia_foto_model->retorna_noticia_foto($id);
        if ($this->Noticia_foto_model->delete_noticia_foto($foto->id)) {
            unlink("./uploads/noticia_img/" . $foto->noticia_id . "/" . $foto->nome_arquivo . $foto->extensao);
            // OK
            $this->session->set_flashdata('alerta_sucesso', 'Sucesso ao gravar');
            redirect('sistema/noticia/imagens/' . $foto->noticia_id);
        } else {
            // creation failed, this should never happen
            $this->session->set_flashdata('alerta_erro', 'Erro! Por favor, tente novamente.');
            redirect('sistema/noticia/imagens/' . $foto->noticia_id);
        }
    }
  
        
}
