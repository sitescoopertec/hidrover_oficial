<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_menu extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Tipo_menu_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Tipo de Menus';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_menu');
            $data->result = $this->Tipo_menu_model->retorna_tipo_menus();
            $this->load->template('tipo_menu/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo de Menus - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_menu');
            $this->load->template('tipo_menu/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo de Menus';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','tipo_menu');
            $data->result = $this->Tipo_menu_model->retorna_tipo_menu($id);
            $this->load->template('tipo_menu/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');    
            $class->icone= $this->input->post('icone');
            $class->ordem= $this->input->post('ordem');            
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Tipo_menu_model->create_tipo_menu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_menu/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/tipo_menu/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');    
            $class->icone= $this->input->post('icone');
            $class->ordem= $this->input->post('ordem');            
//            $class->ativo = ($this->input->post('ativo')==='on') ? '1' : '0';

            if ($this->Tipo_menu_model->update_tipo_menu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_menu/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/tipo_menu/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Tipo_menu_model->update_tipo_menu($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Tipo_menu_model->delete_tipo_menu($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/tipo_menu/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/tipo_menu/index');

            }
        }
}
