<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Tipo_menu_model');
            $this->load->model('Modulo_model');
            $this->load->model('Menu_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Menus';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','menu');
            $data->result = $this->Menu_model->retorna_menus();
            $this->load->template('menu/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Menus - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','menu');
            //Busca os dados da View
            $data->modulos = $this->Modulo_model->retorna_modulo_ativos();
            $data->tipo_menus = $this->Tipo_menu_model->retorna_tipo_menus();
            $this->load->template('menu/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Menus';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('sistema','menu');
            //Busca os dados da View
            $data->modulos = $this->Modulo_model->retorna_modulo_ativos();
            $data->tipo_menus = $this->Tipo_menu_model->retorna_tipo_menus();
            $data->result = $this->Menu_model->retorna_menu($id);
            $this->load->template('menu/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->descricao = $this->input->post('descricao');               
            $class->menu = $this->input->post('menu'); 
            $class->ordem= $this->input->post('ordem');            
            $class->target= $this->input->post('target');            
            $class->icone= $this->input->post('icone');
            $class->tipo_menu_id = $this->input->post('tipo_menu_id');            
            $class->modulo_id = $this->input->post('modulo_id');  
            $class->ativo = 1; //Por padrão no create é ativo =1
            if ($this->Menu_model->create_menu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/menu/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/menu/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');
            $class->id = $id;    
            $class->descricao = $this->input->post('descricao');               
            $class->menu = $this->input->post('menu'); 
            $class->ordem= $this->input->post('ordem');            
            $class->target= $this->input->post('target');            
            $class->icone = $this->input->post('icone');
            $class->modulo_id = $this->input->post('modulo_id');  
            $class->tipo_menu_id = $this->input->post('tipo_menu_id');          
//            $class->ativo = ($this->input->post('ativo')==='on') ? '1' : '0';

            if ($this->Menu_model->update_menu($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('sistema/menu/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('sistema/menu/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Menu_model->update_menu($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Menu_model->delete_menu($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('sistema/menu/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('sistema/menu/index');

            }
        }
}
