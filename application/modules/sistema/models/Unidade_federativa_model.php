<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Unidade_federativa_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_unidade_federativas() {
        $this->db->from('unidade_federativa');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_unidade_federativa($data) {
        return $this->db->insert('unidade_federativa', $data);
    }

    public function update_unidade_federativa($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('unidade_federativa', $data);
        }
    }

    public function delete_unidade_federativa($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('unidade_federativa');
        }
    }

    public function retorna_unidade_federativa($id) {

        $this->db->from('unidade_federativa');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_unidade_federativa_ativos() {

        $this->db->from('unidade_federativa');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
