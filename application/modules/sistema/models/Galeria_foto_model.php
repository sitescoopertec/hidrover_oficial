<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Galeria_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_galeria_foto($data) {
        return $this->db->insert('galeria_foto', $data);
    }

    public function update_galeria_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('galeria_foto', $data);
        }
    }

    public function delete_galeria_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('galeria_foto');
        }
    }

    public function retorna_galeria_foto($id) {

        $this->db->from('galeria_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_galeria_fotos_ativas() {

        $this->db->from('galeria_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
     public function retorna_galeria_foto_principal($galeria_id) {

        $this->db->select('gf.id, gf.galeria_id, gf.id foto_id, gf.principal, gf.nome_arquivo, gf.extensao');
        $this->db->from('galeria_foto AS gf');
        $this->db->where('gf.galeria_id', $galeria_id);
        $this->db->where('gf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_galeria_fotos($galeria_id) {

        $this->db->select('gf.id, gf.galeria_id, gf.id foto_id, gf.principal, gf.nome_arquivo, gf.extensao');
        $this->db->from('galeria_foto AS gf');
        $this->db->where('gf.galeria_id', $galeria_id);
        $query = $this->db->get();
        return $query->result();
    }
}
