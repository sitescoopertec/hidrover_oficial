<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Usuario_grupo_menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_usuario_grupo_menus($usuario_grupo_id) {

        $this->db->select('mnu.id menu_id, mnu.descricao');
        $this->db->from('usuario_grupo_menu as usr_grp_mnu');
        $this->db->join('menu as mnu', 'mnu.id=usr_grp_mnu.menu_id');
        $this->db->where('usuario_grupo_id',$usuario_grupo_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_usuario_grupo_menu($data) {
        return $this->db->insert('usuario_grupo_menu', $data);
    }


    public function delete_usuario_grupo_menu($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('usuario_grupo_menu');
        }
    }
    
    public function update_usuario_grupo_menu($data) {
        if ($data->usuario_grupo_id) {
            $this->db->query("DELETE FROM usuario_grupo_menu WHERE usuario_grupo_id = " . $data->usuario_grupo_id . "");
            foreach ($data->menus as $menu_id) {
                $data->menu_id = $menu_id;
                $this->db->insert('usuario_grupo_menu', $data);
            }
            return true;
        }
    }

}
