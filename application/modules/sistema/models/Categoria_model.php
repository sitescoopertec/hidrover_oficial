<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Categoria_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_categorias($unidade_negocio_id) {
        $this->db->from('categoria');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('categoria', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('categoria', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('categoria');
        }
    }
    
    public function retorna_categoria($id) {

        $this->db->from('categoria');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_categoria_ativos($unidade_negocio_id) {

        $this->db->from('categoria');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
