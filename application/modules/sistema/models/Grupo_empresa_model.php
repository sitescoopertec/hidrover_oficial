<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Grupo_empresa_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_grupo_empresas() {
        $this->db->from('grupo_empresa');        
        $this->db->order_by('nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function create($data) {
        return $this->db->insert('grupo_empresa', $data);
    }

    public function update($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('grupo_empresa', $data);
        }
    }

    public function delete($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('grupo_empresa');
        }
    }

    public function retorna_grupo_empresa($id) {

        $this->db->from('grupo_empresa');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_grupo_empresas_ativos() {

        $this->db->from('grupo_empresa');
        $this->db->where('ativo', 1);
        $this->db->order_by('nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
}
