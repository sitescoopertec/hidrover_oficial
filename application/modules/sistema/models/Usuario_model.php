<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Usuario_model class.
 * 
 * @extends CI_Model
 */
class Usuario_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_usuarios() {

        $this->db->from('usuario');
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_menus($usuario_id){
        $this->db->select(' mo.id modulo_id, mo.descricao desc_modulo, mo.ordem ordem_modulo, mo.icone icone_modulo,
            tp_mnu.id tipo_menu_id, tp_mnu.descricao desc_tipo_menu,tp_mnu.icone icone_tipo_menu, 
            usr_grp.id usuario_grupo_id, usr_grp.descricao desc_usuario_grupo, mnu.id menu_id, 
            mnu.descricao desc_menu, mnu.menu, mnu.target, mnu.icone');
        $this->db->from('menu as mnu');
        $this->db->join('usuario_grupo_menu as usr_grp_mnu','usr_grp_mnu.menu_id=mnu.id', 'left');
        $this->db->join('usuario_grupo as usr_grp','usr_grp.id=usr_grp_mnu.usuario_grupo_id', 'left');
        $this->db->join('usuario as usr','usr.usuario_grupo_id=usr_grp.id', 'left');
        $this->db->join('tipo_menu as tp_mnu','tp_mnu.id=mnu.tipo_menu_id', 'left');
        $this->db->join('modulo as mo','mo.id=mnu.modulo_id', 'left');
        
        $this->db->where('usr.id',$usuario_id);
        $this->db->order_by('mo.ordem, tp_mnu.ordem, mnu.ordem');                
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_usuarios_completos() {
        $this->db->select('u.id, u.nome, u.usuario, u.ativo, u.tipo, grp.descricao desc_grupo');
        $this->db->from('usuario AS u');
        $this->db->join('usuario_grupo grp','grp.id=u.usuario_grupo_id');
        //Não tem acesso master de admin
        if (!$_SESSION["is_admin"]){
            $this->db->where(" EXISTS (
                    select usr.id from usuario usr
                    left outer join usuario_unidade u_neg on u_neg.usuario_id=usr.id
                    where u.id=usr.id and u_neg.unidade_negocio_id IN ( 
                                    select unidade_negocio_id from usuario_unidade where usuario_id =". $_SESSION["usuario_id"] . " 

                    )) and u.tipo='T' ");
            
        }
        $this->db->order_by('u.nome');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_usuario($data) {
        if ($data->senha) {
            $data->senha = $this->hash_password($data->senha);
        }
        return $this->db->insert('usuario', $data);
    }

    public function update_usuario($data) {

        if ($data->senha) {
            $data->senha = $this->hash_password($data->senha);
        }
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('usuario', $data);
        }
    }

    public function delete_usuario($id) {

        if ($id) {
            $this->db->query('DELETE FROM usuario_unidade WHERE usuario_id=' . $id);
            $this->db->query('DELETE FROM usuario_acesso WHERE usuario_id=' . $id);
            $this->db->where('id', $id);
            return $this->db->delete('usuario');
        }
    }

    public function valida_login($usuario, $senha) {

        $this->db->select('senha');
        $this->db->from('usuario');
        $this->db->where('usuario', $usuario);
        $hash = $this->db->get()->row('senha');

        return $this->verify_password_hash($senha, $hash);
    }

    public function retorna_id_por_usuario($usuario) {
        $this->db->select('id');
        $this->db->from('usuario');
        $this->db->where('usuario', $usuario);
        return $this->db->get()->row('id');
    }

    public function retorna_usuario($usuario_id) {
        $this->db->select('usu.*, uni.unidade_negocio_id');
        $this->db->from('usuario usu');
        $this->db->join('usuario_unidade uni', 'uni.usuario_id = usu.id');
        $this->db->where('uni.padrao', 1);
        $this->db->where('usu.id', $usuario_id);
        return $this->db->get()->row();
        

    }

    private function hash_password($senha) {

        return password_hash($senha, PASSWORD_BCRYPT);
    }

    private function verify_password_hash($senha, $hash) {

        return password_verify($senha, $hash);
    }

}
