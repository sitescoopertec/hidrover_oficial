<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Galeria_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_galerias($unidade_negocio_id) {
        $this->db->from('galeria');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('galeria', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('galeria', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('galeria');
        }
    }
    
    public function retorna_galeria($id) {

        $this->db->from('galeria');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_galeria_ativos($unidade_negocio_id) {
        $this->db->select('g.*, cat.descricao desc_categoria');
        $this->db->from('galeria g');
        $this->db->join('categoria cat','cat.id=g.categoria_id');
        $this->db->where('g.ativo', 1);
        $this->db->where('g.unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('g.descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
