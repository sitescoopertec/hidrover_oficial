<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Tipo_pessoa_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_tipo_pessoas() {
        $this->db->from('tipo_pessoa');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_tipo_pessoa($data) {
        return $this->db->insert('tipo_pessoa', $data);
    }

    public function update_tipo_pessoa($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('tipo_pessoa', $data);
        }
    }

    public function delete_tipo_pessoa($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('tipo_pessoa');
        }
    }

    public function retorna_tipo_pessoa($id) {

        $this->db->from('tipo_pessoa');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_tipo_pessoa_ativos() {

        $this->db->from('tipo_pessoa');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
