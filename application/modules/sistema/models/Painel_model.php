<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Painel_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_paineis($unidade_negocio_id) {
        $this->db->from('painel');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('painel', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('painel', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('painel');
        }
    }
    
    public function retorna_painel($id) {

        $this->db->from('painel');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_painel_ativos($unidade_negocio_id) {

        $this->db->from('painel');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
