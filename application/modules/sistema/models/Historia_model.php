<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Historia_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_historias($unidade_negocio_id) {
        $this->db->from('historia');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('ano');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('historia', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('historia', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('historia');
        }
    }
    
    public function retorna_historia($id) {

        $this->db->from('historia');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_historia_ativos($unidade_negocio_id) {

        $this->db->from('historia');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('ano');
        $query = $this->db->get();
        return $query->result();
    }
}
