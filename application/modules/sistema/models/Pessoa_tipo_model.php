<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Pessoa_tipo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_pessoa_tipos($pessoa_id) {

        $this->db->select('tp.id tipo_pessoa_id, tp.descricao');
        $this->db->from('pessoa_tipo as pt');
        $this->db->join('tipo_pessoa tp', 'pt.tipo_pessoa_id=tp.id');
        $this->db->where('pt.pessoa_id',$pessoa_id);
        $this->db->order_by('tp.descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_pessoa_tipo($data) {
        return $this->db->insert('pessoa_tipo', $data);
    }


    public function delete_pessoa_tipo($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('pessoa_tipo');
        }
    }
    
    public function update_pessoa_tipo($data) {
        if ($data->pessoa_id) {
            $this->db->query("DELETE FROM pessoa_tipo WHERE pessoa_id = " . $data->pessoa_id . "");
            foreach ($data->tipos as $tipo_id) {
                $data->tipo_pessoa_id = $tipo_id;
                $this->db->insert('pessoa_tipo', $data);
            }
            return true;
        }
    }

}
