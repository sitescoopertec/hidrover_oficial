<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Equipe_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_equipes($unidade_negocio_id) {
        $this->db->from('equipe');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('equipe', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('equipe', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('equipe');
        }
    }
    
    public function retorna_equipe($id) {

        $this->db->from('equipe');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_equipe_ativos($unidade_negocio_id) {

        $this->db->from('equipe');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    

    
    
}
