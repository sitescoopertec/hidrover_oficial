<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Servico_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_servico_foto($data) {
        return $this->db->insert('servico_foto', $data);
    }

    public function update_servico_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('servico_foto', $data);
        }
    }

    public function delete_servico_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('servico_foto');
        }
    }

    public function retorna_servico_foto($id) {

        $this->db->from('servico_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_servico_fotos_ativas() {

        $this->db->from('servico_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
     public function retorna_servico_foto_principal($servico_id) {

        $this->db->select('lf.id, lf.servico_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('servico_foto AS lf');
        $this->db->where('lf.servico_id', $servico_id);
        $this->db->where('lf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_servico_fotos($servico_id) {

        $this->db->select('lf.id, lf.servico_id, lf.id foto_id, lf.principal, lf.nome_arquivo, lf.extensao');
        $this->db->from('servico_foto AS lf');
        $this->db->where('lf.servico_id', $servico_id);
        $this->db->where('lf.principal', 1);
        $query = $this->db->get();
        return $query->result();
    }
}
