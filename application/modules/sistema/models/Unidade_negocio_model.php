<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Acessorio_model class.
 * 
 * @extends CI_Model
 */
class Unidade_negocio_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_unidade_negocios() {

        $this->db->from('unidade_negocio');
        $this->db->order_by('nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    
    public function retorna_unidade_negocios_id($unidade_negocio_id) {

        $this->db->from('unidade_negocio');
         $this->db->where('id', $unidade_negocio_id);
        
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_unidade_negocios_ativos() {

        $this->db->from('unidade_negocio');
        $this->db->where('ativo',1);
        $this->db->order_by('nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function create_unidade_negocio($data) {
        return $this->db->insert('unidade_negocio', $data);
    }

    public function update_unidade_negocio($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('unidade_negocio', $data);
        }
    }

    public function delete_unidade_negocio($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('unidade_negocio');
        }
    }

    public function retorna_unidade_negocio($id) {

        $this->db->from('unidade_negocio');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_unidades_usuario($usuario_id) {

        $this->db->from('unidade_negocio as uni');
        $this->db->where('uni.ativo', 1);
        $this->db->where('NOT EXISTS (
                            SELECT 1 
                            FROM usuario_unidade as un_usu
                            WHERE uni.id = un_usu.unidade_negocio_id AND un_usu.usuario_id='. $usuario_id . '
        )');
        if ($_SESSION["is_admin"]== false){
            $this->db->where(' EXISTS (
                    SELECT 1 
                    FROM usuario_unidade as un_usu
                    WHERE uni.id = un_usu.unidade_negocio_id AND un_usu.usuario_id='. $_SESSION["usuario_id"] . '
                    )');
        }
        $this->db->order_by('uni.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_unidades_do_usuario($usuario_id) {

        $this->db->from('unidade_negocio as uni');
        
        $this->db->where('uni.ativo', 1);
        $this->db->where('EXISTS (
        SELECT 1 
        FROM usuario_unidade as un_usu
        WHERE uni.id = un_usu.unidade_negocio_id AND un_usu.usuario_id='. $usuario_id . '
        )');
        $this->db->order_by('uni.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
}
