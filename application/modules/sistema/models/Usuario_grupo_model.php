<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Usuario_grupo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_usuario_grupos() {

        $this->db->from('usuario_grupo');
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_usuario_grupo($data) {
        return $this->db->insert('usuario_grupo', $data);
    }

    public function update_usuario_grupo($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('usuario_grupo', $data);
        }
    }

    public function delete_usuario_grupo($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('usuario_grupo');
        }
    }

    public function retorna_usuario_grupo($id) {

        $this->db->from('usuario_grupo');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }
    
    public function retorna_menus_grupo($usuario_grupo_id) {

        $this->db->from('usuario_grupo');
        $this->db->where('usuario_grupo_id', $usuario_grupo_id);
        return $this->db->get()->row();
    }

    public function retorna_usuario_grupos_ativos() {

        $this->db->from('usuario_grupo');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
