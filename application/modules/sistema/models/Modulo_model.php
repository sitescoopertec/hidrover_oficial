<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Modulo_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_modulos() {

        $this->db->from('modulo');
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_modulo($data) {
        return $this->db->insert('modulo', $data);
    }

    public function update_modulo($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('modulo', $data);
        }
    }

    public function delete_modulo($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('modulo');
        }
    }

    public function retorna_modulo($id) {

        $this->db->from('modulo');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_modulo_ativos() {

        $this->db->from('modulo');
        $this->db->where('ativo', 1);
        $this->db->order_by('ordem');
        $query = $this->db->get();
        return $query->result();
    }
}
