<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Noticia_foto_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function create_noticia_foto($data) {
        return $this->db->insert('noticia_foto', $data);
    }

    public function update_noticia_foto($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('noticia_foto', $data);
        }
    }

    public function delete_noticia_foto($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('noticia_foto');
        }
    }

    public function retorna_noticia_foto($id) {

        $this->db->from('noticia_foto');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }
    
    public function retorna_noticia_fotos_ativas() {

        $this->db->from('noticia_foto');
        $this->db->where('ativo', 1);
        $this->db->order_by('titulo');
        $query = $this->db->get();
        return $query->result();
    }

    public function retorna_noticia_fotos($noticia_id) {

        $this->db->select('nf.id, nf.noticia_id, nf.id foto_id, nf.principal, nf.nome_arquivo, nf.extensao');
        $this->db->from('noticia_foto AS nf');
        $this->db->where('nf.noticia_id', $noticia_id);
        $query = $this->db->get();
        return $query->result();
    }
}
