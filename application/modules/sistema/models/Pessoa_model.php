<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Pessoa_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_pessoas() {

        $this->db->select('pes.*, uf.sigla, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('pessoa pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->order_by('pes.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_pessoa($data) {
        return $this->db->insert('pessoa', $data);
    }

    public function update_pessoa($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('pessoa', $data);
        }
    }

    public function delete_pessoa($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('pessoa');
        }
    }

    public function retorna_pessoa($id) {
        $this->db->select('pes.*, uf.sigla, cid.unidade_federativa_id, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('pessoa pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->where('pes.id', $id);
        return $this->db->get()->row();
    }
    
    public function retorna_pessoa_por_tipo($tipo_pessoa_id) {
        $this->db->select('pes.*, uf.sigla, cid.unidade_federativa_id, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('pessoa pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->join('pessoa_tipo pt','pt.pessoa_id=pes.id');
        $this->db->where('pes.ativo', 1);
        $this->db->where('pt.tipo_pessoa_id', $tipo_pessoa_id);
          $query = $this->db->get();
        return $query->result();
    }

    public function retorna_pessoas_ativos() {

        $this->db->from('pessoa');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
