<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_menus() {

        $this->db->select('mnu.id, mnu.descricao, mnu.ordem, mnu.icone, mnu.ativo, t_mnu.descricao desc_tipo_menu, mnu.target');
        $this->db->from('menu mnu');
        $this->db->join('tipo_menu t_mnu','t_mnu.id=mnu.tipo_menu_id');
        $this->db->order_by('t_mnu.descricao, mnu.ordem');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_menu($data) {
        return $this->db->insert('menu', $data);
    }

    public function update_menu($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('menu', $data);
        }
    }

    public function delete_menu($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('menu');
        }
    }

    public function retorna_menu($id) {

        $this->db->from('menu');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_menus_ativos() {

        $this->db->from('menu');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
