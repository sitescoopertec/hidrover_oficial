<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Cidade_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_cidades() {

        $this->db->select('cid.*, uf.sigla, uf.descricao desc_uf');
        $this->db->from('cidade cid');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->order_by('uf.sigla, cid.descricao');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_cidades_estado($uf_id) {

        $this->db->select('cid.*, uf.sigla, uf.descricao desc_uf');
        $this->db->from('cidade cid');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->where('uf.id',$uf_id);
        $this->db->order_by('cid.descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_cidade($data) {
        return $this->db->insert('cidade', $data);
    }

    public function update_cidade($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('cidade', $data);
        }
    }

    public function delete_cidade($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('cidade');
        }
    }

    public function retorna_cidade($id) {

        $this->db->from('cidade');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_cidades_ativos() {

        $this->db->from('cidade');
        $this->db->where('ativo', 1);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
