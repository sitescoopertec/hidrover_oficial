<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Noticia_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_noticias($unidade_negocio_id) {
        $this->db->from('noticia');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('data','desc');
        $this->db->limit('3');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('noticia', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('noticia', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('noticia');
        }
    }
    
    public function retorna_noticia($id) {

        $this->db->from('noticia');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_noticia_ativos($unidade_negocio_id) {

        $this->db->from('noticia');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('titulo');
        $query = $this->db->get();
        return $query->result();
    }
    

    
    
}
