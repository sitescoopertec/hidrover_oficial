<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 
          <div class="row">
              <div class="col-12">
              <div class="card mb-4">
            
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo $titulo?></h4>
                    </div>
                     <div class="card-subtitle">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Novo</span>
                        </a>
                     </div>
                    <div class="table-responsive">
                        <div class="dataTables_wrapper">
                            <table  class=" table table-striped display responsive no-wrap" id="view_report_table" cellspacing="0" width="100%">
                                <thead >
                                  <tr class="bg-primary text-white">
                                    <th>Grupo Usuário</th>
                                    <th>Nome</th>
                                    <th>Ativo</th>
                                    <th>Ações</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if (isset($result)) {
                                            foreach ($result as $row) {
                                                if ($row->ativo == 1) {
                                                    $ativo = 'Sim';
                                                } else {
                                                    $ativo = 'Não';
                                                }
                                                ?>
                                                <tr>
                                                    <td><?php echo $row->desc_grupo; ?></td>
                                                    <td><?php echo $row->nome; ?></td>
                                                    <td><?php echo $ativo; ?></td>
                                                    <td class="text-white">
                                                        <a href="<?php echo $caminho_url->edit .'/'. $row->id ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i> Editar</a>
                                                        <a onclick="deleteConfirm('<?php echo $caminho_url->delete .'/' . $row->id ?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Excluir</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
              </div>
                  </div>
            
          </div>
        
<!--        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
          -->

   <!--<script src="<?php echo base_url("assets/vendor/jQuery/js/jquery-3.3.1.js")?>"></script>-->
  
   <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
 <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
  
  <!--<script src="<?php echo base_url("assets/js/demo/data-table-demo.js")?>"></script>-->
        <script type="text/javascript">
            $(document).ready(function () {
             
        
                $('#view_report_table').DataTable({
                    responsive: true, 
                    dom: 'lTf<"html5buttons"B>gtip',
                    pageLength: 25,
                    buttons: ['excel', 'pdf'],
//                    buttons: [{
//                    extend: 'collection',
//                    text: 'Export',
//                    buttons: ['excel', 'pdf']}],
                    language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
                });
            });
            function deleteConfirm(url) {
                bootbox.confirm({
              
                    message: "Tem certeza que deseja excluir?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Não'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Sim'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            window.location = url;
                        }
                    }
                });
            }
        </script>

        