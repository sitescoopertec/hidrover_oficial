<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="row">
        <div class="col-12">         
            <div class="card mb-4">               
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo 'Trocar Senha - ' . $result->nome?></h4>
                    </div>
               
                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                         
                        <fieldset class="form-group">
                          <label class="form-label">Usuário</label>
                          <input type="text" readonly="true" id="usuario" name="usuario" value="<?php echo $result->usuario ?>" required class="form-control">
                        </fieldset>
                         <fieldset class="form-group">
                          <label class="form-label">Senha</label>
                          <input type="password" id="senha" name="senha" class="form-control">
                        </fieldset>
                       
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>                            
                        </div>
                        
              </form>
                     
            </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>
