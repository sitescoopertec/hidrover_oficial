<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="row">
        <div class="col-12">         
            <div class="card mb-4">               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->nome?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Novo</span>
                        </a>
                        <a href="<?php echo base_url('sistema/usuario/unidade_negocio/' . $result->id)?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-building"></i>
                            <span class="align-middle">Unidade Negócio</span>
                        </a>
                    </div>
                       
                    <div class="col-md-6">
                        <label class="switcher">
                            <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro .'/'. $result->id ?>')" name="ativo" <?php echo ($result->ativo==='1') ? 'checked':''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                            <span class="switcher-label">Ativo</span>
                        </label>
                    </div>                    
                </div>
                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                         
                        <fieldset class="form-group">
                            <label class="form-label">Grupo Usuário</label>
                            <select id="usuario_grupo_id" name="usuario_grupo_id" class="custom-select">
                                <?php
                                foreach ($grupo_usuarios as $grp) {
                                    $selected = '';
                                    if ($result->usuario_grupo_id == $grp->id) {
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Usuário</label>
                          <input type="text" id="usuario" name="usuario" value="<?php echo $result->usuario ?>" required class="form-control">
                        </fieldset>
                         <fieldset class="form-group">
                          <label class="form-label">Senha</label>
                          <input type="password" id="senha" name="senha" class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Nome Completo</label>
                          <input type="text" id="nome" name="nome" value="<?php echo $result->nome ?>" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Email</label>
                          <input type="email" id="email" name="email"  value="<?php echo $result->email ?>" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Celular</label>
                          <input type="text" id="celular" name="celular" value="<?php echo $result->celular ?>"  required class="form-control">
                        </fieldset>
                        <?php
                            if ($_SESSION['is_admin']) {
                        ?>
                        <fieldset class="form-group">
                            <label class="form-label">Perfil de Acesso</label>
                            <select id="tipo" name="tipo" class="custom-select">
                                <option value="A" <?php echo ($result->tipo == 'A') ? 'selected' : null ?>>Administrador</option>
                                <option value="T" <?php echo ($result->tipo == 'T') ? 'selected' : null ?>>Básico</option>                              
                            </select>
                        </fieldset>
                        <?php
                        }
                        ?>
                       
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>
                        
              </form>
                     
            </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>
