<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-12">


        <div class="card mb-4">

            <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo ?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                <form method="POST"  data-parsley-validate data-toggle="validator" id="validation-form" 
                      action="<?php echo $caminho_url->create_registro ?>" 
                      class="col-md-8 col-sm-8 col-xs-12"> 

                    <fieldset class="form-group">
                        <label class="form-label">Descrição</label>
                        <input type="text" id="descricao" name="descricao" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Detalhes</label>
                        <textarea type="text" id="detalhes" rows="5" name="detalhes" required class="form-control"></textarea>
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Ícone</label>
                        <input type="text" id="icone" name="icone" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Avançar</button>
                        <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                    </div>    
                </form>

            </div>
        </div>
    </div>

</div>

<link rel="stylesheet" href="<?php echo base_url("assets/vendor/quill/css/quill.snow.css")?>">
<script src="<?php echo base_url("assets/vendor/quill/js/quill.min.js")?>"></script>

<script>
    <!-- Initialize Quill editor -->

                  var quill = new Quill('#editor', {
                    theme: 'snow'
                  });


</script>