<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" 
                          class="col-md-8 col-sm-8 col-xs-12">   
                     
                        <fieldset class="form-group">
                          <label class="form-label">Nome Fantasia</label>
                          <input type="text" id="nome_fantasia" name="nome_fantasia" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Email</label>
                          <input type="email" id="email" name="email"  required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Telefone</label>
                          <input type="text" id="telefone" name="telefone"  class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Celular</label>
                          <input type="text" id="celular" name="celular"  required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label">Grupo Usuários Padrão</label>
                            <select id="tipo_menu_id" name="usuario_grupo_id" class="custom-select">
                              <option>Selecione...</option>
                                <?php foreach ($grupo_usuarios as $grp) { ?>
                                    <option value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>
                  
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>    
              </form>
                     
            </div>
            </div>
        </div>

    </div>
