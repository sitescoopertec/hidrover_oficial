<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->descricao?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Novo</span>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <label class="switcher">
                            <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro .'/'. $result->id ?>')" name="ativo" <?php echo ($result->ativo==='1') ? 'checked':''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                            <span class="switcher-label">Ativo</span>
                        </label>
                    </div>
                    
                    </div>
                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                         
                        
                        <fieldset class="form-group">
                          <label class="form-label">Descrição</label>
                          <input type="text" id="descricao" name="descricao" value="<?php echo $result->descricao?>" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Sigla</label>
                          <input type="text" id="sigla" name="sigla" value="<?php echo $result->sigla?>" required class="form-control">
                        </fieldset>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>
                        
              </form>
                     
            </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>
