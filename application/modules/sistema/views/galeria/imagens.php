<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- page content -->
<div class="row">
    <div class="col-12">


        <div class="card mb-4">

            <div class="card-body">
                <div class="card-title mb-4">
                    <h4>Fotos Galeria</h4>
                </div>
                <form id="formulario" method="POST" action="<?php echo base_url("sistema/galeria/update_imagem"); ?>" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left"  data-toggle="validator">
                    <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12"></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <span class="red"><?php echo validation_errors(); ?></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-12 col-lg-12">
                            <article class="card mb-4">
                                <div class="card-body">
                                    <h4 class="card-title mb-4">Upload Fotos</h4>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
                                            <fieldset class="form-group">
                                                <label class="control-label" for="userfile">Foto upload</label>
                                                <input type="file" id="userfile" name="userfile" class="file-upload-default">
                                                <div class="input-group col-xs-12">
                                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                                    <span class="input-group-append">
                                                        <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                                    </span>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Salvar</button>
                            <a href="<?php echo site_url('sistema/galeria/edit/' . $result->id) ?>" class="btn btn-primary">Voltar</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        <div class="clearfix"></div>

    </div>
</div>

<!-- form validation -->

<!-- Inicio Galeria -->

<div class="col-12">
    <article class="card mb-4">
        <div class="card-body thumbnail">
            <h4 class="card-title mb-4">Imagens</h4>
            <div class="row">
                <?php
                // FOTOS
                foreach ($fotos as $fot) {
                    ?> 
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <a href="javascript:void(0)" class="overlay">
                            <span class="bg-overlay"><button onclick="deleteConfirm('<?php echo site_url('sistema/galeria/delete_imagem/' . $fot->id) ?>')" type="button" class="btn btn-danger exluir">Excluir</button></span>
                            <img class="img-galeria" src="<?php echo base_url('uploads/galeria_img/' . $result->id . '/' . $fot->nome_arquivo . $fot->extensao) ?>" alt="<?php echo $fot->nome_arquivo ?>" />
                        </a>
                    </div>
                    <?php
                }
                ?>  
            </div>
        </div>
    </article>
</div>

<!-- Final Galeria-->

<script src="<?php echo base_url("assets/js/ckeditor/ckeditor.js"); ?>"></script>

<!-- /page content -->
<script type="text/javascript">

                                    function deleteConfirm(url) {
                                        bootbox.confirm("Tem certeza que deseja excluir?", function (res) {
                                            if (res === true) {
                                                window.location = url;
                                            } else {
                                                return res;
                                            }
                                        });
                                    }
                                    function principalConfirm(url) {
                                        bootbox.confirm("Tem certeza que deseja colocar essa imagem como principal?", function (res) {
                                            if (res === true) {
                                                window.location = url;
                                            } else {
                                                return res;
                                            }
                                        });
                                    }

</script>



