<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" 
                          class="col-md-8 col-sm-8 col-xs-12"> 
                        
                        <fieldset class="form-group">
                            <label class="form-label">Categoria</label>
                            <select id="categoria_id" name="categoria_id" class="form-control">
                                <option value="">Selecione...</option>
                                <?php 
                                foreach ($categorias as $cat) { ?>
                                    <option value="<?php echo $cat->id ?>"><?php echo $cat->descricao ?></option>
                                <?php } ?>
                            </select>
                        </fieldset>
                        
                        <fieldset class="form-group">
                          <label class="form-label">Descrição</label>
                          <input type="text" id="descricao" name="descricao" class="form-control">
                        </fieldset>
                        
                        <fieldset class="form-group">
                          <label class="form-label">Detalhe</label>
                          <textarea type="text" row="20" id="detalhe" name="detalhe" class="form-control"></textarea>
                        </fieldset>
              
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>    
              </form>
                     
            </div>
            </div>
        </div>

    </div>
