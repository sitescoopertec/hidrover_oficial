<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo $titulo?></h4>
                    </div>
                    <div class="card-subtitle">


                    </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" >  
                        <div class="form-row">
                            
                       
                            <div class="form-group col-md-8 ">
                                <fieldset class="form-group">
                                  <label class="form-label">Nome Fantasia</label>
                                  <input type="text" id="nome_fantasia" name="nome_fantasia" required class="form-control">
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="form-label">Razão Social</label>
                                  <input type="text" id="razao_social" name="razao_social" required class="form-control">
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="form-label">Email</label>
                                  <input type="email" id="email" name="email"  required class="form-control">
                                </fieldset>
                                <div class="form-row">
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">CPF</label>
                                        <input type="text" id="cpf" name="cpf"  required class="form-control">
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">Telefone</label>
                                        <input type="text" id="telefone" name="telefone"  class="form-control">
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">Celular</label>
                                        <input type="text" id="celular" name="celular"  required class="form-control">
                                    </fieldset>
                                </div>
                                <div class="form-row">
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">UF</label>
                                            <select onchange="carrega_combo()" id="uf_id" name="uf_id" class="custom-select">
                                              <option>Selecione...</option>
                                                <?php foreach ($ufs as $uf) { ?>
                                                    <option value="<?php echo $uf->id ?>"><?php echo $uf->descricao ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Cidade</label>
                                            <select id="cidade_id" name="cidade_id" class="custom-select">
                                              <option>Selecione...</option>
                                                <?php foreach ($cidades as $grp) { ?>
                                                    <option value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                        <fieldset class="form-group col-md-12">
                                            <label class="form-label">Endereço</label>
                                            <input type="text" id="endereco" name="endereco"  required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Bairro</label>
                                            <input type="text" id="endereco" name="endereco"  required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">CEP</label>
                                            <input type="text" id="endereco" name="endereco"  required class="form-control">
                                        </fieldset>
                                </div>
                                <div class="form-row">
                                        <fieldset class="form-group col-md-8">
                                            <label class="form-label">Contato</label>
                                            <input type="text" id="endereco" name="endereco"  required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-4">
                                            <label class="form-label">Telefone Contato</label>
                                            <input type="text" id="endereco" name="endereco"  required class="form-control">
                                        </fieldset>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                                    <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                </div>    
                            </div>
                            <div class="col-md-3 form-group">
                                <div class="card-title mb-4">
                                    <h5>Tipo</h5>
                                </div>
                                <div class="card-subtitle">
                                    <div class="row">

                                        <?php
                                           foreach ($tipos as $mnu) {
                                               $checked = '';
                                               //foreach ($menus_salvos as $ok) {
//                                                   if ($ok->menu_id == $mnu->id) {
//                                                       $checked = 'checked';
//                                                   }
                                               //}
                                               ?>
                                           <div class="col-md-12 ">
                                               <fieldset class="form-group">
                                                   <div class="form-check">
                                                     <label class="custom-checkbox primary-checkbox">
                                                       <input <?php echo $checked ?> name="menus[]"  value="<?php echo $mnu->id ?>" class="form-check-input" type="checkbox">
                                                       <?php echo $mnu->descricao ?>
                                                       <i class="input-helper"></i>
                                                     </label>
                                                   </div>
                                               </fieldset>
                                               </div>
                                           <?php } ?>


                                    </div>
                                </div>
                            </div>

                     
                                   
                        </div>  
                    </form>
                     
                </div>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        var path = "<?php echo base_url() ?>";
        
        function reseta_combo( el ) {
            $("select[name='"+el+"']").empty();       
        } 
        
        function carrega_combo() {
            //Limpa o combo cidade
            reseta_combo('cidade_id');
            var uf = document.getElementById('uf_id');
            $.getJSON( path + 'sistema/cidade/carrega_cidades_estado/' + uf.value, function (data){
                var option = new Array();
                option[0] = document.createElement('option');
                    $( option[0] ).attr( {value : ""} );
                    $( option[0] ).append( 'Selecione...' ); 
                    $("select[name='cidade_id']").append( option[0] );         
                $.each(data, function(i, obj){ 
                    option[i] = document.createElement('option');
                    $( option[i] ).attr( {value : obj.id} );
                    $( option[i] ).append( obj.descricao ); 
                    $("select[name='cidade_id']").append( option[i] );         
                });
            });     
        }
        
//        function carrega_cargas(orcamento_ambiente_carga_id, tipo_carga_id, carga_id){
//        var combo = document.getElementById("carga_" + orcamento_ambiente_carga_id);
//     
//        if ( combo.length<2){
//            resetaCombo("carga_" + orcamento_ambiente_carga_id);
//            $.getJSON( path + '/cidade/carrega_cidades_estado/' + tipo_carga_id, function (data){
//                var option = new Array();
//                $.each(data, function(i, obj){ 
//                    option[i] = document.createElement('option');
//                    $( option[i] ).attr( {value : obj.id} );
//                    $( option[i] ).append( obj.descricao ); 
//                    $("select[name='carga_"+ orcamento_ambiente_carga_id + "']").append( option[i] );         
//                });
//            });     
//        }
//    }
    </script>
