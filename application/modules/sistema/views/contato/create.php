<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" 
                          class="col-md-8 col-sm-8 col-xs-12"> 
                        
                        <fieldset class="form-group">
                            <label for="data">Data Abertura
                            </label>
                                <input type="text" id="data" name="data" value="<?php echo date("d/m/Y");?>"  maxlength="10" class="form-control col-md-7 col-xs-12" >
                                <div class="help-block with-errors"></div>
                        </fieldset>
                       
                        <fieldset class="form-group">
                          <label class="form-label">Nome</label>
                          <input type="text" id="nome" name="nome" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Telefone</label>
                          <input type="text" id="telefone" name="telefone" required class="form-control">
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Email</label>
                          <input type="text" id="email" name="email" required class="form-control">
                        </fieldset>
                           
                        <fieldset class="form-group">
                            <label class="form-label" for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">Aberto</option>
                                    <option value="0">Encerrado</option>
                                </select>
                        </fieldset>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>    
              </form>
                     
            </div>
            </div>
        </div>

    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $("#formulario").validationEngine();
        $('#data_abertura').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            todayHighlight: true,
            autoclose: true
        });
        $('#data_abertura').mask("##/##/####");  
       
    });
    
</script>
