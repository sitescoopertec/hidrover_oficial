<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-12">         
        <div class="card mb-4">               
            <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->nome_fantasia ?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            <label class="switcher">
                                <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro . '/' . $result->id ?>')" name="ativo" <?php echo ($result->ativo === '1') ? 'checked' : ''; ?> class="switcher-input">
                                <span class="switcher-indicator">
                                </span>
                                <span class="switcher-label">Ativo</span>
                            </label>
                        </div>                    
                    </div>
                </div>
                <div class="tab-panel">
                    <div class="tab-responsive">
                        <ul class="nav nav-tabs tabs-container">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#dados">
                                    <i class="mdi mdi-home md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Dados</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#empresa">
                                    <i class="mdi mdi-city md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Sobre Empresa</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#produto">
                                    <i class="mdi mdi-camera md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Produtos</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#servico">
                                    <i class="mdi mdi-settings md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Serviço</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#redes">
                                    <i class="mdi mdi-facebook-box md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Redes Sociais</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#localizacao">
                                    <i class="mdi mdi-google-maps md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Localização</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#contato">
                                    <i class="mdi mdi-phone-in-talk md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Contato</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->update_registro ?>" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                        <div class="tab-content">
                            <div class="tab-pane active" id="dados">
                                <div class="form-row">
                                    <div class="form-group col-md-12 ">
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Nome Fantasia</label>
                                            <input type="text" id="nome_fantasia" name="nome_fantasia" value="<?php echo $result->nome_fantasia ?>" class="form-control">
                                        </fieldset> 

                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">CNPJ</label>
                                            <input type="text" id="cnpj" name="cnpj" value="<?php echo $result->cnpj ?>"   class="form-control">
                                        </fieldset> 

                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Endereço</label>
                                            <input type="text" id="endereco" name="endereco" value="<?php echo $result->endereco ?>"   class="form-control">
                                        </fieldset> 

                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Cidade</label>
                                            <input type="text" id="cidade" name="cidade" value="<?php echo $result->cidade ?>"   class="form-control">
                                        </fieldset>

                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Logo</label>
                                            <div class="col-md-55">
                                                <div class="thumbnail">
                                                    <img style="width: 30%; margin-left: 0%; display: block;" src="<?php echo base_url($result->logo) ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p style="margin-left: 0px; margin-top: 10px;">Sugestão de tamanho: 1200px X 450px</p>
                                            </div>
                                        </fieldset> 
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="empresa">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Título Empresa</label>
                                    <input type="text" id="titulo_empresa" name="titulo_empresa" value="<?php echo $result->titulo_empresa ?>"   class="form-control">
                                </fieldset>
                                
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Descrição Empresa</label>
                                    <input type="text" id="descricao_empresa" name="descricao_empresa" value="<?php echo $result->descricao_empresa ?>"   class="form-control">
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Texto Empresa</label>
                                    <textarea type="text" row="5"  id="detalhes_empresa" name="detalhes_empresa" class="form-control"><?php echo $result->detalhes_empresa ?></textarea>
                                </fieldset>
                            </div>

                            <div class="tab-pane" id="produto">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Título Produto</label>
                                    <input type="text" id="titulo_produto" name="titulo_produto" value="<?php echo $result->titulo_produto ?>"   class="form-control">
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Texto Produto</label>
                                    <textarea type="text" row="5"  id="descricao_produto" name="descricao_produto" class="form-control"><?php echo $result->descricao_produto ?></textarea>
                                </fieldset>
                            </div>

                            <div class="tab-pane" id="servico">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Serviço Titulo</label>
                                    <input type="text" id="servico_titulo" name="servico_titulo" value="<?php echo $result->servico_titulo ?>"   class="form-control">
                                </fieldset> 

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Serviço Detalhes</label>
                                    <textarea type="text" row="5"  id="servico_detalhes" name="servico_detalhes" class="form-control"><?php echo $result->servico_detalhes ?></textarea>
                                </fieldset>

                            </div>

                            <div class="tab-pane" id="redes">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Skype</label>
                                    <input type="text" id="skype" name="skype" value="<?php echo $result->skype ?>"   class="form-control">
                                </fieldset> 

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Facebook</label>
                                    <input type="text" id="face" name="face" value="<?php echo $result->face ?>"   class="form-control">
                                </fieldset> 

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Instagram</label>
                                    <input type="text" id="insta" name="insta" value="<?php echo $result->insta ?>"   class="form-control">
                                </fieldset> 
                            </div>

                            <div class="tab-pane" id="localizacao">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Titulo Localização</label>
                                    <input type="text" id="titulo_localizacao" name="titulo_localizacao" value="<?php echo $result->titulo_localizacao ?>"   class="form-control">
                                </fieldset> 

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Texto Localização</label>
                                    <textarea type="text" row="5"  id="texto_localizacao" name="texto_localizacao" class="form-control"><?php echo $result->texto_localizacao ?></textarea>
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Mapa</label>
                                    <textarea type="text" row="20"  id="mapa" name="mapa" class="form-control"><?php echo $result->mapa ?></textarea>
                                </fieldset>
                            </div>

                            <div class="tab-pane" id="contato">
                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Contato Titulo</label>
                                    <input type="text" id="contato_titulo" name="contato_titulo" value="<?php echo $result->contato_titulo ?>"   class="form-control">
                                </fieldset> 

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Contato Texto</label>
                                    <textarea type="text" row="5"  id="contato_texto" name="contato_texto" class="form-control"><?php echo $result->contato_texto ?></textarea>
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Email</label>
                                    <input type="email" id="email" name="email" value="<?php echo $result->email ?>"   class="form-control">
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Telefone</label>
                                    <input type="text" id="telefone" name="telefone" value="<?php echo $result->telefone ?>"   class="form-control">
                                </fieldset>

                                <fieldset class="form-group col-md-6">
                                    <label class="form-label">Celular</label>
                                    <input type="text" id="celular" name="celular" value="<?php echo $result->celular ?>"   class="form-control">
                                </fieldset> 

                            </div>

                            <div class="card-footer ">
                                <button type="submit" class="btn btn-success submit-btn mr-2 mb-4">Salvar</button>
                                <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                            </div>   
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function ativar_registro(url) {
        var ativo = document.getElementById('ativo');
        var valor = 0;
        if (ativo.checked) {
            valor = 1;
        }
        $.ajax({
            url: url,
            type: "POST",
            datatype: "html",
            "data": {valor: valor}
        });
    }
</script>
