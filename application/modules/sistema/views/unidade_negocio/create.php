<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                 <div class="tab-panel">
                    <div class="tab-responsive">
                        <ul class="nav nav-tabs tabs-container">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#dados">
                                    <i class="mdi mdi-home md-18 align-middle mr-2"></i>
                                    <span class="lh-3 align-middle d-inline-block">Dados</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="dados">
                            <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" action="<?php echo $caminho_url->create_registro ?>">
                                <div class="form-row">
                                    <div class="form-group col-md-12 ">
                                        <div class="form-row">
                                            <fieldset class="form-group col-md-3">
                                                <label class="form-label">Nome Fantasia</label>
                                                <input type="text" id="nome_fantasia" name="nome_fantasia" required class="form-control">
                                            </fieldset> 

                                            <fieldset class="form-group col-md-3">
                                                <label class="form-label">CNPJ</label>
                                                <input type="text" id="cnpj" name="cnpj" required class="form-control">
                                            </fieldset> 

                                            <fieldset class="form-group col-md-3">
                                                <label class="form-label">Email</label>
                                                <input type="email" id="email" name="email" required class="form-control">
                                            </fieldset> 

                                            <fieldset class="form-group col-md-3">
                                                <label class="form-label">Telefone</label>
                                                <input type="text" id="telefone" name="telefone" required class="form-control">
                                            </fieldset>  
                                        </div>

                                        <div class="form-row">
                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Celular</label>
                                                <input type="text" id="celular" name="celular" required class="form-control">
                                            </fieldset> 

                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Endereço</label>
                                                <input type="text" id="endereco" name="endereco" required class="form-control">
                                            </fieldset> 

                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Cidade</label>
                                                <input type="text" id="cidade" name="cidade" required class="form-control">
                                            </fieldset> 
                                        </div>

                                        <div class="form-row">
                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Sobre a Empresa</label>
                                                <textarea type="text" row="5"  id="sobre_empresa" name="sobre_empresa" class="form-control"></textarea>
                                            </fieldset>
                                            
                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Titulo Carrousel</label>
                                                <textarea type="text" row="5"  id="titulo_carosel" name="titulo_carosel" class="form-control"></textarea>
                                            </fieldset> 

                                            <fieldset class="form-group col-md-4">
                                                <label class="form-label">Descrição Carrousel</label>
                                                <textarea type="text" row="5"  id="descricao_carosel" name="descricao_carosel" class="form-control"></textarea>
                                            </fieldset>
                                        </div>
                                        
                                        <div class="form-row">
                                            <fieldset class="form-group col-md-6">
                                                <label class="form-label">Serviço Titulo</label>
                                                <textarea type="text" row="5"  id="servico_titulo" name="servico_titulo" class="form-control"></textarea>
                                            </fieldset>
                                            
                                            <fieldset class="form-group col-md-6">
                                                <label class="form-label">Serviço Detalhes</label>
                                                <textarea type="text" row="5"  id="servico_detalhes" name="servico_detalhes" class="form-control"></textarea>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="card-footer ">
                                        <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Próximo</button>
                                        <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                    </div>    
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
