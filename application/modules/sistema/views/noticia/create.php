<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col-12">


        <div class="card mb-4">

            <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo ?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                <form method="POST"  data-parsley-validate data-toggle="validator" id="validation-form" 
                      action="<?php echo $caminho_url->create_registro ?>" 
                      class="col-md-8 col-sm-8 col-xs-12"> 

                    <fieldset class="form-group">
                        <label class="form-label">Titulo</label>
                        <input type="text" id="titulo" name="titulo" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Subtitulo</label>
                        <input type="text" id="subtitulo" name="subtitulo" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Texto</label>
                        <textarea type="text" id="texto" rows="15" name="texto" required class="form-control"></textarea>
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Titulo da Fonte</label>
                        <input type="text" id="titulo_fonte" name="titulo_fonte" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>
                    
                    <fieldset class="form-group">
                        <label class="form-label">Link da Fonte</label>
                        <input type="text" id="link_fonte" name="link_fonte" required class="form-control">
                        <div class="help-block with-errors"></div>
                    </fieldset>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Avançar</button>
                        <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                    </div>    
                </form>

            </div>
        </div>
    </div>

</div>

<link rel="stylesheet" href="<?php echo base_url("assets/vendor/quill/css/quill.snow.css")?>">
<script src="<?php echo base_url("assets/vendor/quill/js/quill.min.js")?>"></script>

<script>
    <!-- Initialize Quill editor -->

                  var quill = new Quill('#editor', {
                    theme: 'snow'
                  });


</script>