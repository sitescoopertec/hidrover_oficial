<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo?></h4>
                </div>
                <div class="card-subtitle">


                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" 
                          class="col-md-8 col-sm-8 col-xs-12"> 
                       
                        <fieldset class="form-group">
                          <label class="form-label">Descrição</label>
                          <input type="text" id="descricao" name="descricao" class="form-control">
                        </fieldset>
                        
                        <fieldset class="form-group">
                          <label class="form-label">Título</label>
                          <input type="text"  id="titulo" name="titulo" class="form-control">
                        </fieldset>
                        
                        <fieldset class="form-group">
                            <label class="form-label">Texto</label>
                            <textarea type="text"   id="texto" row="15" name="texto" class="form-control"></textarea>
                        </fieldset>
              
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>    
              </form>
                     
            </div>
            </div>
        </div>

    </div>
