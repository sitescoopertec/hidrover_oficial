<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->descricao?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Novo</span>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <label class="switcher">
                            <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro .'/'. $result->id ?>')" name="ativo" <?php echo ($result->ativo==='1') ? 'checked':''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                            <span class="switcher-label">Ativo</span>
                        </label>
                    </div>
                    
                    </div>
                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>" enctype="multipart/form-data" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                         
                        
                        <fieldset class="form-group">
                          <label class="form-label">Descrição</label>
                          <input type="text" id="descricao" name="descricao" value="<?php echo $result->descricao?>" class="form-control">
                        </fieldset>
                        
                        <fieldset class="form-group">
                          <label class="form-label">Título</label>
                          <input type="text" id="titulo" name="titulo" value="<?php echo $result->titulo?>" class="form-control">
                        </fieldset>
                       
                        <fieldset class="form-group">
                            <label class="form-label">Texto</label>
                            <textarea type="text" id="texto" rows="15" name="texto" class="form-control"><?php echo $result->texto?></textarea>
                        </fieldset>
                        
                        <fieldset class="form-group">
                          <label class="form-label">Banner</label>
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <img style="width: 30%; margin-left: 0%; display: block;" src="<?php echo base_url($result->banner) ?>" alt="Banner">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" id="userfile" name="userfile" class="col-md-7 col-xs-12" >
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p style="margin-left: 0px; margin-top: 10px;">Sugestão de tamanho: 1200px X 450px</p>
                            </div>
                        </fieldset>
                     
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>
                        
              </form>
                     
            </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>
