<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 
          <div class="row">
              <div class="col-12">
              <div class="card mb-4">
            
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4>Itens de Instalação - <?php echo $result->descricao; ?> </h4>
                    </div>
                    
                     <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form"
                           action="<?php echo base_url('servico/servico/create_item') ?>" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="servico_id" id="servico_id" value="<?php echo $result->id; ?>">
                        <fieldset class="form-group">
                            <label class="form-label">Itens Instalação</label>
                            <select id="item_id" name="item_id"  class="custom-select buscador">
                              
                                <?php foreach ($itens as $ite) { ?>
                                    <option value="<?php echo $ite->id ?>"><?php echo $ite->descricao ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </fieldset>
                        <fieldset class="form-group">
                          <label class="form-label">Quantidade</label>
                          <input type="text" id="qtd" name="qtd" required class="form-control">
                          <div class="help-block with-errors"></div>
                        </fieldset>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Adicionar</button>
                            <a href="<?php echo $caminho_url->edit .'/'.$result->id  ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>
                     </form>
                </div>
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4>Lista de itens para instalação</h4>
                    </div>
                
                    <div class="table-responsive">
                        <div class="dataTables_wrapper">
                            <table  class=" table table-striped display responsive no-wrap" id="view_report_table" cellspacing="0" width="100%">
                                <thead >
                                  <tr class="bg-primary text-white">
                                    <th>Item</th>                                    
                                    <th>Qtd</th>    
                                    <th>Ações</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if (isset($prod_itens)) {
                                            foreach ($prod_itens as $row) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row->desc_item; ?></td>
                                                    <td><?php echo databasetonumber($row->qtd); ?></td>
                                                    <td class="text-white">                                                    
                                                        <a onclick="deleteConfirm('<?php echo base_url('servico/servico/delete_item' .'/' . $row->id) ?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Excluir</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>  
              </div>
                  </div>
            
          </div>
        
<!--        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
          -->

   <!--<script src="<?php echo base_url("assets/vendor/jQuery/js/jquery-3.3.1.js")?>"></script>-->
  
   <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
 <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
  
  <!--<script src="<?php echo base_url("assets/js/demo/data-table-demo.js")?>"></script>-->
        <script type="text/javascript">
            $(document).ready(function () {
          $('#view_report_table').DataTable({
                    responsive: true, 
                    dom: 'lTf<"html5buttons"B>gtip',
                    pageLength: 25,
                    
                    "processing": true,
                    "deferRender": true,
                    "autoWidth": false,
                    "filter": false,
                    "orderMulti": false,
                    "bSort": false,
                    "paging": false,
                    "bInfo" : false,
                    buttons: [],

                    language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
                });
            });
            function deleteConfirm(url) {
                bootbox.confirm({
              
                    message: "Tem certeza que deseja excluir?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Não'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Sim'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            window.location = url;
                        }
                    }
                });
            }
        </script>

        