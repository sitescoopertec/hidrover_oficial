<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->descricao?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle"> Novo</span>
                        </a>
                        <a href="<?php echo base_url("servico/servico/item/" . $result->id)?>" class="btn btn-success  btn-sm">       
                            <i class="fa fa-list"></i><span class="align-middle"> Itens Instalação</span>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <label class="switcher">
                            <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro .'/'. $result->id ?>')" name="ativo" <?php echo ($result->ativo==='1') ? 'checked':''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                            <span class="switcher-label">Ativo</span>
                        </label>
                    </div>
                    
                    </div>
                </div>
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>" class="col-md-8 col-sm-8 col-xs-12">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                         
                        
                        <fieldset class="form-group">
                          <label class="form-label">Descrição</label>
                          <input type="text" id="descricao" name="descricao" value="<?php echo $result->descricao?>" required class="form-control">
                        </fieldset>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                            <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                        </div>
                        
              </form>
            
            </div>
                 <div class="card-body">
                    <div class="card-title mb-4">
                        <h4>Lista de itens para instalação</h4>
                    </div>
                
                    <div class="table-responsive">
                        <div class="dataTables_wrapper">
                            <table  class=" table table-striped display responsive no-wrap" id="view_report_table" cellspacing="0" width="100%">
                                <thead >
                                  <tr class="bg-primary text-white">
                                    <th>Item</th>                                    
                                    <th>Qtd</th>  
                                    
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if (isset($prod_itens)) {
                                            foreach ($prod_itens as $row) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row->desc_item; ?></td>
                                                    <td><?php echo databasetonumber($row->qtd); ?></td>                                                  
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                
        </div>

    </div>

   <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
 <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
  


 
    <script type="text/javascript">
            $(document).ready(function () {
             
        
                $('#view_report_table').DataTable({
                    responsive: true, 
                    dom: 'lTf<"html5buttons"B>gtip',
                    pageLength: 25,
                    
                    "processing": true,
                    "deferRender": true,
                    "autoWidth": false,
                    "filter": false,
                    "orderMulti": false,
                    "bSort": false,
                    "paging": false,
                    "bInfo" : false,
                    buttons: [],

                    language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
                });
            });
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>

    

