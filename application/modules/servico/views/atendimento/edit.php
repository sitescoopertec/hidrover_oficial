<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="row">
        <div class="col-12">         
            <div class="card mb-4">               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo 'AT Nº ' . $result->id . " - " . $result->nome_tecnico?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                                <i class="fa fa-file"></i>
                                <span class="align-middle">Novo</span>
                            </a>                       
                        </div>                                
                    </div>
                </div>
                <div class="form-group col-md-12 ">
                    <div class="tab-panel">    
                            <div class="tab-responsive">
                                <ul class="nav nav-tabs tabs-container">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#dados">
                                            <i class="mdi mdi-home md-18 align-middle mr-2"></i>
                                            <span class="lh-3 align-middle d-inline-block">Dados</span>
                                        </a>
                                    </li>  
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ordem_servico">
                                            <i class="fab fa-product-hunt md-18 align-middle mr-2"></i>
                                            <span class="lh-3 align-middle d-inline-block">Ordens de Serviços</span>
                                        </a>
                                    </li> 
                                    
                                </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="dados">
                                <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" action="<?php echo $caminho_url->update_registro ?>">
                                    <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                                    <div class="form-row">
                                        <fieldset class="form-group col-md-4">
                                            <label class="form-label">Data Agendamento</label>
                                            <input type="text"  maxlength="10" value="<?php echo isset($result->data_agendamento) ? date("d/m/Y", strtotime($result->data_agendamento)) : null ?>" id="data_agendamento" name="data_agendamento" value="" class="form-control">
                                        </fieldset> 

                                        <fieldset class="form-group col-md-4">
                                            <label class="form-label">Data Realizada</label>
                                            <input type="text"  maxlength="10"  readonly="true" value="<?php echo isset($result->data_realizado) ? date("d/m/Y", strtotime($result->data_realizado)) : null ?>" id="data_realizado" name="data_realizado" value="" class="form-control">
                                        </fieldset> 
                                        <fieldset class="form-group col-md-4">
                                            <label class="form-label">Status</label>
                                            <select id="atendimento_status_id" name="atendimento_status_id" class="custom-select">
                                                <?php
                                                foreach ($status as $fa) {
                                                          $selected = '';
                                                        if ($result->atendimento_status_id == $st->id) {
                                                            $selected = 'selected';
                                                        }
                                                    ?>
                                                        <option <?php echo $selected ?> value="<?php echo $fa->id ?>"><?php echo $fa->descricao ?></option>
                                                    <?php } ?>
                                            </select>
                                        </fieldset>
                                    </div>


                                    <div class="form-row">
                                        <fieldset class="form-group col-md-12">
                                            <label class="form-label">Técnico Responsável </label>
                                            <select id="tecnico_id" name="tecnico_id" class="custom-select">
                                                <option  value="">Selecione...</option>
                                                <?php
                                                foreach ($tecnicos as $fa) {
                                                          $selected = '';
                                                        if ($result->tecnico_id == $fa->id) {
                                                            $selected = 'selected';
                                                        }
                                                    ?>
                                                        <option <?php echo $selected ?> value="<?php echo $fa->id ?>"><?php echo $fa->nome ?></option>
                                                    <?php } ?>
                                            </select>
                                        </fieldset>

                                    </div>

                                    <div class="form-row">
                                       <fieldset class="form-group col-md-12">
                                            <label class="form-label">Observação</label>
                                            <textarea type="text" row="5"  id="observacao" name="observacao" class="form-control"><?php echo $result->observacao ?></textarea>
                                        </fieldset> 

                                    </div>
                                
                                    <div class="card-footer ">
                                        <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                                        <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                    </div>  


                                </form>       
                        </div>
                            <div class="tab-pane show" id="ordem_servico">
                                <button type="button" class="btn btn-primary btn-outline btn-sm" data-toggle="modal" data-target="#modal_os">
                                    <i class="fa fa-plus md-16"></i> Novas OS
                                </button>
                                
                                <div id="sec_tabela_ordens">
                                    
                                </div>
                                
                                
                                
                            </div>
                            
                     </div>
                        </div>
                </div>
            </div>
        </div>

    </div>
 </div>
    <!--######################### INICIO MODAL PRODUTO ################################-->
    <div class="modal modal-default" id="modal_os">
    <div class="modal-dialog modal-lg">
      <form class="modal-content" method="POST"  data-parsley-validate data-toggle="validator" id="validation-form"  >
        <div class="modal-header">
          <h5 class="modal-title">
            Lista de Ordens de Serviços
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
        </div>
          
        <input type="hidden" id="ordem_servico_servico_id" value="-1">
        <div class="modal-body">
            <div class="card-title mb-4">
                <div class="form-row">
                    <fieldset class="form-group col-md-12">
                        <label class="form-label">Técnico Responsável </label>
                        <select id="tecnico_id_m" name="tecnico_id_m" class="custom-select">
                            <option  value="-1">Selecione...</option>
                            <?php
                            foreach ($tecnicos as $fa) {
                                      
                                ?>
                                    <option <?php echo $selected ?> value="<?php echo $fa->id ?>"><?php echo $fa->nome ?></option>
                                <?php } ?>
                        </select>
                    </fieldset>
                </div>                                        
                
                <div class="form-row">
                    <fieldset class="form-group col-md-5">
                        <label class="form-label">Período Inicial</label>
                        <input type="text"  maxlength="10" value="<?php echo date('d/m/Y', strtotime('-7 days')) ?>" id="data_inicial" name="data_inicial" value="" class="form-control">
                    </fieldset> 

                    <fieldset class="form-group col-md-5">
                        <label class="form-label">Período Final</label>
                        <input type="text"  maxlength="10"  value="<?php echo date('d/m/Y', strtotime('+7 days')) ?>" id="data_final" name="data_final" value="" class="form-control">
                    </fieldset> 
                    <fieldset class="form-group col-md-2">
                        <label class="form-label">Pesquisar OS</label>
                        <a onclick="carrega_ordem_servico()" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Buscar</span>
                        </a>
                    </fieldset>    
                </div>
                <div id="sec_os">
                    
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button  class="btn btn-light waves-effect" data-dismiss="modal">Fechar</button>
            <a type="submit" onclick="salvar_ordens()" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Adicionar</a>
        </div>
      </form>
    </div>
  </div>
    

    <!--######################### FINAL MODAL PRODUTO ################################-->

    

    

    <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
    <link href="<?php echo base_url("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css")?>" />
    <script src="<?php echo base_url("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js")?>"></script>
    
    <script type="text/javascript">
         var path = '<?php echo base_url()?>';
        $(document).ready(function () {
            $('#view_report_table').DataTable({
                    responsive: true, 
                    dom: 'lTf<"html5buttons"B>gtip',
                    pageLength: 25,
                    buttons: [],
                    "paging":   false,
                    "bDestroy": true,
                    "info":     false,
                    language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
            });
            
   
           

            $('#data_agendamento').datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR",
                todayHighlight: true,
                autoclose: true
            });
            $('#data_realizado').datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR",
                todayHighlight: true,
                autoclose: true
            });    
            
            
            $('#data_inicial').datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR",
                todayHighlight: true,
                autoclose: true
            });
            $('#data_final').datepicker({
                format: "dd/mm/yyyy",
                language: "pt-BR",
                todayHighlight: true,
                autoclose: true
            });
//            $('#data_criacao').mask("##/##/####");
//            $('#data_planejamento').mask("##/##/####");
//            $('#data_realizado').mask("##/##/####");

           // carrega_combo_servico_os();
            
           //carrega_combo_tecnico();
           carrega_tabela_ordens();
        });
    
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
        
        

        function carrega_ordem_servico(){
            var atendimento_id = document.getElementById("id").value;
            var tecnico_id_m = document.getElementById("tecnico_id_m").value;
            var data_inicial = document.getElementById("data_inicial").value;
            var data_final = document.getElementById("data_final").value;
            console.log(tecnico_id_m + '+' + data_inicial + '+' + data_final)
            $.ajax({
                    url: path + "servico/atendimento/retorna_ordem_servico",
                    type: "POST",
                    datatype: "json",
                    "data": {atendimento_id: atendimento_id,
                            tecnico_id: tecnico_id_m,
                            data_inicial: data_inicial,
                            data_final: data_final}  ,
                    success: function (data) {
                       var form = jQuery.parseJSON(data); 
                        $('#sec_os').empty();
                        $('#sec_os').html(form.ordens);           
            
                } 
                });
            
        }
        
        function salvar_ordens() {
            
            var atendimento_id = document.getElementById('id').value;
//            var ordens = document.getElementById("ordens").values;
            var checked = []; 
            $("input[name='ordens[]']:checked").each(function () 
            {
                checked.push(parseInt($(this).val()));
            });
            console.log('Array - ' + checked);
            $.ajax({
                url: path + "servico/atendimento/salvar_os_atendimento",
                type: "POST",
                datatype: "html",
                "data": {atendimento_id: atendimento_id,
                         ordens: checked}  ,
                 success: function () {
                        carrega_ordem_servico();
                        carrega_tabela_ordens();
                 }
            });
        }
        
         function deleteConfirm(url) {
                bootbox.confirm({
              
                    message: "Tem certeza que deseja excluir?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Não'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Sim'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            $.ajax({
                                    url: url,
                                    type: "POST",
                                    datatype: "json",
                                    success: function (data) {
                                        carrega_tabela_ordens();
                                    } 
                                });
                        }
                    }
                });
        }
        
        
        
        // Equipe
        
        function carrega_tabela_ordens(){
            var atendimento_id = document.getElementById("id").value;
            $('#sec_tabela_ordens').empty();
            $('#sec_tabela_ordens').load(path + "servico/atendimento/retorna_ordens_atendimento/" + atendimento_id);           
            
        }
             
       
       

        
         
    </script>
