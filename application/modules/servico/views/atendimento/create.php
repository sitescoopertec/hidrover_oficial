<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo $titulo?></h4>
                    </div>
                    <div class="card-subtitle">


                    </div>
                
                         <div class="tab-panel">    
                            <div class="tab-responsive">
                                <ul class="nav nav-tabs tabs-container">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#dados">
                                            <i class="mdi mdi-home md-18 align-middle mr-2"></i>
                                            <span class="lh-3 align-middle d-inline-block">Dados</span>
                                        </a>
                                    </li>  
                                </ul>
                            </div>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="dados">
                                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" action="<?php echo $caminho_url->create_registro ?>">
                                             <div class="form-row">

                                             <div class="form-group col-md-12 ">
                                                 <div class="form-row">
                                                     <fieldset class="form-group col-md-3">
                                                         <label class="form-label">Data Agendamento</label>
                                                         <input type="text"  maxlength="10" value="<?php echo date("d/m/Y")?>" id="data_agendamento" name="data_agendamento" value="" class="form-control">
                                                     </fieldset> 

                                                     <fieldset class="form-group col-md-3">
                                                         <label class="form-label">Data Realizada</label>
                                                         <input type="text"  maxlength="10"  readonly="true" id="data_realizado" name="data_realizado" value="" class="form-control">
                                                     </fieldset> 

                                                 </div>


                                                     <div class="form-row">
                                                         <fieldset class="form-group col-md-5">
                                                             <label class="form-label">Técnico Responsável </label>
                                                             <select id="tecnico_id" name="tecnico_id" class="custom-select">
                                                                 <option  value="">Selecione...</option>
                                                                 <?php
                                                                 foreach ($tecnicos as $fa) {
                                                                     ?>
                                                                     <option value="<?php echo $fa->id ?>"><?php echo $fa->nome ?></option>
                                                                 <?php } ?>
                                                             </select>
                                                         </fieldset>
                                                         <fieldset class="form-group col-md-4">
                                                             <label class="form-label">Status</label>
                                                             <select id="atendimento_status_id" name="atendimento_status_id" class="custom-select">
                                                                 <?php
                                                                 foreach ($status as $fa) {
                                                                     ?>
                                                                     <option value="<?php echo $fa->id ?>"><?php echo $fa->descricao ?></option>
                                                                 <?php } ?>
                                                             </select>
                                                         </fieldset>
                                                     </div>

                                                     <div class="form-row">
                                                        <fieldset class="form-group col-md-12">
                                                             <label class="form-label">Observação</label>
                                                             <textarea type="text" row="5"  id="observacao" name="observacao" class="form-control"></textarea>
                                                         </fieldset> 

                                                     </div>
                                             </div>
                                             <div class="card-footer ">
                                                 <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                                                 <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                             </div>    
                                         </div>

                                 </form>
                           </div>
                            
                        </div>
                    </div>
                    
                                          
                </div>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        var path = "<?php echo base_url() ?>";
        
        function reseta_combo( el ) {
            $("select[name='"+el+"']").empty();       
        } 
        
        function carrega_projetos_cliente() {
            //Limpa o combo cidade
            reseta_combo('projeto_id');
            var cliente = document.getElementById('cliente_id');
            $.getJSON( path + 'servico/ordem_servico/carrega_projetos_cliente/' + cliente.value, function (data){
                var option = new Array();
                option[0] = document.createElement('option');
                    $( option[0] ).attr( {value : ""} );
                    $( option[0] ).append( 'Selecione...' ); 
                    $("select[name='projeto_id']").append( option[0] );         
                $.each(data, function(i, obj){ 
                    option[i] = document.createElement('option');
                    $( option[i] ).attr( {value : obj.id} );
                    $( option[i] ).append( obj.descricao ); 
                    $("select[name='projeto_id']").append( option[i] );         
                });
            });     
        }
        function carrega_fases_projeto() {
            //Limpa o combo cidade
            reseta_combo('projeto_fase_id');
            var projeto = document.getElementById('projeto_id');
            $.getJSON( path + 'servico/ordem_servico/carrega_fases_projeto/' + projeto.value, function (data){
                var option = new Array();
                option[0] = document.createElement('option');
                    $( option[0] ).attr( {value : ""} );
                    $( option[0] ).append( 'Selecione...' ); 
                    $("select[name='projeto_fase_id']").append( option[0] );         
                $.each(data, function(i, obj){ 
                    option[i] = document.createElement('option');
                    $( option[i] ).attr( {value : obj.id} );
                    $( option[i] ).append( obj.descricao ); 
                    $("select[name='projeto_fase_id']").append( option[i] );         
                });
            });     
        }
//        function carrega_cargas(orcamento_ambiente_carga_id, tipo_carga_id, carga_id){
//        var combo = document.getElementById("carga_" + orcamento_ambiente_carga_id);
//     
//        if ( combo.length<2){
//            resetaCombo("carga_" + orcamento_ambiente_carga_id);
//            $.getJSON( path + '/cidade/carrega_cidades_estado/' + tipo_carga_id, function (data){
//                var option = new Array();
//                $.each(data, function(i, obj){ 
//                    option[i] = document.createElement('option');
//                    $( option[i] ).attr( {value : obj.id} );
//                    $( option[i] ).append( obj.descricao ); 
//                    $("select[name='carga_"+ orcamento_ambiente_carga_id + "']").append( option[i] );         
//                });
//            });     
//        }
//    }
    </script>
