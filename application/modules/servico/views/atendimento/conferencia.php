<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 
          <div class="row">
              <div class="col-12 col-xl-12">
              <div class="card mb-4">
            
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4>Conferência de Itens Atendimento Nº <?php echo $result->id?> </h4>
                    </div>
                     <div class="card-subtitle">
                        
                     </div>
                    <div class="table-responsive">
                        <div class="col-12 col-sm-12 col-lg-12">
                            <article class="card mb-4">
                                <div class="card-body">
                                    <div class="list-wrapper">
                                        <ul class="d-flex flex-column type-badge-list">
                                            <?php foreach ($itens_conferencia as $row){ ?>
                                            <li class="completed">
                                                <div class="form-check">
                                                    <label class="custom-checkbox variation3 mb-0">
                                                        <input class="form-check-input" type="checkbox" value="<?php echo $row->conferido?>" onchange="conferencia_item('<?php echo $row->id?>','<?php echo $row->conferido?>')"  <?php echo ($row->conferido == '1' ? 'checked' : '') ?> > <?php echo $row->desc_item . " - Qtd: " . $row->qtd_necessaria ?>                                           <i class="input-helper"></i>
                                                    </label>
                                                </div>                        
                                            </li>                      
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="card-footer ">
                                        <a href="<?php echo base_url('servico/atendimento/finalizar_conferencia/' . $conferencia->id) ?>" class="btn btn-success submit-btn with-arrow mr-2 mb-4">Finalizar</a>                                        
                                        <a href="<?php echo base_url('servico/atendimento/edit/' . $result->id ) ?>" class="btn mr-2 mb-4">Voltar</a>
                                    </div> 
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              
    </div>
        

   <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
  <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
 <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
  
  <!--<script src="<?php echo base_url("assets/js/demo/data-table-demo.js")?>"></script>-->
        <script type="text/javascript">
            var path = '<?php echo base_url()?>'
            $(document).ready(function () {
             
        
  
            });
            function conferencia_item(conferencia_item_id, conferido ) {
                if (conferido == 0 ){
                    conferido = 1;
                } else {          
                    conferido = 0;
                }   
                
                
                $.ajax({
                    url: path + '/servico/atendimento/atualiza_item_conferencia',
                    type: "POST",
                    datatype: "html",
                    "data": {conferencia_item_id : conferencia_item_id,
                            conferido: conferido}                     
                });
            }
           ;
        function deleteConfirm(url) {
                bootbox.confirm({
              
                    message: "Tem certeza que deseja excluir?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Não'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Sim'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            window.location = url;
                        }
                    }
                });
            }
        </script>

        