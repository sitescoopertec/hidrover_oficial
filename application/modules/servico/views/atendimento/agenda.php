<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link href="<?php echo base_url("assets/fullcallendar/core/main.css")?>" rel="stylesheet" />
<link href="<?php echo base_url("assets/fullcallendar/daygrid/main.css")?>" rel='stylesheet' />

<link href="<?php echo base_url("assets/fullcallendar/timegrid/main.css")?>" rel='stylesheet' />
<link href="<?php echo base_url("assets/fullcallendar/list/main.css")?>" rel='stylesheet' />
<script src="<?php echo base_url("assets/fullcallendar/core/main.js")?>"></script>
<script src="<?php echo base_url("assets/fullcallendar/core/locales-all.js")?>"></script>
<script src="<?php echo base_url("assets/fullcallendar/interaction/main.js")?>"></script>
<script src="<?php echo base_url("assets/fullcallendar/daygrid/main.js")?>"></script>
<script src="<?php echo base_url("assets/fullcallendar/timegrid/main.js")?>"></script>
<script src="<?php echo base_url("assets/fullcallendar/list/main.js")?>"></script>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo $titulo?></h4>
                        
                    </div>
                        <div class="card-subtitle">
                            <button type="button" class="btn btn-primary btn-outline btn-sm" data-toggle="modal" data-target="#modal_atendimento">
                                <i class="fa fa-plus md-16"></i> Novo Atendimento
                            </button>
                        </div>
                    
                        <div id='calendar'></div>
                
                </div>
            </div>
        </div>
            
    <!--###########################   INICIO MODAL Atendimento ################################-->
  <div class="modal modal-default" id="modal_atendimento">
    <div class="modal-dialog modal-lg">
      <form class="modal-content" method="POST"  data-parsley-validate data-toggle="validator" id="validation-form"  >
        <div class="modal-header">
          <h5 class="modal-title">
            Nova Pergunta            
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
        </div>
          
           <input type="hidden" id="pergunta_id" value="-1">
        <div class="modal-body">
           
              <div class="form-row">
                <div class="form-group col">
                    <label class="switcher">
                        <input type="checkbox" id="ativo"  checked="true" name="ativo" class=" switcher-input">
                        <span class="switcher-indicator">
                        </span>
                        <span class="switcher-label">Ativo</span>
                    </label>
                </div>
            </div>  
            <div class="form-row">
                <div class="form-group col">
                    <label class="form-label">Pergunta Pai</label>
                    <select id="pergunta_pai_id" name="pergunta_pai_id"  class="custom-select">      
                        <option value="" >-- Nulo --</option>
                    </select>
                </div>
                
              
            </div>                                         
            <div class="form-row">
                <div class="form-group col">
                    <label class="form-label">Pergunta</label>
                    <textarea type="text" rows="3"  required class="form-control"  id="pergunta" name="pergunta" ></textarea>
                    <div class="help-block with-errors"></div>
                </div>           
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label class="form-label">Alertas</label>
                    <textarea type="text" rows="3"  class="form-control mb-12"  id="alerta" name="alerta" ></textarea>
                </div>           
            </div>    
            <div class="form-row">
                <div class="form-group col">
                    <label class="form-label">Ordem</label>
                    <input type="number" class="form-control mb-4"  required  id="ordem" name="ordem" >
                </div>           
            </div> 
        </div>
        <div class="modal-footer">
            <button  class="btn btn-light waves-effect" data-dismiss="modal">Fechar</button>
            <a onclick="delete_pergunta()" id="btn_excluir" hidden class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Adicionar</a>
          <a type="submit" onclick="salvar_pergunta()" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Adicionar</a>
        </div>
      </form>
    </div>
  </div>
<!--###########################   FINAL MODAL Atendimento ################################-->

                   
<script>
var path = '<?php echo base_url()?>';
  document.addEventListener('DOMContentLoaded', function() {

    
     $.ajax({
                    url: path + "servico/atendimento/retorna_atendimentos",
                    type: "POST",
                    datatype: "json",
//                    "data": {formulario_id: formulario_id}  ,
                    success: function (data) {
                            var calendarEl = document.getElementById('calendar');
                       var atendimentos = jQuery.parseJSON(data); 
                        
                    var calendar = new FullCalendar.Calendar(calendarEl, {
                      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                      height: 'parent',
                      locale: "pt-br",
                      header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                      },
                      defaultView: 'timeGridWeek',
                      defaultDate: '2019-05-06',
                      minTime: '07:00:00',
                      maxTime: '20:00:00',
                      //hiddenDays: [ 0, 6 ], //Esconde sabado e domingo
                      navLinks: true, // can click day/week names to navigate views
                      editable: true,
                      eventLimit: true, // allow "more" link when too many events
                      events: atendimentos
                    });

                    calendar.render();
                 } 
                });



  });

</script>