


<div class="table-responsive">
    <div class="dataTables_wrapper">
        <table  class=" table table-striped display responsive no-wrap" id="tabela_ordens" cellspacing="0" width="100%">
            <thead >
              <tr class="bg-primary text-white">
                <th>OS</th>                                     
                <th>Data Planejamento</th>                                     
                <th>Cliente</th>                                     
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
                <?php
                    if (isset($lista_ordens)) {
                        foreach ($lista_ordens as $row) {
                            ?>
                            <tr>
                                <td ><?php echo  $row->ordem_servico_id; ?></td>
                                <td ><?php echo date('d/m/Y', strtotime($row->data_planejamento)); ?></td>
                                
                                <td ><?php echo $row->nome_fantasia; ?></td>
                                <td class="text-white" style="text-align: center; width: 25%">                                    
                                    <a title="Excluir Registro" onclick="deleteConfirm('<?php echo  base_url('servico/atendimento/delete_ordem_servico/' . $row->id) ?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Exluir</a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
            </tbody>
        </table>
    </div>
</div>
   <script src="<?php echo base_url("assets/vendor/data-table/js/jquery.dataTables.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.buttons.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.flash.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/jszip.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/pdfmake.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/vfs_fonts.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.html5.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/buttons.print.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/js/dataTables.bootstrap.min.js")?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/dataTables.responsive.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/vendor/data-table/Responsive-2.2.2/js/responsive.bootstrap4.min.js"); ?>"></script>
    <link href="<?php echo base_url("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css")?>" />
    <script src="<?php echo base_url("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js")?>"></script>
    
    <script type="text/javascript">
         var path = '<?php echo base_url()?>';
        $(document).ready(function () {
            $('#tabela_ordens').DataTable({
                    responsive: true, 
                    dom: 'lTf<"html5buttons"B>gtip',
                    pageLength: 25,
                    buttons: [],
                    "paging":   false,
                    "bDestroy": true,
                    "info":     false,
                    language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
            });
        });
    </script>        