<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="row">
        <div class="col-12">
          

            <div class="card mb-4">
               
                <div class="card-body">
                    <div class="card-title mb-4">
                        <h4><?php echo $titulo?></h4>
                    </div>
                    <div class="card-subtitle">


                    </div>
                    <form method="POST" data-parsley-validate data-toggle="validator" id="validation-form" 
                          action="<?php echo $caminho_url->create_registro?>" >  
                        <div class="form-row">
                            
                       
                            <div class="form-group col-md-12 ">
                                <fieldset class="form-group">
                                  <label class="form-label">Descrição</label>
                                  <input type="text" id="descricao" name="descricao" required class="form-control">
                                  <div class="help-block with-errors"></div>
                                </fieldset>
                                 
                                    
                                        
                                
                                <div class="form-row">
                                    <fieldset class="form-group  col-md-6">
                                        <label class="form-label">Centro de Custos</label>
                                        <select id="centro_custo_id" name="centro_custo_id" class="custom-select">
                                          <option>Selecione...</option>
                                            <?php foreach ($custos as $grp) { ?>
                                                <option value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </fieldset>
                                    <fieldset class="form-group col-md-6">
                                        <label class="form-label">Tipo Despesa</label>
                                        <select id="tipo_despesa" name="tipo_despesa" class="custom-select">
                                            <option value="0">Débito</option>
                                            <option value="1">Crédito</option>
                                        </select>
                                         <div class="help-block with-errors"></div>                                          

                                    </fieldset>
                             
                                </div>
                               
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                                    <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                </div>    
                            </div>
                          
                        </div>  
                    </form>
                     
                </div>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        var path = "<?php echo base_url() ?>";
        


    </script>
