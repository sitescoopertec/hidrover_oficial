<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="row">
        <div class="col-12">         
            <div class="card mb-4">               
                <div class="card-body">
                <div class="card-title mb-4">
                    <h4><?php echo $titulo . ' - ' . $result->nome_fantasia?></h4>
                </div>
                <div class="card-subtitle">
                    <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $caminho_url->create?>" class="btn btn-primary btn-outline btn-sm">       
                            <i class="fa fa-file"></i>
                            <span class="align-middle">Novo</span>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <label class="switcher">
                            <input type="checkbox" id="ativo" onchange="ativar_registro('<?php echo $caminho_url->ativar_registro .'/'. $result->id ?>')" name="ativo" <?php echo ($result->ativo==='1') ? 'checked':''; ?> class="switcher-input">
                            <span class="switcher-indicator">
                            </span>
                            <span class="switcher-label">Ativo</span>
                        </label>
                    </div>                    
                </div>
                </div>
                    
                    <form method="POST" id="validation-form" novalidate="novalidate" data-select2-id="validation-form" 
                            action="<?php echo $caminho_url->update_registro ?>">
                        <div class="form-row">
                        <input type="hidden" name="id" id="id" value="<?php echo $result->id; ?>">
                        <div class="form-group col-md-12 ">
                                <fieldset class="form-group">
                                  <label class="form-label">Nome Fantasia</label>
                                  <input type="text" id="nome_fantasia" name="nome_fantasia" value="<?php echo $result->nome_fantasia; ?>" required class="form-control">
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="form-label">Razão Social</label>
                                  <input type="text" id="razao_social" name="razao_social" value="<?php echo $result->razao_social; ?>" required class="form-control">
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="form-label">Email</label>
                                  <input type="email" id="email" name="email" value="<?php echo $result->email; ?>" required class="form-control">
                                </fieldset>
                                <div class="form-row">
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">CPF</label>
                                        <input type="text" id="cpf" name="cpf" value="<?php echo $result->cpf; ?>" required class="form-control">
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">Telefone</label>
                                        <input type="text" id="telefone" name="telefone" value="<?php echo $result->telefone; ?>" class="form-control">
                                    </fieldset>
                                    <fieldset class="form-group col-md-4">
                                        <label class="form-label">Celular</label>
                                        <input type="text" id="celular" name="celular" value="<?php echo $result->celular; ?>" required class="form-control">
                                    </fieldset>
                                </div>
                                <div class="form-row">
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">UF</label>
                                            <select onchange="carrega_combo()" id="uf_id" name="uf_id" class="custom-select">
                                                <?php
                                                    foreach ($ufs as $grp) {
                                                        $selected = '';
                                                        if ($result->unidade_federativa_id == $grp->id) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option <?php echo $selected ?> value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                                    <?php } ?>
                                            </select>
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Cidade</label>
                                            <select id="cidade_id" name="cidade_id" class="custom-select">
                                                <?php
                                                foreach ($cidades as $grp) {
                                                    $selected = '';
                                                    if ($result->cidade_id == $grp->id) {
                                                        $selected = 'selected';
                                                    }
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo $grp->id ?>"><?php echo $grp->descricao ?></option>
                                                <?php } ?>
                                            </select>
                                        </fieldset>
                                        <fieldset class="form-group col-md-12">
                                            <label class="form-label">Endereço</label>
                                            <input type="text" id="endereco" name="endereco" value="<?php echo $result->endereco; ?>" required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">Bairro</label>
                                            <input type="text" id="bairro" name="bairro" value="<?php echo $result->bairro; ?>" required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-6">
                                            <label class="form-label">CEP</label>
                                            <input type="text" id="cep" name="cep" value="<?php echo $result->cep; ?>" required class="form-control">
                                        </fieldset>
                                </div>
                                <div class="form-row">
                                        <fieldset class="form-group col-md-8">
                                            <label class="form-label">Contato</label>
                                            <input type="text" id="contato" name="contato" value="<?php echo $result->contato; ?>" required class="form-control">
                                        </fieldset>
                                        <fieldset class="form-group col-md-4">
                                            <label class="form-label">Telefone Contato</label>
                                            <input type="text" id="contato_telefone" name="contato_telefone" value="<?php echo $result->contato_telefone; ?>" required class="form-control">
                                        </fieldset>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success submit-btn with-arrow  mr-2 mb-4">Salvar</button>
                                    <a href="<?php echo $caminho_url->index ?>" class="btn mr-2 mb-4">Voltar</a>
                                </div>    
                            </div>
                        
                        </div>
                    </form>
            </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        function ativar_registro(url) {
            var ativo = document.getElementById('ativo');
            var valor = 0;
            if (ativo.checked){
                valor = 1;
            }
            $.ajax({
                url: url,
                type: "POST",
                datatype: "html",
                "data": {valor: valor}                     
            });
        }
    </script>
