<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Servico_model');
            $this->load->model('Servico_item_model');
            $this->load->model('Item_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Serviços';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','servico');
            $data->result = $this->Servico_model->retorna_servicos($_SESSION["uni_negocio_id"]);
            $this->load->template('servico/servico/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Serviços - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','servico');
            $this->load->template('servico/servico/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Serviços';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','servico');
            $data->prod_itens = $this->Servico_item_model->retorna_servico_itens($id);
            $data->result = $this->Servico_model->retorna_servico($id);
            $this->load->template('servico/servico/edit',$data);
	}
        
        public function item($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Itens do Serviço';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','servico');
            $data->itens = $this->Item_model->retorna_itens_ativos($_SESSION["uni_negocio_id"]);
            $data->prod_itens = $this->Servico_item_model->retorna_servico_itens($id);
            $data->result = $this->Servico_model->retorna_servico($id);
            $this->load->template('servico/servico/item',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Servico_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/servico/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/servico/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            if ($this->Servico_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/servico/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/servico/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Servico_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Servico_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('servico/servico/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('servico/servico/index');
            }
        }
        
        
        
         public function create_item() {
            $class = new stdClass();
            $class->servico_id = $this->input->post('servico_id');
            $class->item_id = $this->input->post('item_id');
            $class->qtd = numbertodatabase($this->input->post('qtd'));
            
            if ($this->Servico_item_model->create($class)) {
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/servico/item/' . $class->servico_id);
            } else {
                // creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/servico/edit/' . $class->servico_id);
            }
            
        }
        
        public function delete_item($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Servico_item_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('servico/servico/item/' . $id);
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('servico/servico/edit/' . $id);
            }
        }
        
        
}
