<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupo_pergunta extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Grupo_pergunta_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Grupo Pergunta';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','grupo_pergunta');
            $data->result = $this->Grupo_pergunta_model->retorna_grupo_perguntas($_SESSION["uni_negocio_id"]);
            $this->load->template('grupo_pergunta/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Grupo Pergunta - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','grupo_pergunta');
            $this->load->template('grupo_pergunta/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Grupo Pergunta';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','grupo_pergunta');
            $data->result = $this->Grupo_pergunta_model->retorna_grupo_pergunta($id);
            $this->load->template('grupo_pergunta/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Grupo_pergunta_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/grupo_pergunta/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/grupo_pergunta/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            if ($this->Grupo_pergunta_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/grupo_pergunta/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/grupo_pergunta/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Grupo_pergunta_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Grupo_pergunta_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('servico/grupo_pergunta/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('servico/grupo_pergunta/index');
            }
        }
}
