<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Formulario_model');
            $this->load->model('Formulario_pergunta_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Formulário';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','formulario');
            $data->result = $this->Formulario_model->retorna_formularios($_SESSION["uni_negocio_id"]);
            $this->load->template('formulario/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Formulário - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','formulario');
            $this->load->template('formulario/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Formulário';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','formulario');
            $data->result = $this->Formulario_model->retorna_formulario($id);
            $this->load->template('formulario/edit',$data);
	}
        
        public function pergunta($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Perguntas do Formulário';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','formulario');
            $data->result = $this->Formulario_model->retorna_formulario($id);
            $this->load->template('formulario/pergunta',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Formulario_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/formulario/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/formulario/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            if ($this->Formulario_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/formulario/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/formulario/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Formulario_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Formulario_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('servico/formulario/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('servico/formulario/index');
            }
        }
        
        
        
        public function retorna_perguntas(){
            $formulario_id = $this->input->post('formulario_id');  
          
            $data = new stdClass();
            $perg_pai = $this->Formulario_pergunta_model->retorna_formulario_perguntas_pai($formulario_id);         
             $pergunta = array();
            $linha = 0;
            foreach ($perg_pai as $pai){
                $pergunta[] = array('text' => $pai->pergunta  ,'href' => "edita_pergunta(". $pai->id . ")",'icon' => 'fa fa-list','nodes' =>"");
                $pergunta[$linha]["nodes"] = $this->retorna_filhos($formulario_id,$pai->id);
                $linha= $linha + 1;
            }          
            $perguntas = json_encode($pergunta);
            echo $perguntas;
        }
        
        public function retorna_filhos($formulario_id, $pergunta_pai_id){
            $perguntas = array();
            $linha = 0;
            $filhos = $this->Formulario_pergunta_model->retorna_formulario_perguntas_filhos($formulario_id, $pergunta_pai_id);         
            if (!empty($filhos)){
                
                foreach ($filhos as $filho){
                    $perguntas[] = array('text' => $filho->pergunta  ,'href' => "edita_pergunta(". $filho->id . ")",'nodes' =>"");
                    $perguntas[$linha]["nodes"] = $this->retorna_filhos($filho->formulario_id, $filho->id);                     
                    $linha= $linha + 1;
                 }
             }else{
                  return "";
             }
           
            return $perguntas;
            
           
        }
        
        
        
        public function salvar_pergunta(){
            
            $class = new stdClass();
            $pergunta_id = $this->input->post('pergunta_id');
            if ($pergunta_id === '-1'){
                $class->formulario_id = $this->input->post('formulario_id');                  
                if($this->input->post('pergunta_pai_id')<>""){
                    $class->pergunta_pai_id = $this->input->post('pergunta_pai_id');    
                }
                $class->pergunta = $this->input->post('pergunta');    
                $class->alerta = $this->input->post('alerta');    
                $class->ordem = $this->input->post('ordem');    
                $class->ativo = $this->input->post('ativo');    
                $this->Formulario_pergunta_model->create($class);
                
            } else {
                
                $class->id = $pergunta_id; 
                $class->formulario_id = $this->input->post('formulario_id');                                  
                if($this->input->post('pergunta_pai_id')<>""){
                    $class->pergunta_pai_id = $this->input->post('pergunta_pai_id');    
                }
                $class->pergunta = $this->input->post('pergunta');    
                $class->alerta = $this->input->post('alerta');    
                $class->ordem = $this->input->post('ordem');    
                $class->ativo = $this->input->post('ativo');  
                $this->Formulario_pergunta_model->update($class);
            }    
            
            
        }
        
        public function retorna_pergunta_pai(){
            
            $formulario_id = $this->input->post('formulario_id');   
            $perg_pai = $this->Formulario_pergunta_model->retorna_formulario_perguntas($formulario_id);        
            echo json_encode($perg_pai);
        }
        
        
        public function retorna_pergunta(){
            
            $pergunta_id = $this->input->post('pergunta_id');   
            $perg_pai = $this->Formulario_pergunta_model->retorna_formulario_pergunta($pergunta_id);        
            echo json_encode($perg_pai);
        }
        
        
        
        
        
        
}
