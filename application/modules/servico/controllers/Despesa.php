<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Despesa extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Despesa_model');           
            $this->load->model('sistema/Centro_custo_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Tipo de Despesas';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','despesa');
            $data->result = $this->Despesa_model->retorna_despesas($_SESSION["uni_negocio_id"]);
            $this->load->template('despesa/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo de Despesas - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','despesa');
            $data->custos = $this->Centro_custo_model->retorna_centro_custo_analitica_ativos($_SESSION["uni_negocio_id"]);
            $this->load->template('despesa/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Tipo de Despesas';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','despesa');
            //Busca os dados da View
            $data->custos = $this->Centro_custo_model->retorna_centro_custo_analitica_ativos($_SESSION["uni_negocio_id"]);
            $data->result = $this->Despesa_model->retorna_despesa($id);
            $this->load->template('despesa/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->tipo_despesa = $this->input->post('tipo_despesa'); //0 - Débito / 1 - Crédito
            $class->centro_custo_id = $this->input->post('centro_custo_id');
            
            $class->ativo = 1;
            if ($this->Despesa_model->create_despesa($class)) {
                $retID = $this->db->insert_id();
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/despesa/edit/' . $retID);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/despesa/create');
            } 
           
        }
        
        
        public function update_registro(){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');
            $class->tipo_despesa = $this->input->post('tipo_despesa'); //0 - Débito / 1 - Crédito
            $class->centro_custo_id = $this->input->post('centro_custo_id');
            
            
            if ($this->Despesa_model->update_despesa($class)) {
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/despesa/edit/' . $class->id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/despesa/create');
            } 
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Despesa_model->update_despesa($class)) {            
                $this->session->set_flashdata('alerta_sucesso','Salvo com sucesso!');            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Despesa_model->delete_despesa($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/despesa/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/despesa/index');

            }
        }
}
