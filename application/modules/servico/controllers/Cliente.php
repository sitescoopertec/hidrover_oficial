<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Cliente_model');
           
            $this->load->model('sistema/Cidade_model');
            $this->load->model('sistema/Unidade_federativa_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Cliente';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','cliente');
            $data->result = $this->Cliente_model->retorna_clientes();
            $this->load->template('cliente/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Clientes - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','cliente');
            //Busca os dados da View
            $data->ufs = $this->Unidade_federativa_model->retorna_unidade_federativa_ativos();
            //$data->cidades = $this->Cidade_model->retorna_cidades_ativos();
            //$data->tipos = $this->Tipo_cliente_model->retorna_tipo_cliente_ativos();
            $this->load->template('cliente/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Clientes';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','cliente');
            //Busca os dados da View
            $data->ufs = $this->Unidade_federativa_model->retorna_unidade_federativa_ativos();
            $data->cidades = $this->Cidade_model->retorna_cidades_ativos();
//            $data->tipos = $this->Tipo_cliente_model->retorna_tipo_cliente_ativos();
//            $data->tipos_salvos = $this->Cliente_tipo_model->retorna_cliente_tipos($id);
            $data->result = $this->Cliente_model->retorna_cliente($id);
            $this->load->template('cliente/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            $class->razao_social = $this->input->post('razao_social');
            $class->cpf = $this->input->post('cpf');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->celular = $this->input->post('celular');
            $class->cidade_id = $this->input->post('cidade_id');
            $class->endereco = $this->input->post('endereco');
            $class->bairro = $this->input->post('bairro');
            $class->cep = $this->input->post('cep');
            $class->contato = $this->input->post('contato');
            $class->contato_telefone = $this->input->post('contato_telefone');
            $class->ativo = 1;
            if ($this->Cliente_model->create_cliente($class)) {
                $retID = $this->db->insert_id();
                $data = new stdClass();
                $data->cliente_id = $retID;
                
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/cliente/edit/' . $retID);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/cliente/create');
            } 
           
        }
        
        
        public function update_registro(){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->nome_fantasia = $this->input->post('nome_fantasia');
            $class->razao_social = $this->input->post('razao_social');
            $class->cpf = $this->input->post('cpf');
            $class->email = $this->input->post('email');
            $class->telefone = $this->input->post('telefone');
            $class->celular = $this->input->post('celular');
            $class->cidade_id = $this->input->post('cidade_id');
            $class->endereco = $this->input->post('endereco');
            $class->bairro = $this->input->post('bairro');
            $class->cep = $this->input->post('cep');
            $class->contato = $this->input->post('contato');
            $class->contato_telefone = $this->input->post('contato_telefone');
            
            if ($this->Cliente_model->update_cliente($class)) {
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/cliente/edit/' . $class->id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/cliente/create');
            } 
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Cliente_model->update_cliente($class)) {            
                $this->session->set_flashdata('alerta_sucesso','Salvo com sucesso!');            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Cliente_model->delete_cliente($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/cliente/index');
            } else {         


                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/cliente/index');

            }
        }
}
