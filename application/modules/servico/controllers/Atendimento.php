<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atendimento extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Atendimento_model');
            $this->load->model('Tecnico_model');
            $this->load->model('Ordem_servico_model');
            $this->load->model('Atendimento_status_model');
            
            $this->load->model('Atendimento_ordem_servico_model');
            $this->load->model('Conferencia_model');
            $this->load->model('Conferencia_item_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Atendimento';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento');
            $data->result = $this->Atendimento_model->retorna_atendimentos($_SESSION["uni_negocio_id"]);
            $this->load->template('atendimento/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Atendimento - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento');
            //Busca os dados da View
            $data->tecnicos = $this->Tecnico_model->retorna_tecnicos_ativos($_SESSION["uni_negocio_id"]);
            $data->status = $this->Atendimento_status_model->retorna_atendimento_status_ativos($_SESSION["uni_negocio_id"]);
            
            $this->load->template('atendimento/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Atendimento - AT';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento');
            //Busca os dados da View
            $data->tecnicos = $this->Tecnico_model->retorna_tecnicos_ativos($_SESSION["uni_negocio_id"]);
            $data->status = $this->Atendimento_status_model->retorna_atendimento_status_ativos($_SESSION["uni_negocio_id"]);
            $data->result = $this->Atendimento_model->retorna_atendimento($id);
            $this->load->template('atendimento/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->tecnico_id = $this->input->post('tecnico_id');
            $class->atendimento_status_id = $this->input->post('atendimento_status_id');
            $class->observacao = $this->input->post('observacao');
            
            
            $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_agendamento'));
            if ($data == false) {
                $date = null;
            } else {
                $date = $data->format('Y-m-d');
            }            
            $class->data_agendamento = empty($date) ? NULL : $date;
            
            $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_realizado'));
            if ($data == false) {
                $date = null;
            } else {
                $date = $data->format('Y-m-d');
            }            
            $class->data_realizado = empty($date) ? NULL : $date;
            
            if ($this->Atendimento_model->create($class)) {
                $retID = $this->db->insert_id();
                $data = new stdClass();
                $data->atendimento_id = $retID;
                
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/atendimento/edit/' . $retID);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/atendimento/create');
            } 
           
        }
        
        
        public function update_registro(){
            $class = new stdClass();
            $class->id = $this->input->post('id');
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->tecnico_id = $this->input->post('tecnico_id');
            $class->atendimento_status_id = $this->input->post('atendimento_status_id');
            $class->observacao = $this->input->post('observacao');
            
            
            $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_agendamento'));
            if ($data == false) {
                $date = null;
            } else {
                $date = $data->format('Y-m-d');
            }            
            $class->data_agendamento = empty($date) ? NULL : $date;
            
            $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_realizado'));
            if ($data == false) {
                $date = null;
            } else {
                $date = $data->format('Y-m-d');
            }            
            $class->data_realizado = empty($date) ? NULL : $date;
            
            if ($this->Atendimento_model->update($class)) {
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/atendimento/edit/' . $class->id);
            } else {
                // user creation failed, this should never happen
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/atendimento/create');
            } 
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Atendimento_model->update_atendimento($class)) {            
                $this->session->set_flashdata('alerta_sucesso','Salvo com sucesso!');            
            }
        }
        public function delete($id) {
            if ($this->Atendimento_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
                redirect('servico/atendimento/index');
            } else {         
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                redirect('servico/atendimento/index');
            }
        }
        
        
        public function retorna_atendimentos(){   
        
            $atendimentos = $this->Atendimento_model->retorna_atendimentos($_SESSION["uni_negocio_id"]);
//            title: 'Click for Google',
//            url: 'http://google.com/',
//            start: '2019-04-28'
//            end: '2019-04-12T12:30:00'
            $result = array();
            foreach($atendimentos as $row){
               $result[] = array('title' => $row->nome_tecnico, 'url' =>'#', 'start'=>$row->data_agendamento, 'color'=> $row->cor);
            }
            

                
                
                
            echo json_encode($result); 
            return;  
        }
        
        public function carrega_fases_projeto($projeto_id){   
        
            $fases= $this->Projeto_fase_model->retorna_projeto_fases($projeto_id);

            if( empty ( $fases ) ) 
                return '{ "descricao": "Nenhuma Fase Encontrado" }';         
            echo json_encode($fases); 
            return;  
        }
        

        
        
        ///######################## INICIO DA ABA OS #########################
        
        public function retorna_ordem_servico(){     
            
            $atendimento_id = $this->input->post('atendimento_id');
            $tecnico_id = $this->input->post('tecnico_id');
            $data_inicial = $this->input->post('data_inicial');
            $data_final = $this->input->post('data_final');
            
            
            $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_inicial'));
            if ($data == false) {
                $dataini = "";
            } else {
                $dataini = $data->format('Y-m-d');
            } 
             $data = DateTime::createFromFormat('d/m/Y',  $this->input->post('data_final'));
            if ($data == false) {
                $datafin = "";
            } else {
                $datafin = $data->format('Y-m-d');
            }    
            
            $ordens = $this->Ordem_servico_model->retorna_ordem_servicos_para_atendimento($_SESSION["uni_negocio_id"],$atendimento_id,$tecnico_id, $dataini, $datafin);
             $html = ' <ul class="todo-list ">'
                         .'<span class="align-middle" style="font-weight:bold; padding-bottom:25px;">Ordens de Serviços</span>'
                         .'</br>';
            foreach ($ordens as $row){
               
                    $html = $html . '<li>'
                        . '   <div class="form-check">'
                        . '     <label class="custom-checkbox variation3">'
                        . '       <input class="form-check-input"  name="ordens[]" value="'.  $row->id .'" type="checkbox" >'. 'OS-' . $row->id  . ' - ' . $row->nome_fantasia . ' - ' . date('d-m-Y', strtotime($row->data_planejamento))
                        . '       <i class="input-helper"></i>'
                        . '     </label>'
                        . '   </div>'
                        . ' </li>';
                    
                
            }
            $html = $html . '</ul>';
            $data = new stdClass();
            $data->ordens = $html;
            echo json_encode($data)    ;
            
        }
        
        public function salvar_os_atendimento(){
            
            $class = new stdClass();
            $ordens = $this->input->post('ordens');            
            if (!empty($ordens)){
                $class->atendimento_id = $this->input->post('atendimento_id');     
                foreach ($ordens as $os){
                    $class->ordem_servico_id = $os;  
                    $this->Atendimento_ordem_servico_model->create($class);
                    $this->update_status_os_agendada($os);
                }
            }    
        }
        
        public function update_status_os_agendada($ordem_servico_id){            
            $class = new stdClass();
            $class->id = $ordem_servico_id;
            $class->ordem_servico_status_id = 2;
            $this->Ordem_servico_model->update($class);
        }
        
        public function retorna_ordens_atendimento($atendimento_id){            
            $data = new stdClass();
            $data->lista_ordens = $this->Atendimento_ordem_servico_model->retorna_atendimento_ordem_servicos($atendimento_id);            
            $this->load->view('atendimento/load_ordem_servico',$data);            
        }

        public function delete_ordem_servico($id) {
            $data = new stdClass();
            if ($this->Atendimento_ordem_servico_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION['msg_sucesso']);
            } else {         
                $this->session->set_flashdata('alerta_erro', $_SESSION['msg_erro']);
                
            }
        }
          ///######################## FINAL DA ABA PRODUTOS #########################
        
        
        
        //########################## SEPARAÇÃO #################################### 
        public function conferencia($atendimento_id){            
            $data = new stdClass();
            $data->result = $this->Atendimento_model->retorna_atendimento($atendimento_id);
            $data->conferencia = $this->Conferencia_model->retorna_conferencia_atendimento($atendimento_id);
            
            if(empty($data->conferencia)){
                $this->gerar_itens_conferencia($atendimento_id); 
                $data->conferencia = $this->Conferencia_model->retorna_conferencia_atendimento($atendimento_id);
            } 

            $data->itens_conferencia = $this->Conferencia_item_model->retorna_conferencia_itens($data->conferencia->id);
           
            
            
            $this->load->template('atendimento/conferencia',$data)  ;               
        }
        
        public function gerar_itens_conferencia($atendimento_id){
            
            $class = new stdClass();
            
            $class->data_separacao = date('Y-m-d');
            $class->atendimento_id = $atendimento_id;
            $class->usuario_id = $_SESSION["usuario_id"];
            $class->status = 0;
            
            if ($this->Conferencia_model->create($class)){
               $conferencia_id = $this->db->insert_id();
               $this->Conferencia_item_model->gera_itens_conferencia($atendimento_id, $conferencia_id) ;
            }            
        }    
        
        
        public function atualiza_item_conferencia(){
            
            $class = new stdClass();
            $conferencia_item_id = $this->input->post("conferencia_item_id");
            $conferido = $this->input->post("conferido");
                      
            
            $class->id = $conferencia_item_id;           
            $class->conferido = $conferido;
            
            $this->Conferencia_item_model->update($class);
            
        }    
        
        
        public function finalizar_conferencia($conferencia_id){            
            $class = new stdClass();
            $conferencia = $this->Conferencia_model->retorna_conferencia($conferencia_id);
            $class->id = $conferencia_id;           
            $class->status = 1;            
            $this->Conferencia_model->update($class);
            
            //Marca o atendimento como conferência realizada
            $class2 = new stdClass();
            
            $class2->id = $conferencia->atendimento_id;           
            $class2->conferencia = 1;            
            $this->Atendimento_model->update($class2);
            
        }    
        
        
        
}
