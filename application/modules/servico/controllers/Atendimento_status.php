<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atendimento_status extends MX_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Atendimento_status_model');
            $this->load->helper('form');
        }

	public function index(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Lista de Status Atendimento';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento_status');
            $data->result = $this->Atendimento_status_model->retorna_atendimento_statuss($_SESSION["uni_negocio_id"]);
            $this->load->template('atendimento_status/index',$data);
	}
        
        public function create(){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Status Atendimento - Novo';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento_status');
            $this->load->template('atendimento_status/create',$data);
	}
        
        public function edit($id){
            $data = new stdClass();
            //Título Página
            $data->titulo = 'Status Atendimento';
            //Cria as URL da View
            $data->caminho_url = caminhos_url_form('servico','atendimento_status');
            $data->result = $this->Atendimento_status_model->retorna_atendimento_status($id);
            $this->load->template('atendimento_status/edit',$data);
	}
        
        public function create_registro(){
            $class = new stdClass();
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            $class->ativo = 1; //Por padrão no create é ativo =1

            if ($this->Atendimento_status_model->create($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/atendimento_status/create');
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/atendimento_status/create');
            }
        }
        
        public function update_registro(){
            $class = new stdClass();
            $id = $this->input->post('id');            
            $class->id = $id;    
            $class->unidade_negocio_id = $_SESSION["uni_negocio_id"];
            $class->descricao = $this->input->post('descricao');    
            if ($this->Atendimento_status_model->update($class)) {
                // OK
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);
                redirect('servico/atendimento_status/edit/' . $id);
            } else {

                // user creation failed, this should never happen
                $data->error = 'Error! Please, try again.';
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                // send error to the view
                redirect('servico/atendimento_status/create');
            }
        }
        public function ativar_registro($id){
            $class = new stdClass();
            $class->id = $id;    
            $class->ativo= $this->input->post('valor');                        

            if ($this->Atendimento_status_model->update($class)) {            
                $this->session->set_flashdata('alerta_sucesso',$_SESSION["msg_sucesso"]);            
            }
        }
        public function delete($id) {

            // create the data object
            $data = new stdClass();

            if ($this->Atendimento_status_model->delete($id)) {            
                $this->session->set_flashdata('alerta_sucesso', $_SESSION["msg_sucesso"]);
                redirect('servico/atendimento_status/index');
            } else {        
                $this->session->set_flashdata('alerta_erro', $_SESSION["msg_erro"]);
                redirect('servico/atendimento_status/index');
            }
        }
}
