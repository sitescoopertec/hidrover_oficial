<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Despesa_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_despesas($unidade_negocio_id) {

        $this->db->select('ite.*,  cc.descricao desc_centro_custo');
        $this->db->from('despesa ite');
        $this->db->join('centro_custo cc','cc.id=ite.centro_custo_id');
        $this->db->where('ite.unidade_negocio_id', $unidade_negocio_id);
        $this->db->order_by('ite.descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_despesa($data) {
        return $this->db->insert('despesa', $data);
    }

    public function update_despesa($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('despesa', $data);
        }
    }

    public function delete_despesa($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('despesa');
        }
    }

    public function retorna_despesa($id) {
         $this->db->select('ite.*,  cc.descricao desc_centro_custo');
        $this->db->from('despesa ite');
        $this->db->join('centro_custo cc','cc.id=ite.centro_custo_id');
        $this->db->where('ite.id', $id);
        return $this->db->get()->row();
    }
    

    public function retorna_despesas_ativos($unidade_negocio_id) {

        $this->db->from('despesa');
        $this->db->where('unidade_negocio_id', $unidade_negocio_id);
        $this->db->where('ativo', 1);
        $this->db->order_by('nome');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function retorna_despesas_ativos_os($unidade_negocio_id, $ordem_servico_id) {

        $this->db->from('despesa tec');
        $this->db->where('tec.unidade_negocio_id', $unidade_negocio_id);
        $this->db->where('not exists (select id from ordem_servico_equipe x where x.despesa_id=tec.id and x.ordem_servico_id=' . $ordem_servico_id . ")");
        
        $this->db->where('tec.ativo', 1);
        $this->db->order_by('tec.nome');
        $query = $this->db->get();
        return $query->result();
    }
}
