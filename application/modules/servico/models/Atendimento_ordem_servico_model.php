<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Atendimento_ordem_servico_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }
        


    public function retorna_atendimento_ordem_servicos($atendimento_id) {
//        $this->db->select('at.atendimento_id, os.*, os_tp.descricao desc_tipo_os, os_st.descricao desc_status_os, for.descricao desc_formulario');
//        $this->db->select('fa.descricao desc_fase_projeto, cli.nome_fantasia, pj.descricao desc_projeto');
        $this->db->select('at.atendimento_id, at.id, os.id ordem_servico_id, os.data_planejamento, cli.nome_fantasia');
        $this->db->from('atendimento_ordem_servico at');
        $this->db->join('ordem_servico os','os.id=at.ordem_servico_id');
        $this->db->join('ordem_servico_tipo os_tp','os_tp.id=os.ordem_servico_tipo_id');
        $this->db->join('ordem_servico_status os_st','os_st.id=os.ordem_servico_status_id');
        $this->db->join('formulario for','for.id=os.formulario_id','left');
        $this->db->join('projeto_fase fa','fa.id=os.projeto_fase_id','left');
        $this->db->join('projeto pj','pj.id=fa.projeto_id','left');
        $this->db->join('cliente cli','cli.id=os.cliente_id');
        $this->db->where('at.atendimento_id',$atendimento_id);
        $this->db->order_by('os.data_planejamento');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('atendimento_ordem_servico', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('atendimento_ordem_servico', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('atendimento_ordem_servico');
        }
    }
    
    public function retorna_atendimento_ordem_servico($id) {

        $this->db->select('at.*, ats.descricao desc_atendimento_ordem_servico_status, tec.nome nome_tecnico, tec.cor');
        $this->db->from('atendimento_ordem_servico at');        
        $this->db->join('atendimento_ordem_servico_status ats','ats.id=at.atendimento_ordem_servico_status_id');        
        $this->db->join('tecnico tec','tec.id=at.tecnico_id');
        $this->db->where('at.id', $id);
        return $this->db->get()->row();
    }


}
