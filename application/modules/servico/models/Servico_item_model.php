<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Servico_item_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }

    public function retorna_servico_itens($servico_id) {
        $this->db->select('pro.*, ite.descricao desc_item');
        $this->db->from('servico_item pro');
        $this->db->join('item ite','ite.id= pro.item_id');
        $this->db->where('pro.servico_id',$servico_id);
        $this->db->order_by('ite.descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('servico_item', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('servico_item', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('servico_item');
        }
    }
    
    public function retorna_servico_item($id) {

        $this->db->from('servico_item');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_servico_item_ativos($unidade_negocio_id) {

        $this->db->from('servico_item');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
