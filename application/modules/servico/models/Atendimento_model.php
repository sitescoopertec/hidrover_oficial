<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Atendimento_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }
        


    public function retorna_atendimentos($unidade_negocio_id) {
        $this->db->select('at.*, case when at.conferencia = 0 then "Pendente" else "Realizada" end conferencia,  ats.descricao desc_atendimento_status, tec.nome nome_tecnico, tec.cor',false);
        $this->db->from('atendimento at');        
        $this->db->join('atendimento_status ats','ats.id=at.atendimento_status_id');        
        $this->db->join('tecnico tec','tec.id=at.tecnico_id');
        $this->db->where('at.unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('at.data_agendamento');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('atendimento', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('atendimento', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->query('delete from atendimento_ordem_servico where atendimento_id =' . $id);
            $this->db->where('id', $id);
            return $this->db->delete('atendimento');
        }
    }
    
    public function retorna_atendimento($id) {

        $this->db->select('at.*, ats.descricao desc_atendimento_status, tec.nome nome_tecnico, tec.cor');
        $this->db->from('atendimento at');        
        $this->db->join('atendimento_status ats','ats.id=at.atendimento_status_id');        
        $this->db->join('tecnico tec','tec.id=at.tecnico_id');
        $this->db->where('at.id', $id);
        return $this->db->get()->row();
    }

    public function retorna_atendimento_ativos($unidade_negocio_id) {

        
        $this->db->select('at.*, ats.descricao desc_atendimento_status, tec.nome nome_tecnico');
        $this->db->from('atendimento at');        
        $this->db->join('atendimento_status ats','ats.id=at.atendimento_status_id');        
        $this->db->join('tecnico tec','tec.id=at.tecnico_id');
        $this->db->where('at.unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('at.data_agendamento');
        
        $query = $this->db->get();
        return $query->result();
    }
}
