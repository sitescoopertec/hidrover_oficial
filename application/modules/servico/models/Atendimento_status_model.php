<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Serie_model class.
 * 
 * @extends CI_Model
 */
class Atendimento_status_model extends CI_Model {

    public function __construct() {
        parent::__construct();        
    }
        


    public function retorna_atendimento_statuss($unidade_negocio_id) {
        $this->db->from('atendimento_status');
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }

    public function create($data) {
        return $this->db->insert('atendimento_status', $data);
    }

    public function update($data) {
        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('atendimento_status', $data);
        }
    }

    public function delete($id) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('atendimento_status');
        }
    }
    
    public function retorna_atendimento_status($id) {

        $this->db->from('atendimento_status');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retorna_atendimento_status_ativos($unidade_negocio_id) {

        $this->db->from('atendimento_status');
        $this->db->where('ativo', 1);
        $this->db->where('unidade_negocio_id',$unidade_negocio_id);
        $this->db->order_by('descricao');
        $query = $this->db->get();
        return $query->result();
    }
}
