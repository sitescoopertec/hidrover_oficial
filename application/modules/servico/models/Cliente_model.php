<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portador_model class.
 * 
 * @extends CI_Model
 */
class Cliente_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->database();
    }

    public function retorna_clientes() {

        $this->db->select('pes.*, uf.sigla, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('cliente pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->order_by('pes.nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }

    public function create_cliente($data) {
        return $this->db->insert('cliente', $data);
    }

    public function update_cliente($data) {

        if ($data->id) {
            $this->db->where('id', $data->id);
            return $this->db->update('cliente', $data);
        }
    }

    public function delete_cliente($id) {

        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete('cliente');
        }
    }

    public function retorna_cliente($id) {
        $this->db->select('pes.*, uf.sigla, cid.unidade_federativa_id, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('cliente pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->where('pes.id', $id);
        return $this->db->get()->row();
    }
    
    public function retorna_cliente_por_tipo($tipo_cliente_id) {
        $this->db->select('pes.*, uf.sigla, cid.unidade_federativa_id, uf.descricao desc_uf, cid.descricao desc_cidade');
        $this->db->from('cliente pes');
        $this->db->join('cidade cid','pes.cidade_id=cid.id');
        $this->db->join('unidade_federativa uf','cid.unidade_federativa_id=uf.id');
        $this->db->join('cliente_tipo pt','pt.cliente_id=pes.id');
        $this->db->where('pes.ativo', 1);
        $this->db->where('pt.tipo_cliente_id', $tipo_cliente_id);
          $query = $this->db->get();
        return $query->result();
    }

    public function retorna_cliente_ativos() {

        $this->db->from('cliente');
        $this->db->where('ativo', 1);
        $this->db->order_by('nome_fantasia');
        $query = $this->db->get();
        return $query->result();
    }
}
