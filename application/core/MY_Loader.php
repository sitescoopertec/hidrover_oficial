<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {
    public function template($template_name, $vars = array(), $return = FALSE){
        if($return):
            $content  = $this->view('template/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('template/footer', $vars, $return);

            return $content;
        else:
            $this->view('template/header', $vars);
            $this->view($template_name, $vars);
            $this->view('template/footer', $vars);
        endif;
    }
    public function template_site($template_name, $vars = array(), $return = FALSE){
        if($return):
            $content  = $this->view('template_site/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('template_site/footer', $vars, $return);

            return $content;
        else:
            $this->view('template_site/header', $vars);
            $this->view($template_name, $vars);
            $this->view('template_site/footer', $vars);
        endif;
    }
    
}

