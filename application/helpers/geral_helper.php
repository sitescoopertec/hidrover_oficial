<?php

defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Enun_tipo_pessoa
{
    const Cliente = 1;
    const Fornecedor = 2;
    const Mecanico = 3;
    const Motorista = 4;
    const Posto = 5;
}



function caminhos_url_form($nome_modulo, $nome_controller){
   $caminho_url = new stdClass();
   $caminho_url->index = base_url($nome_modulo.'/'.$nome_controller.'/index');
   $caminho_url->create = base_url($nome_modulo.'/'.$nome_controller.'/create');            
   $caminho_url->edit = base_url($nome_modulo.'/'.$nome_controller.'/edit');
   $caminho_url->create_registro = base_url($nome_modulo.'/'.$nome_controller.'/create_registro');
   $caminho_url->update_registro = base_url($nome_modulo.'/'.$nome_controller.'/update_registro');
   $caminho_url->delete = base_url($nome_modulo.'/'.$nome_controller.'/delete');
   $caminho_url->ativar_registro = base_url($nome_modulo.'/'.$nome_controller.'/ativar_registro');
   
   return $caminho_url;
}

function normalizeChars($string) {
    return str_replace(array('à', 'á', 'â', 'ã', 'ä', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý'), array('a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y'), $string);
}

function numbertodatabase($number) {
    if ($number >999){
        $number = str_replace('.', '', $number);
    }
    return str_replace(',', '.', $number);
}

function databasetonumber($number) {
    return number_format($number,2,",",".");
}

function databasetonumber1dec($number) {
    return number_format($number,1,",",".");
}
function databasetonumber3dec($number) {
    return number_format($number,3,",",".");
}
function databasetonumber4dec($number) {
    return number_format($number,4,",",".");
}
function databasetonumber0dec($number) {
    return number_format($number,0,",",".");
}
function getIp_Client() {
            $ip = null;
            if ((isset($_SERVER['HTTP_X_FORWARDED_FOR'])) &&
                    (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            elseif ((isset($_SERVER['HTTP_CLIENT_IP'])) &&
                    (!empty($_SERVER['HTTP_CLIENT_IP']))) {
                $ip = explode(".", $_SERVER['HTTP_CLIENT_IP']);
                $ip = "{$ip[3]}.{$ip[2]}.{$ip[1]}.{$ip[0]}";
            }
            elseif ((!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) &&
                    (empty($_SERVER['HTTP_X_FORWARDED_FOR'])) &&
                    (!isset($_SERVER['HTTP_CLIENT_IP'])) &&
                    (empty($_SERVER['HTTP_CLIENT_IP'])) &&
                    (isset($_SERVER['REMOTE_ADDR']))) {
                $ip = ($_SERVER['REMOTE_ADDR']);
            }
            else {
                // ip is null
            }
            return ($ip);
}
