<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct() {
            parent::__construct();
            $this->load->model('sistema/Unidade_negocio_model');
            $this->load->model('sistema/Usuario_model');
        }
	public function index()
	{
          $this->load->view('login/login');
            
	}
        
        public function adm()
	{
                       $this->load->view('login/login');
            
	}
    public function login2() {
        
        $_SESSION["msg_sucesso"]='Ação realizada com sucesso!';
        $_SESSION["msg_erro"]='OPS! Alguma coisa ocorreu, tente novamente!';
        
        $_SESSION["uni_negocio_id"]=1;
        $_SESSION['usuario_id'] = 1;
        $_SESSION['is_admin'] = true;
        
        $data = new stdClass();
        $_SESSION['menu'] = $this->monta_menu();
        $this->load->template('dashboard/dashboard', $data);
       
    }
    
    public function login() {
        $data = new stdClass();
        $username = $this->input->post('usuario');
        $password = $this->input->post('senha');

        if ($this->Usuario_model->valida_login($username, $password)) {
            $user_id = $this->Usuario_model->retorna_id_por_usuario($username);
            $user = $this->Usuario_model->retorna_usuario($user_id);

            if ($user->ativo == 0) {
                $this->session->set_flashdata('erro_login', 'User não está ativo.');
                 redirect('login/index');
            } else {

                // set session user datas
                $_SESSION['usuario_id'] = (int) $user->id;
                $_SESSION['usuario'] = (string) $user->usuario;
                $_SESSION['nome'] = (string) $user->nome;
                $_SESSION['logged_in'] = (bool) true;
                $_SESSION['is_confirmed'] = (bool) $user->ativo;
                $_SESSION['is_admin'] = (bool) ($user->tipo == "1" ? TRUE : FALSE);
                $_SESSION['usuario_grupo_id'] = (int) $user->usuario_grupo_id;                   
                $_SESSION['uni_negocio_id'] = (int)$user->unidade_negocio_id;
                

                //Carrega os Menus do Usuario
                $_SESSION['menu'] = $this->monta_menu();
                
                
                    
                $_SESSION["msg_sucesso"]='Ação realizada com sucesso!';
                $_SESSION["msg_erro"]='OPS! Alguma coisa ocorreu, tente novamente!';
        
    

                   $this->load->template('dashboard/dashboard', $data);
            }
        } else {
            $this->session->set_flashdata('erro_login', 'Usuário ou senha incorretos.');
             redirect('login/index');
        } 
    }
    
    
    public function logout() {
        if (isset($_SESSION['usuario_id']) && $_SESSION['logged_in'] === true) {
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            redirect('login/index');
        } else {
            redirect('/');
        }
    }
    
    
    public function home() {
        
        $_SESSION["msg_sucesso"]='Ação realizada com sucesso!';
        $_SESSION["msg_erro"]='OPS! Alguma coisa ocorreu, tente novamente!';
        
        $_SESSION["uni_negocio_id"]=1;
        $_SESSION['usuario_id'] = 1;
        $_SESSION['is_admin'] = true;
        
        $data = new stdClass();
        $_SESSION['menu'] = $this->monta_menu();
        $this->load->template('dashboard/dashboard', $data);
       
    }
    
    public function monta_menu(){
        
        $this->load->model('sistema/Usuario_model');
        $menus = $this->Usuario_model->retorna_menus(3);
        
        
                    $modulo = '0';
                    $fechar_modulo = 'NAO';
                    $tipo_menu = '0';
                    $fechar_tipo_menu = 'NAO';
                    $str_menu = '';
                    foreach ($menus as $mnu) {
                        if ($mnu->modulo_id != $modulo) {
                            $modulo = $mnu->modulo_id;
                            $tipo_menu =0;
                            if ($fechar_modulo == 'SIM') {
                                //<!--Fechamento do menu e modulo-->
                                $str_menu = $str_menu .'</div> </li> </ul> </div> </li>';
                            }
                            //<!--Abertura do Modulo-->       
                    $str_menu = $str_menu . ' <li class="nav-item "> ' 
                                    .'<a class="nav-link has-submenu waves-effect menu_modulo" href="#">'
                                    .'<i class="' . $mnu->icone_modulo .' md-18 mr-2"></i>'
                                    .'<span class="menu-title ">' . $mnu->desc_modulo .'</span></a>'
                                .'<div class="sub-menu level1">';   
                            $fechar_modulo = 'SIM';
                            $fechar_tipo_menu = 'NAO';
                        }
                        
                        if ($mnu->tipo_menu_id != $tipo_menu) {
                            $tipo_menu = $mnu->tipo_menu_id;
                            if ($fechar_tipo_menu == 'SIM') {
                                //Fecha Tipo Menu                            
                                $str_menu = $str_menu . ' </div> </li> </ul>';                       
                            }
                            
                            $str_menu = $str_menu . '<ul> <li class="nav-item">'
                                    .'<a class="nav-link has-submenu" href="#"><i class="' . $mnu->icone_tipo_menu .' md-18 mr-2"></i>' . $mnu->desc_tipo_menu
                                    .'</a>'
                                    .'<div class="sub-menu level2">';              
                        
                                $fechar_tipo_menu = 'SIM';
                        }
                                $str_menu = $str_menu .'<ul>'
                                        .'<li><a class="nav-link" href="' . base_url($mnu->menu) .'"><i class="' . $mnu->icone .' md-16 mr-2"></i>' . $mnu->desc_menu .'</a></li>'
                                                        .'</ul>';

//                         $fechar_modulo = 'NAO';       
//                         $fechar_tipo_menu = 'NAO';
                         
                        
                        
                        
                    }
                    

        return $str_menu;
    }

}
