<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MX_Controller {

	public function __construct() {
            parent::__construct();
            $this->load->model('sistema/Unidade_negocio_model');
            $this->load->model('sistema/Usuario_model');
            $this->load->model('sistema/Painel_model');
            $this->load->model('sistema/Servico_model');
            $this->load->model('sistema/Topico_model');
            $this->load->model('sistema/Categoria_model');
            $this->load->model('sistema/Galeria_model');
            $this->load->model('sistema/Historia_model');

            $_SESSION["uni_negocio_id"] = 1;
        }
        
	public function index() {
          $data = new stdClass();
          
          //Busca os informações da empresa
        $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
        
        $data->empresas = $this->Unidade_negocio_model->retorna_unidade_negocios_id($_SESSION["uni_negocio_id"]);
        
        $data->painel = $this->Painel_model->retorna_painel_ativos($_SESSION["uni_negocio_id"]);
        
        $data->servicos = $this->Servico_model->retorna_servico_ativos($_SESSION["uni_negocio_id"]);
        
        $data->topicos = $this->Topico_model->retorna_topico_ativos($_SESSION["uni_negocio_id"]);
        
        $data->categorias = $this->Categoria_model->retorna_categoria_ativos($_SESSION["uni_negocio_id"]);
        
        $data->galerias = $this->Galeria_model->retorna_galeria_ativos($_SESSION["uni_negocio_id"]);
        
        $this->load->template_site('site/home', $data);
	
        
        }
        
        public function historia () {
            $data = new stdClass();
            $this->load->helper('form');
            $this->load->library('form_validation');
            //Busca os informações da empresa
            $data->empresa = $this->Unidade_negocio_model->retorna_unidade_negocio($_SESSION["uni_negocio_id"]);
            $data->historias = $this->Historia_model->retorna_historias($_SESSION["uni_negocio_id"]);
       
            $this->load->template_site('site/historia', $data);
        }
        
}
