// Data Table Plugin Call
$(function () {
	$('#view_report_table').DataTable({
		dom: 'lTf<"html5buttons"B>gtip',
		pageLength: 10,
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		],
                language: {
                        url: '<?php echo base_url("assets/vendor/data-table/js/portugues.json"); ?>'
                    }
	});
});